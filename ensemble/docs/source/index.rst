.. canesm-ensemble documentation master file, created by
   sphinx-quickstart on Thu Feb 28 15:15:16 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to canesm-ensemble's documentation!
===========================================

|build-status| |coverage|


To help with setting up CanESM5 ensemble members this code
provides a wrapper around the `setup-canesm <https://gitlab.science.gc.ca/CanESM/CanESM5/blob/develop_canesm/Running_readme.md>`_ script. Here
are a few key points

   * Used remotely from your laptop or from the science network
   * Ensembles are setup using a simple configuration file
   * Ability to change most canesm.cfg, basefile and restart settings, among others.
   * Option to share code and executables between ensemble members
   * Ability to add members and extend simulation time
   * Tool to help with job submission for large ensembles (beta)
   * Graphical overview of the ensemble status (beta)

.. note::

   This code is designed for CanESM5 rc3.1+, so will not work on older versions.


.. toctree::
   :maxdepth: 2
   :caption: Getting Started:

   usage
   overview
   cli

.. toctree::
   :maxdepth: 2
   :caption: User Guide:

   options
   examples

.. toctree::
   :maxdepth: 1
   :caption: Help & Reference:

   canesm
   changelog
   help
   py3setup

.. |build-status| image:: https://gitlab.science.gc.ca/CanESM/canesm-ensemble/badges/master/build.svg?style=flat
    :alt: build status
    :target: https://gitlab.science.gc.ca/CanESM/canesm-ensemble/commits/master

.. |coverage| image:: https://gitlab.science.gc.ca/CanESM/canesm-ensemble/badges/master/coverage.svg?style=flat
    :alt: build status
    :target: https://gitlab.science.gc.ca/CanESM/canesm-ensemble/commits/master


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
