.. _examples:

Examples
********

.. _step_by_step:

A Step-by-Step Guide
--------------------

1. **Creating the config file**

   Once :ref:`installed<usage>` the next step is to create a configuration file that
   will dictate how the ensemble and each member is setup. The configuration is a simple
   text file that lists the settings and parameters that will be used. It uses the YAML markup
   language, so can be created in any text editor.

   .. code-block:: bash

      $ touch test_config.yaml

2. **Options for setup-canesm**

   We can now start adding settings to our ``test_config.yaml`` file. Since this is based on
   a wrapper around ``setup-canesm`` we need to define the three inputs to that code.

   ``runid`` will be the base-name given to the runs with a ``-###`` appended to each
   ensemble member.

   ``config`` defines whether the model will be run in coupled mode (``ESM``) or with
   specified sea surface temperatures and sea ice (``AMIP``).

   ``ver`` specifies which version of the model we will use. This can be either a branch
   name from the CanESM5 repository, or a commit hash. For our example lets call this ``test_run``
   and make an ``AMIP`` ensemble from release candidate 3.1, the code used for the CMIP 6 experiments.

   .. code-block:: yaml

      runid: test_run
      config: AMIP
      ver: rc3.1

3. **Run location**

   Next, we need to define where the simulation will be done. For this we need to define the
   ``machine`` that we will use, the ``user`` account where it will be ran from, and the
   ``run_directory`` (relative to ``$HOME`` in the ``user`` account) where the code will be stored.

   .. code-block:: yaml

      machine: hare
      user: abc123
      run_directory: test_run

4. **Ensemble size**

   We now have everything we need to setup an ensemble. The next step is to specify the ensemble options.
   First, lets set how many ensemble members we want for this experiment

   .. code-block:: yaml

      ensemble_size: 2

5. **Run dates**

   For this test we'll just do two model runs. Next we need to set what time period
   the model will run over. Lets say we want to do a 10 year run starting in 2000.

   .. code-block:: yaml

      start_time: 2000
      stop_time: 2010

6. **Restart Files**

   Next, we need to specify the initial conditions of the model. We can take these
   from a previous model run. What run you choose depends on the expiriment you will
   be doing, but for this test lets use the historical CMIP 6 run as a starting point.

   .. code-block:: yaml

      restart_files: rc3.1-his01

   We've now set the model run from which we will take the initial conditions, but we
   also need to specify the time from that run that we want to take them. If we want to setup
   our runs with the historical conditions at the begining of the run we can set the
   ``restart_dates`` to be the conditions just before our run starts.

   .. code-block:: yaml

      restart_dates: 1999_m12

   However, this isn't required, and we could use any date that exists in the
   ``restart_files``. The ``restart_files`` can be stored on disc, or on tape, and
   you'll need to know where these are. ``rc3.1-his01`` is on tape, so we need to tell the
   ensemble to load it from there.

   .. code-block:: yaml

      tapeload: True

   .. note::

      There are some subtleties associated with restart files that are good to know and are
      discussed in more detail :ref:`here<understanding_restarts>`

7. **Additional Options**

   So, right now we have two ensemble members all setup to go, but they are currently identical,
   which isn't too helpful. So we need to change something about the runs. Let's perturb the intitial
   conditions for each job so they start on different paths. This is controlled by the
   ``pp_rdm_num_pert`` option.

   .. code-block:: yaml

      pp_rdm_num_pert: [0, 1]

8. **Putting it all together**

   And that's it for setup. We should now have a complete configuration file that looks like

   .. code-block:: yaml

      runid: test_run
      config: AMIP
      ver: rc3.1
      machine: hare
      user: abc123
      run_directory: test_run
      ensemble_size: 2
      start_time: 2000
      stop_time: 2010
      restart_files: rc3.1-his01
      restart_dates: 1999
      tapeload: True
      pp_rdm_num_pert: [0, 1]

   These are a few of the options that can be set, but there are many more, as described in :ref:`options`.

9. **Submitting the job**

   Lastly, we need to launch the ensemble. In the conda environment that you installed the code
   (see :ref:`here<setting_up_p3>` if you are unsure of conda environments), or from the pre-built
   environment on science, run the following command

   .. code-block:: bash

      $ setup-ensemble test_config.yaml


.. _example_config_files:

Example Configuration Files
---------------------------

Below are a few examples of some configuration files to help with setup.

.. _basic-yaml-file:

Basic YAML File
===============

.. literalinclude:: ../../canesm/setup_examples/example_config.yaml
   :language: yaml

.. _basic-json-file:

Basic JSON File
===============

If you prefer JSON the same can be done using this format

.. literalinclude:: ../../canesm/setup_examples/example_config.json
   :language: json

.. _setup-from-table-file:

Using a Table File
==================

.. literalinclude:: ../../canesm/setup_examples/example_config_from_table.yaml
   :language: yaml

And the table itself

.. literalinclude:: ../../canesm/setup_examples/example_table.txt

``canesm_cfg``, ``phys_parm`` or ``inline_diag_nl`` options can be adjusted in the table
with the column name of ``file:option``. For example

.. _canesm_cfg_table:

.. code-block::

   runid       restart_files   restart_dates   pp_rdm_num_pert canesm_cfg:runmode  phys_parm:pp_solar_const
   run-001     mc_abc          4500_m12	       0               AMIP-nudged         1360.747
   run-002     mc_abc          4550_m12	       2               AMIP-nudged         1361.747
   run-003     mc_abc          4600_m12	       4               AMIP-nudged         1362.747
   run-004     mc_abc          4650_m12	       6               AMIP-nudged         1363.747
   run-005     mc_abc          4700_m12	       8               AMIP-nudged         1364.747