runid       restart_files   restart_dates   pp_rdm_num_pert canesm_cfg:runmode  phys_parm:pp_solar_const    start_time  stop_time   run_directory
run-001     mc_abc          4500_m12	    0               AMIP-nudged         1360.747                    2000_m01    2014_m12    abc
run-002     mc_abc          4550_m12	    2               AMIP-nudged         1361.747                    2001_m01    2015_m12    abcd
run-003     mc_abc          4600_m12	    4               AMIP-nudged         1362.747                    2002_m01    2016_m12    abcd
run-004     mc_abc          4650_m12	    6               AMIP-nudged         1363.747                    2003_m01    2017_m12    abcde
run-005     mc_abc          4700_m12	    8               AMIP-nudged         1364.747                    2004_m01    2018_m12    abcde