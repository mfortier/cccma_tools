#!/bin/bash
# 
# Add a remote to CanESM repo (and the associated submodules)
#   - First arg:                namespace that the CanESM remote belongs to
#   - (optional) Second Arg:    name to attach to the new remote - if not given, defaults to the first argument


#==================
# handle arguments
#==================
[ -z "$1" ] && { echo "ERROR: You provide the username as the first argument" ; exit 1 ; }
name_space=$1
if [[ -n "$2" ]] ; then
    # if second argument given, set the remote name to it
    remote_name=$2
else
    # default to using the 1st argument
    remote_name=$1
fi

#=====================================
# Verify that we are in the super-repo
#=====================================
[ -d CanAM ] ||  { echo "ERROR: You MUST be in the CanESM super-repo" ; exit 1 ; }

#======================================
# Determine the gitlab root url to use
#======================================
# get list of current remote root addresses from 'git remote -v' output (removing ":namespace/project.git")
current_root_addresses=$(git remote -v | awk '{print $2}' | sed "s#:.*\.git##g")
if [[ -n $current_root_addresses ]]; then

    # get UNIQUE root addresses
    unique_root_addresses=$(echo $current_root_addresses | tr ' ' '\n' | sort -u)

    # select root address to use
    num_root_addresses=$(echo $unique_root_addresses | wc -w)
    if (( num_root_addresses == 1 ));  then
        # only one root address present. Use that
        ROOT_ADDRESS=$unique_root_addresses
    else
        # more than one address present, ask user to select
        echo "Your repo currently uses more than one url address for your remotes!"
        echo "select one to apply to this remote..."
        select address in $unique_root_addresses; do
            ROOT_ADDRESS=$address
            break
        done
    fi
else
    # there are NO remote addresses, default to gitlab.science.gc.ca
    ROOT_ADDRESS="git@gitlab.science.gc.ca"
fi

#=================
# Add the remotes!
#=================
git submodule foreach --recursive "git remote add ${remote_name} ${ROOT_ADDRESS}:${name_space}/\$name.git"
git remote add ${remote_name} ${ROOT_ADDRESS}:${name_space}/CanESM5.git
