#!/bin/bash
# mod_nl is used to change namelist values for the current job
mod_nl(){
  # Modify a text file containing namelists
  # Each variable definition in this input namelist file must be on a line by itself
  # and be the of the form var =.* which will be replaced with var = value
  #
  # Usage: mod_nl namelist_file_in var1 [var2 ...]
  #   namelist_file_in  ...is an existing text file containing the namelists
  #   var1 var2 ...     ...names of variables to be changed
  #
  set -e
  [ -z "$1" ] && cpl_bail "mod_nl requires a namelist file as the first arg"
  [ -z "$2" ] && cpl_bail "mod_nl requires at least 1 variable name on the command line"
  nlfile=$1
  [ ! -s $nlfile ] && cpl_bail "mod_nl input namelist file --> $nlfile <-- is missing or empty"
  shift

  # Create a backup copy of the input namelist file, overwriting any existing backup
  cp -f $nlfile ${nlfile}.bak

  # Write a sed program to make the requested substitutions
  sprog=mod_nl_cmd
  rm -f $sprog
  touch $sprog
  for var in $*; do
    ischar=0
    # Any variable name beginning with _char_ is assumed to contain a string value
    # The leading _char_ prefix is removed from this name before further processing
    if [ x`echo $var|sed -n '/^ *_char_/p'` != x ]; then
      ischar=1
      var=`echo $var|sed 's/^_char_//'`
    fi
    eval val=\$$var
    if [ -z "$val" ]; then
      # If this variable is not defined then issue a warning and continue
      echo "mod_nl: $var is not defined"
      continue
    fi
    if [ $ischar -eq 1 ]; then
      # This is a character variable that needs to be quoted
      vsub='s/^ *'$var' *=.*/'"  ${var} = \"$val\""'/'
    else
      vsub='s/^ *'$var' *=.*/'"  ${var} = $val"'/'
    fi
    # Add this command to the file
    # Note, the quotes are required to preserve white space
    echo "$vsub" >> $sprog

    tstrng=`grep '^ *'$var' *=' $nlfile | sed 's/ *//g'`
    if [ "x$tstrng" = "x" ]; then
      # This variable does not appear in the namelist file
      # Insert a line with a definition for var that will get modified below
      sed '1 a\
  '$var' = 0' $nlfile > add_nl_var_$$
      mv add_nl_var_$$ $nlfile
      echo "${nlfile}: ADDED $var = $val"
    else
      # Echo the changes to stdout (useful for debugging runs)
      echo "${nlfile}: RESET $var = $val"
    fi
  done

  # Make the substitutions in situ, overwriting the input file
  sed -f $sprog $nlfile > mod_nl_tmp_$$
  mv mod_nl_tmp_$$ $nlfile
  rm $sprog
}