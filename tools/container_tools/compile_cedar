#!/bin/bash

# Basic compilation steps for CanESM
# Note the directory "canesm" containing the full, recursive source code
# is assumed to exist at the location where this is run.
#
# Any required setting must be applied before compilation (including
# selecting the correct CPP key files and CanAM sizes).

# Set the root work location
CWD=$PWD

MAKE_ARGS="MKMF_TEMPLATE=mkmf.gcc"

# Compile CanAM
cd ${CWD}/canesm/CanAM/build

# Change the size of NNODE_A from 32 to 16. This must match however many MPI tasks CanAM is to be run with.
cp include/.defaults/*.h .
sed -i 's/#define _PAR_NNODE_A             32/#define _PAR_NNODE_A             16/' cppdef_sizes.h
make $MAKE_ARGS clean && make $MAKE_ARGS CPP_SIZES_FILE=$PWD/cppdef_sizes.h CPP_CONFIG_FILE=$PWD/cppdef_config.h > ${CWD}/compile_canam.log 2>&1 &


# compile CanCPL
cd ${CWD}/canesm/CanCPL/build
ln -s ../../CanAM/build/cppdef* .
make $MAKE_ARGS clean && make $MAKE_ARGS > ${CWD}/compile_cancpl.log 2>&1 &

# Compile NEMO coupled config for gcc
# First setup some input files from other components
cd ${CWD}/canesm/CanNEMO/nemo/NEMO/external
rm -f cppdef_*
ln -s ${CWD}/canesm/CanAM/build/cppdef_sizes.h cppdef_sizes.h90
ln -s ${CWD}/canesm/CanAM/build/cppdef_config.h cppdef_config.h90
cp ${CWD}/canesm/CanCPL/src/comm/com_cpl.F90 .

sed -i 's/cppdef_sizes.h/cppdef_sizes.h90/' com_cpl.F90
sed -i 's/cppdef_config.h/cppdef_config.h90/' com_cpl.F90

# Now compile NEMO
cd ${CWD}/canesm/CanNEMO/nemo/CONFIG
./makenemo clean -n CCC_CANCPL_ORCA1_LIM_CMOC -m gfortran_linux \
    && ./makenemo -j 8 -n CCC_CANCPL_ORCA1_LIM_CMOC -m gfortran_linux add_key "key_nosignedzero" del_key "key_diaar5" > ${CWD}/compile_cannemo.log 2>&1 &

wait

cd $CWD

# Link the compiled applications to the root work location
for target in 'canesm/CanAM/build/bin/AGCM' 'canesm/CanCPL/build/CPL' \
	      'canesm/CanNEMO/nemo/CONFIG/CCC_CANCPL_ORCA1_LIM_CMOC/BLD/bin/nemo.exe' ; do
    [ -f "$target" ] && cp $target . || echo "failed to link ${target}"  
done
