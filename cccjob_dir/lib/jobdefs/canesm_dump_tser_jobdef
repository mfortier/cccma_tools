#!/bin/bash
#     keyword :: canesm_dump_tser
# description :: Dump time series files.
set -a
. betapath2  # sets path for new package (required until old package is retired)

#  * ........................... Parmsub Parameters ............................

 runid="runid"; year="yyyy"; mon="mm";
 tser_uxxx="sc";
 canesm_dump_tser_short_term="off";

  # These variables are set when the job string is created
  previous_year=NotSet
  previous_month=NotSet

  current_year=NotSet
  current_month=NotSet

  next_year=NotSet
  next_month=NotSet

  run_start_year=NotSet
  run_start_month=NotSet
  run_stop_year=NotSet
  run_stop_month=NotSet

 crawork="crawork";
 username="username"; user="USER"; nqsprfx=""; nqsext="_${runid}_${year}_m${mon}";
 lopgm="lopgm"; stime="10800"; memory1="4gb";

#  * ............................ Condef Parameters ............................

 noprint=on
 nextjob=on
 debug=off
 shortermdir=$canesm_dump_tser_short_term
 hpcarchive_checksum=off

#  * ............................. Deck Definition .............................
jobname=dump_${tser_uxxx}; time=$stime ; memory=$memory1
.   comjcl.cdk
cat > Execute_Script <<'end_of_script'
# Dumps time series files to HPNLS using hpcarchive
# RSK, 02 2019

WRK_DIR=`pwd`

# Archive directory name
now=`date -u +%Y%j%H%M`
this_archive=${tser_uxxx}_${runid}_${previous_year}${previous_month}_${current_year}${current_month}_${now}

# check if archive already exists
[ -d $this_archive ] && { echo "Archive $this_archive appears to exist!" ; exit 1 ; }
mkdir $this_archive

# create file lists to be dumped
data_file_lists $runid $RUNPATH --start=$previous_year:$previous_month --stop=$current_year:$current_month
[ -s ${tser_uxxx}_time_series_files ] || { echo "ERROR in dumping: could not make ${tser_uxx}_time_series_files" ; exit 1 ; }

# Change into the archive dir
cd $this_archive

# Get hard copies of all time series files into a directory which we can dump
while read file; do
  access ${file} ${file} nocp=off
done < $WRK_DIR/${tser_uxxx}_time_series_files
rm -f .*_Link

# add rtds 
while read file; do
  access ${file} ${file} nocp=off
done < $WRK_DIR/${tser_uxxx}_rtd_files
rm -f .*_Link

cd $WRK_DIR

# dump using hpcarchive
if [ "$shortermdir" = "on" ] ; then
  hpcarchive_project=crd_short_term
else
  hpcarchive_project=crd_cccma
fi

if [ "$hpcarchive_checksum" = "on" ] ; then
  hcparchive_checksum_arg="-k"
else
  hcparchive_checksum_arg=""
fi

# try archiving several times
ntrymax=10
ntry=1
while [ $ntry -le $ntrymax ] ; do
  status=""
  hpcarchive ${hcparchive_checksum_arg} -v -g -p ${hpcarchive_project} -a ${this_archive} -c ${this_archive} || status=$?
  echo status=$status
  if [ "$status" = "" ] ; then
    break
  fi
  # unsuccessful archival - sleep a bit and try again.
  sleep 60
  ntry=`expr $ntry + 1`
done
if [ "$status" != "" ] ; then
  exit 1
fi

end_of_script

. endjcl.cdk
#end_of_job
