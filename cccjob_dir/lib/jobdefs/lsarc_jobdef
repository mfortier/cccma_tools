#!/bin/sh
#=======================================================================
# List files found in the CFS archive database             --- lsarc ---
# $Id: lsarc_jobdef 635 2010-06-03 23:20:53Z acrnrls $
#=======================================================================
#
#     keyword :: lsarc
# short description :: list files in CFS archive database
 set -a
 . betapath2

#  * ........................... Parmsub Parameters ............................

 runid=xyz; uxxx=uxxx; lsarc_uxxx=$uxxx; crawork=${runid}_job
 username=acrnxxx; user=XXX;

 # These variables are set when the job string is created
 previous_year=NotSet
 previous_month=NotSet

 current_year=NotSet
 current_month=NotSet

 next_year=NotSet
 next_month=NotSet

 run_start_year=NotSet
 run_start_month=NotSet
 run_stop_year=NotSet
 run_stop_month=NotSet

 nextjob=on
 noprint=on

 # Temporary directory where this script will run
 lsarc_CCRNTMP=''
 CCRNTMP=${lsarc_CCRNTMP:=$CCRNTMP}

 # RUNPATH on execution machine
 lsarc_RUNPATH=''
 if [ -n "$lsarc_RUNPATH" -a "$lsarc_RUNPATH" = "CCRNTMP" ]; then
   # Allow the special case of "lsarc_RUNPATH = CCRNTMP" to force RUNPATH
   # and CCRNTMP to be the same dir
   RUNPATH=$CCRNTMP
 else
   RUNPATH=${lsarc_RUNPATH:=$RUNPATH}
 fi

 # Alternate path to a directory where .queue/.crawork will be found
 JHOME=''

 if [ -n "$JHOME" -a x"$JHOME" != x"$HOME" ]; then
   # Allow optional reset of DATAPATH/RUNPATH
   JHOME_DATA=''
   DATAPATH=${JHOME_DATA:=$DATAPATH}
   RUNPATH=${JHOME_DATA:=$RUNPATH}
   # Allow optional reset of CCRNTMP
   JHOME_RUN=''
   CCRNTMP=${JHOME_RUN:=$CCRNTMP}
 fi

 jobname=lsarc; time="1799" ; memory="900mb" ;

 . comjcl.cdk

cat > Execute_Script <<'end_of_script'

  # ---Start_submit_ignore_code----
  set -a
  stamp=`date "+%j%H%M%S"$$`

  # Use -e option if recognized by echo
  if [ "X`echo -e`" = "X-e" ]; then
    echo_e() { echo ${1+"$@"}; }
  else
    echo_e() { echo -e ${1+"$@"}; }
  fi

  bail(){
    echo_e `date`" --- lsarc: $*"
    exit 1
  }

  lsarc_cfsuser=''
  cfsuser=${lsarc_cfsuser:=$username}

  lsarc_masterdir=''
  masterdir=${lsarc_masterdir:=off}

  lsarc_suffix_list=''
  suffix_list="${lsarc_suffix_list:-gs ss rs+12 ab+12 an+12}"

  lsarc_prefix_list=''
  prefix_list="${lsarc_prefix_list:-$lsarc_uxxx}"

  # This invocation of make_file_name_list will process the *_year and *_months
  # variables defined above and output a file containing definitions for
  # start_year, start_mon, stop_year, stop_mon, end_year, end_mon
  tmp_file_list="lsarc_date_list_${runid}_${stamp}"
  make_file_name_list --dates_only $tmp_file_list 2>&1 ||\
    bail "Problem in make_file_name_list"

  # Verify that the output list is not empty
  [ ! -s "$tmp_file_list" ] && bail "Unable to create file list"

  # A file list was created ...source it
  # This will define start_year, start_mon, stop_year, stop_mon,
  # end_year and end_mon in the current environment
  : ; . $tmp_file_list
  rm -f $tmp_file_list

  # Define a string that will indicate the start and stop dates
  ym_range="${start_year}m${start_mon}_${stop_year}m${stop_mon}"

  # A user supplied list of file1, file2,... definitions may be inserted
  # at this point and these definitions will superceed those below.
  # The user supplied list must redefine join
  join=0
  # <<INSERT_LSARC>>

  if [ $join -eq 0 ]; then
    # There was no user supplied list of file1, file2,... defs
    # Create this list internally

    # Create a file containing a list of file names that may then be
    # "sourced" in the current environment to define the variables
    # file1, file2,..., file$join, join. These variables are used by
    # lsarc below as the list of files to be found in the CFS archive.
    # Create a temporary file containing the file list
    tmp_file_list="lsarc_file_list_${runid}_${stamp}"
    make_file_name_list --nomulti_list $tmp_file_list 2>&1 ||\
      bail "Problem in make_file_name_list"

    [ ! -s "$tmp_file_list" ] && bail "Unable to create file list"

    # A file list was created ...source it
    : ; . $tmp_file_list
    rm -f $tmp_file_list
  fi

  [ $join -eq 0 ] && bail "Unable to compile a list of file names. join=0"

  # Ensure that the file(join+1) variable has a null value.
  # This may have a non-null value if the internal list is shorter than
  # the user supplied list.
  # Setting this variable to null ensures that lsarc will not read
  # more than join file names from the environment.
  Nplus=`echo $join|awk '{printf "%d",$1+1}' -`
  eval file${Nplus}=''

  # Define a location for a log that will contain a record of failures etc
  lsarc_log="$CCRNSRC/arc_dir/log/lsarc_problem_log"

  # Look for the files named in file1, file2,... in the CFS archive database
  Opts=''
  # Explicitly setting runid=abc on the command line tells lsarc to look
  # in the "official" directory of the CFS database
  # Adding the --others option tells lsarc to always look in the "others"
  # dir as well as in the dir associated with runid
  [ x"$masterdir" = xon ] && Opts="--others runid=$runid "

  # try_CFSDATA = 1 means attempt to read file names from the users CFSDATA file
  # The archive data may appear in this file quicker than in the archive database
  # itself, but checking this file alone will not ensure that the files are actually
  # in the arcfile database.
  try_CFSDATA=0

  nn=0
  itt_max=10
  while [ $nn -lt $itt_max ]; do
    nn=`expr $nn + 1`

    # List files on CFS that match the files named in file1, file2,...
    lsarc --env --with_individual --count_file=hits_count $Opts 2>&1 ||\
      bail "Error in lsarc"

    hits=`awk '{printf "%d",$1}' hits_count`
    if [ $hits -eq $join ]; then
      # Success
      echo "Success: Found $hits matches for $join file names"
      exit 0
    else
      # Failure
      if [ $try_CFSDATA -eq 1 ]; then
        # Try the CFSDATA file in the users info directory
        lsarc --env --with_individual --CFSDATA --count_file=hits_count $Opts 2>&1 ||\
          bail "Error in lsarc --CFSDATA"
        hits=`awk '{printf "%d",$1}' hits_count`
      fi
      if [ $hits -eq $join ]; then
        # Success
        echo "Success: Found $hits matches for $join file names"
        exit 0
      else
        # Failure. Wait 2 minutes then try again
        echo `date`"  iter=$nn  Found $hits matches for $join file names"
        [ $nn -lt $itt_max ] && sleep 120
      fi
    fi
  done

  # If we get to this point then the files are not on cfs
  echo `date`" runid = $runid  range = $ym_range  Found $hits matches for $join file names" >> $lsarc_log
  echo `date`" Running as "`whoami`" on "`hostname` >> $lsarc_log
  nn=0
  while [ $nn -lt $join ]; do
    nn=`expr $nn + 1`
    eval fname=\$file$nn
    echo "                               $fname" >> $lsarc_log
  done
  bail "Failure: runid = $runid  range = $ym_range - Found $hits matches for $join file names"

  # ---Stop_submit_ignore_code----

end_of_script

 . endjcl.cdk

#end_of_job
