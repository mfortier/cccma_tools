#!/bin/sh
#=======================================================================
# Copy a list of files from cfs                        --- load_list ---
# $Id: load_list_jobdef 669 2012-04-24 20:44:05Z acrnrls $
#=======================================================================
#
#     keyword :: load_list
# description :: load a list of files from cfs
#

 set -a
 . betapath2

 # bail is a simple error exit routine
 bail(){
   echo "dump_list: $1"
   echo " " >> haltit
   exit 1
 }

#  * ........................... Parmsub Parameters ............................

 # These variables are set when the job string is created
 run_start_year=NotSet  # memory99=1
 run_start_month=NotSet # memory99=1
 run_stop_year=NotSet   # memory99=1
 run_stop_month=NotSet  # memory99=1
 current_year=NotSet    # memory99=1
 current_month=NotSet   # memory99=1
 previous_year=NotSet   # memory99=1
 previous_month=NotSet  # memory99=1
 next_year=NotSet       # memory99=1
 next_month=NotSet      # memory99=1

 if [ $previous_year = "NotSet" -o $previous_month = "NotSet" ]; then
   if [ $next_year = "NotSet" -o $next_month = "NotSet" ]; then
     bail 'Neither previous_year/month nor next_year/month are set'
   else
     load_list_start_year=$current_year
     load_list_start_mon=$current_month
     load_list_end_year=$next_year
     load_list_end_mon=$next_month
   fi
 else
   load_list_start_year=$previous_year
   load_list_start_mon=$previous_month
   load_list_end_year=$current_year
   load_list_end_mon=$current_month
 fi

 load_list_reset_start_year=''
 reset_start_year=${load_list_reset_start_year:=''}
 if [ -n "$reset_start_year" ]; then
   # If reset_start_year is set then it must be of the form old_year:new_year
   # (ie a colon separated pair of integers) where the first integer is
   # the year that needs to be changed and the second integer is the year
   # that it will be changed to.
   old_year=`echo $reset_start_year|awk -F':' '{if(NF!=2){exit};printf "%d",$1}' -`
   new_year=`echo $reset_start_year|awk -F':' '{if(NF!=2){exit};printf "%d",$2}' -`
   [ -z "$old_year" ] && bail "Invalid reset_start_year=$reset_start_year"
   [ -z "$new_year" ] && bail "Invalid reset_start_year=$reset_start_year"
   if [ $old_year -eq $load_list_start_year ]; then
     load_list_start_year=$new_year
   fi
 fi

 load_list_reset_end_year=''
 reset_end_year=${load_list_reset_end_year:=''}
 if [ -n "$reset_end_year" ]; then
   # If reset_end_year is set then it must be of the form old_year:new_year
   # (ie a colon separated pair of integers) where the first integer is
   # the year that needs to be changed and the second integer is the year
   # that it will be changed to.
   old_year=`echo $reset_end_year|awk -F':' '{if(NF!=2){exit};printf "%d",$1}' -`
   new_year=`echo $reset_end_year|awk -F':' '{if(NF!=2){exit};printf "%d",$2}' -`
   [ -z "$old_year" ] && bail "Invalid reset_end_year=$reset_end_year"
   [ -z "$new_year" ] && bail "Invalid reset_end_year=$reset_end_year"
   if [ $old_year -eq $load_list_end_year ]; then
     load_list_end_year=$new_year
   fi
 fi

 load_list_start_year=`echo $load_list_start_year|awk '{printf "%04d",$1}' -`
 load_list_start_mon=`echo $load_list_start_mon|awk '{printf "%02d",$1}' -`
 load_list_end_year=`echo $load_list_end_year|awk '{printf "%04d",$1}' -`
 load_list_end_mon=`echo $load_list_end_mon|awk '{printf "%02d",$1}' -`

 [ -z "$load_list_start_year" ] && bail "load_list_start_year is null"
 [ -z "$load_list_end_year" ]   && bail "load_list_end_year is null"
 [ -z "$load_list_start_mon" ]  && bail "load_list_start_mon is null"
 [ -z "$load_list_end_mon" ]    && bail "load_list_end_mon is null"
 [ $load_list_start_mon -gt 12 -o $load_list_start_mon -lt 1 ] &&\
   bail "load_list_start_mon=$load_list_start_mon is out of range"
 [ $load_list_end_mon -gt 12 -o $load_list_end_mon -lt 1 ] &&\
   bail "load_list_end_mon=$load_list_end_mon is out of range"
 [ $load_list_start_year -gt $load_list_end_year ] &&\
   bail "load_list_start_year=$load_list_start_year is out of range"

 start_year=$load_list_start_year
 start_mon=$load_list_start_mon
 stop_year=$load_list_end_year
 stop_mon=$load_list_end_mon
 end_year=$load_list_end_year
 end_mon=$load_list_end_mon

 year=yyy; mon=mm


 jobname=load_list;
 runid="job000";
 uxxx='uxxx'; load_list_uxxx=$uxxx;
 load_list_prefix="${load_list_uxxx}_${runid}";
 load_list_prefix_=${load_list_prefix}_
 prefix=$load_list_prefix_
 crawork="${runid}_job"; username="acrnxxx"; user="XXX";

 load_cfsuser=$username
 load_list_cfsuser=$load_cfsuser
 cfsuser=$load_list_cfsuser

 RUNID=`echo "${runid}"|tr '[a-z]' '[A-Z]'`;

 stime="1800"; memory1="900mb"; lopgm="lopgm";
 nqsprfx="${runid}_"; nqsext='';

 # If set, arcfile is the name of a cmcarc file in which
 # the files to be retrieved are assumed to exist
 # If not set, all cmcarc and non-cmcarc files will be searched
 load_list_arcfile=''
 arcfile=${load_list_arcfile:=''}

 # If arcfile is not set and arclabel is set then all cmcarc files that
 # contain the substring arclabel within their file name will be searched
 # for the files that are to be retrieved
 load_list_arclabel=''
 arclabel=${load_list_arclabel:=''}

 # It is more efficient to set CCRNTMP = $RUNPATH if all files
 # are to be loaded onto pollux:$RUNPATH
 CCRNTMP=$CCRNTMP

 # RUNPATH may be set if desired
 RUNPATH=$RUNPATH

 # Alternate path to a directory where .queue/.crawork will be found
 JHOME=''

 if [ -n "$JHOME" -a x"$JHOME" != x"$HOME" ]; then
   # Allow optional reset of DATAPATH/RUNPATH
   JHOME_DATA=''
   DATAPATH=${JHOME_DATA:=$DATAPATH}
   RUNPATH=${JHOME_DATA:=$RUNPATH}
   # Allow optional reset of CCRNTMP
   JHOME_RUN=''
   CCRNTMP=${JHOME_RUN:=$CCRNTMP}
 fi

 # BERUNPATH must be set if files are to be copied from a machine that is
 # not the default back end machine
 bemach=''
 if [ -z "$bemach" ]; then
   # If bemach is not set explicitly then see if this job is executing on
   # a back end machine and use that machine name for bemach
   this_mach=`uname -n|awk -F\. '{print \$1}' -`
   on_back_end=0
   case $this_mach in
                    c[0-9]*) on_back_end=1 ;;
     zeta|saiph|spica|hadar) on_back_end=1 ;;
                za|sa|sp|ha) on_back_end=1 ;;
   esac
   if [ $on_back_end -eq 1 ]; then
     # Set this_mach to its known alias
     case $this_mach in
       ha|ha*) this_mach=hadar ;;
       sp|sp*) this_mach=spica ;;
          c1*) this_mach=spica ;;
          c2*) this_mach=hadar ;;
       za|ze*) this_mach=zeta  ;;
       sa|sa*) this_mach=saiph ;;
          c6*) this_mach=saiph ;;
          c7*) this_mach=zeta  ;;
     esac
     bemach=$this_mach
   fi
 fi
 if [ -n "$bemach" ]; then
   # Reset BERUNPATH if bemach is set
   # Ensure bemach contains a known alias
   case $bemach in
     ha|ha*) bemach=hadar ;;
     sp|sp*) bemach=spica ;;
     za|ze*) bemach=zeta  ;;
     sa|sa*) bemach=saiph ;;
   esac
   XXX=`ssh $bemach echo '$RUNPATH' 2>/dev/null` || bail "Unable to determine BERUNPATH"
   BERUNPATH=$XXX
 else
   BERUNPATH=$BERUNPATH
 fi

 # RMTRUNPATH is used when vic = on is specified
 RMTRUNPATH=$RMTRUNPATH

  # rtd parameters
  # PhysA = on/off     physical atmosphere variables
  # PhysO = on/off     physical ocean variables
  # CarbA = on/off     atmosphere carbon variables
  # CarbO = on/off     ocean carbon variables
  # CarbL = on/off     land carbon variables
  PhysA=on
  PhysO=on
  CarbA=on
  CarbO=on
  CarbL=on

# <<INSERT_LOAD_LIST>>

#  * ............................ Condef Parameters ............................

 noprint=on
 nextjob=on

 # Override md5sum check in tloader
 load_list_nocheck=off
 nocheck=${load_list_nocheck:=off}

 load_masterdir=off
 load_list_masterdir=${load_masterdir:=off}
 masterdir=${load_list_masterdir:=off}

 load_shortermdir=on
 load_list_shortermdir=${load_shortermdir:=on}
 shortermdir=${load_list_shortermdir:=on}

 # The default if none of sv, both, vic or svvic are set to "on" is to
 # load files onto the back end machine
 # Note: Once again submit3 is causing a problem. It will not respect end of line
 # comments inside its internally created file named job.info. As a result a
 # construct such as "vic =" found within an end of line comment are flagged as
 # syntax errors even though they are commented out.

 # sv : on means load files onto pollux
 load_sv=off
 load_list_sv=${load_sv:=off}
 sv=${load_list_sv:=off}

 both=off    # both  : on means load files onto pollux and onto back end machine
 vic=off     # vic   : on means send files to Victoria
 svvic=off   # svvic : on means load files onto pollux and send to Victoria

 # keepdskcpy : on means do not remove arcfiles from disk on cfs after they
 # have been loaded. This will eliminate the need to reload the same arc file
 # on a subsequent tloader when files from the previously loaded arc file are
 # required by the subsequent tloader job.
 load_list_keepdskcpy=''
 keepdskcpy=${load_list_keepdskcpy:=''}

  # cnfsallow = on will allow the user to use a cnfs dir for
  # RUNPATH and/or CCRNTMP when svsave = on
  # Normally this is not allowed because of the possibility that files
  # may get corrupted during transfer from/to the nfs file system
  load_list_cnfsallow=''; cnfsallow=${load_list_cnfsallow:=off}

  # besc = on causes a tdumper job to run on the back end (spica/hadar)
  # These definitions need to be on the same line to work around an error in the submit scripts
  load_list_besc=''; besc=${load_list_besc:=off}

#  * ............................. Deck Definition .............................

 # Note: If tloader is placed inside a conditional then submit3 will fail.
 # submit3 will ignore everthing after the tloader line when it creates an
 # internal file named job.info. It then attempts to source job.info and dies
 # due to a syntax error (the final fi is missing).
 . tloader.dk

#end_of_job
