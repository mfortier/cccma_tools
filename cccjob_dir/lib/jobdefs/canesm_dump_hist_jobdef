#!/bin/bash
#     keyword :: canesm_dump_hist
# description :: Dump model history files.
set -a
. betapath2  # sets path for new package (required until old package is retired)

#  * ........................... Parmsub Parameters ............................

 runid="runid"; year="yyyy"; mon="mm"; run_start_year="yyy";run_start_month="mm";
 uxxx="mc";
 canesm_dump_hist_prefix="mc";
 canesm_dump_hist_suffix_list="";
 canesm_dump_hist_short_term="on";

 # set the default to not delete files (they will be deleted in a separate job)
 delete_hist=off

 if [ "$year" = "yyyy" -a "$mon" = "mm" ] ; then
   # if not defined, it is assume a one time job at the beginning of the run.
   # assign previous month
   mon=$(printf "%02d" `expr $run_start_month - 1`)
   if [ $mon -lt 1 ] ; then
     mon=12
     year=$(printf "%04d" `expr $run_start_year - 1`)
   else
     year=$(printf "%04d" $run_start_year)
   fi
 fi

 # These variables are set when the job string is created
 previous_year=NotSet
 previous_month=NotSet

 current_year=NotSet
 current_month=NotSet

 next_year=NotSet
 next_month=NotSet

 run_start_year=NotSet
 run_start_month=NotSet
 run_stop_year=NotSet
 run_stop_month=NotSet

 crawork="crawork";
 username="username"; user="USER"; nqsprfx=""; nqsext="_${runid}_${year}_m${mon}";
 lopgm="lopgm"; stime="10800"; memory1="4gb";

#  * ............................ Condef Parameters ............................

 noprint=on
 nextjob=on
 debug=off
 shortermdir=$canesm_dump_hist_short_term
 hpcarchive_checksum=off

#  * ............................. Deck Definition .............................
jobname=dump_${uxxx}; time=$stime ; memory=$memory1
.   comjcl.cdk
cat > Execute_Script <<'end_of_script'
# Dumps files to HPNLS using hpcarchive
# NCS, 04 2018

WRK_DIR=`pwd`

# Archive directory name
now=`date -u +%Y%j%H%M`
this_archive=${canesm_dump_hist_prefix}_${runid}_${year}_m${mon}_${now}

# check if archive already exists
[ -d $this_archive ] && { echo "Archive $this_archive appears to exist!" ; exit 1 ; }
mkdir $this_archive

# Change into the archive dir
cd $this_archive

# canesm_dump_hist_suffix_list defines a list of suffixes to be dumped.

# These are used by "make_file_name_list" to make the list of files.
[ -z "${canesm_dump_hist_suffix_list}" ] && { echo "canesm_dump_hist_suffix_list must not be empty when dump=on" ; exit 1 ; }
export suffix_list="${canesm_dump_hist_suffix_list}"
export prefix_list=$uxxx
export runid

touch tmp_dump_file_list
make_file_name_list --start=$year:$mon --stop=$year:$mon tmp_dump_file_list || { echo "WARNING: could not make tmp_dump_file_list" ; exit 0 ; }

# The list has been created. Now source it and parse the files
. tmp_dump_file_list

# Get hard copies of all these files into a directory which we can dump
for i in `seq $join`; do
  nam=file$i ;
  access ${!nam} ${!nam} nocp=off
  echo ${!nam} >> $WRK_DIR/tmp_dump_file_list_names_only
done
rm -f .*_Link
rm -f tmp_dump_file_list

cd $WRK_DIR

# check size
sizemin=501 # minimum archive size in MB
size=`du -sm $this_archive | cut -f1 -d$'\t' | cut -f1 -d$' '`
echo size=${size}MB
if [ $size -lt $sizemin ] ; then
  # add a dummy file to meet the min size
  sizeadd=`echo $size $sizemin | awk '{printf "%d",($2-$1+1)}'`
  dd if=/dev/zero of=$this_archive/dummy count=$sizeadd bs=1048576
fi

# dump using hpcarchive
if [ "$shortermdir" = "on" ] ; then
  hpcarchive_project=crd_short_term
else
  hpcarchive_project=crd_cccma
fi

if [ "$hpcarchive_checksum" = "on" ] ; then
  hcparchive_checksum_arg="-k"
else
  hcparchive_checksum_arg=""
fi

# try archiving several times
ntrymax=10
ntry=1
while [ $ntry -le $ntrymax ] ; do
  status=""
  hpcarchive $hcparchive_checksum_arg -v -g -p $hpcarchive_project -a $this_archive -c $this_archive || status=$?
  echo status=$status
  if [ "$status" = "" ] ; then
    break
  fi
  # unsuccessful archival - sleep a bit and try again.
  sleep 60
  ntry=`expr $ntry + 1`
done
if [ "$status" != "" ] ; then
  exit 1
fi

# delete archived files
if [ "$delete_hist" = "on" ] ; then
  fdb mdelete -f tmp_dump_file_list_names_only
fi

end_of_script

. endjcl.cdk
#end_of_job
