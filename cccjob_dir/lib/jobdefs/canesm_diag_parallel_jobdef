#!/bin/sh
#     keyword :: canesm_diag_parallel
# description :: AGCM diagnostics basefile parallel for all months
set -a

#  *               Sample diagnostic submission job for COUPLED RUN.

. betapath2  # sets path for the beta-test version of the new diagnostics package

#  * ........................... Parmsub Parameters ............................

 year="yyyy"; mon="mm"; days=JAN; obsday=JAN; year_offset="${year_offset}"; runid="runid"; crawork="crawork";
 uxxx=mc; diag_uxxx=dc;
 model1="${uxxx}_${runid}_${year}_m${mon}_";
 flabel="${diag_uxxx}_${runid}_${year}_m${mon}_";

 chunk_size=$chunk_size;
 phys_nodex=1;
 nnodex=1;
 nprocx=$chunk_size;
 
 username="username"; user="USER"; nqsprfx="${flabel}"; nqsext="";
 dtime="3600"; gptime="3600"; stime="3600";
 memory_agcm_diag="8gb";
 memory1="${memory_agcm_diag}"; memory2="8gb"; memory3="8gb";
 CCRNTMP_agcm_diag="$CCRNTMP"; TMPFS_agcm_diag="";
 CCRNTMP="${CCRNTMP_agcm_diag}"; TMPFS="${TMPFS_agcm_diag}";
 
 plunit=vic; plpfn=$flabel;
 oldiag="diag4";

 resol="128_64";
 obsfile="obs_sfc_${resol}";
 obsfile2="pd_nmc_${resol}_1979_1988_m${mon}_";
#
  mask=land_mask_${resol};
#

 t1="        1"; t2="999999999"; t3="   1"; s3="   1";
 r1="        1"; r2="999999999"; r3="   1";
 g1="        1"; g2="999999999"; g3="   1";
 a1="        1"; a2="999999999"; a3="   1";
 lml="  995";  d="B";
 lay="    2";   coord=" ET15"; topsig="-1.00"; moist=" QHYB"; plid="      50.0";
 sref="   7.18E-3"; spow="        1.";
 m01="  012";

 issp="   24"; isgg="   24"; israd="   12"; isbeg="   12";

 itrvar="QHYB";   ntrac="   16";
 trac=" BCO  BCY  OCO  OCY  SSA  SSC  DUA  DUC  DMS  SO2  SO4  CO2";

# tracer names

  it01=LWC;  it02=IWC;  it03=BCO;  it04=BCY;  it05=OCO;
  it06=OCY;  it07=SSA;  it08=SSC;  it09=DUA;  it10=DUC;
  it11=DMS;  it12=SO2;  it13=SO4;  it14=HPO;  it15=H2O;
  it16=CO2;

# tracer reference values (default=0 for all tracers)

  xref01=0.         #   LWC
  xref02=0.         #   IWC
  xref03=5.03e-10   #   BCO
  xref04=3.93e-10   #   BCY
  xref05=2.27e-09   #   OCO
  xref06=3.35e-09   #   OCY
  xref07=1.35e-08   #   SSA
  xref08=1.07e-07   #   SSC
  xref09=3.57e-06   #   DUA
  xref10=2.79e-05   #   DUC
  xref11=2.91e-10   #   DMS
  xref12=5.72e-09   #   SO2
  xref13=1.41e-09   #   SO4
  xref14=0.0        #   HPO
  xref15=0.0        #   H2O
  xref16=0.0        #   CO2

# tracer power values (default=1 for all tracers)

# tracer advection: 1/0 flag for tracer advection (on/off)
  adv01=0;   adv02=0;  adv03=1;   adv04=1;    adv05=1;
  adv06=1;   adv07=1;  adv08=1;   adv09=1;    adv10=1;
  adv11=1;   adv12=1;  adv13=1;   adv14=0;    adv15=0;
  adv16=1;

 plv="   44"; p01="-0100"; p02="-0150"; p03="-0200"; p04="-0300"; p05="-0500";
              p06="-0700"; p07="   10"; p08="   15"; p09="   20"; p10="   30";
              p11="   50"; p12="   70"; p13="   80"; p14="   90"; p15="  100";
              p16="  115"; p17="  125"; p18="  130"; p19="  150"; p20="  170";
              p21="  175"; p22="  200"; p23="  225"; p24="  250"; p25="  300";
              p26="  350"; p27="  400"; p28="  450"; p29="  500"; p30="  550";
              p31="  600"; p32="  650"; p33="  700"; p34="  750"; p35="  775";
              p36="  800"; p37="  825"; p38="  850"; p39="  875"; p40="  900";
              p41="  925"; p42="  950"; p43="  975"; p44=" 1000"; p45="     ";
              p46="     "; p47="     "; p48="     "; p49="     "; p50="     ";
 kax="    1"; kin="    1"; lxp="    0"; map="    0"; b="  +";

 lrt="   63"; lmt="   63"; typ="    2";
 lon="  128"; lat="   64"; npg="    2";
 ncx="    2"; ncy="    2";
 delt="    900.0";
 ilev="   49"     ;   levs="   49"     ;

 # daily data tiers (default tier is "1")
 # 0: exit, i.e., not output daily files
 # 1: all 2-D variables requested for CMIP6, saved in _dd file
 # 2: all 3-D/zonal mean variables on pressure levels, saved in _dp and _dx files
 daily_cmip6_tiers="1 2"

# Options for epflux_cmip6.dk    
 nbands="0";  # to process all zonal wavenumbers (k) 

# nbands="1";            # to process a single wavenumber band, in addition to all k
# mfirst_band1="   1";   # starting k of first band  
# mlast_band1="   3";    # ending k of first band  
# plotlab_band1="M=1-3"  # label for plots 
# maxf="  64";           # maxf=lon/2  (needed for llafrb & used only if nbands > 0)

# Options for gwdiag_cmip6.dk   
# number of saves per day (4 => 6hr data)
 nsavesgw_per_day="   4" ### NB FOR RUNS DONE PRIOR TO ~FEB 18, 2019 this parameter should be set to 1

#  * ............................ Condef Parameters ............................

join=1
# auto=on
stat2nd=off
nextjob=on
noprint=on
datatype=specsig
gcmtsav=on
gcm2plus=on
wxstats=on
xtracld=off
xtradif=on

debug=off

splitfiles=on       # switch on the splitting mode.

vorstat=on     # =on to save vorticity (to output rv850 in CMIP6 6hrPlevPt table)
gssave=off     # =on to save subdaily main prognostic gridded variables on eta levels (in gpintstats2.dk)
spsave=off     # =on to save subdaily main prognostic spectral variables on pressure levels (in gpintstats2.dk)
gpxsave=off    # =on to save subdaily traces on pressure levels (in xstats5.dk)
dxsave=on      # =on to save daily zonal (mean) data (in epflux_cmip6.dk and gwdiag_cmip6.dk)
nocleanup=off  # =on to deactivate cleanall.dk and leave all subdaily files saved as ${flabel}_* on disk 
tdfiles=off    # =on means AGCM creates tendency files, =off does not create them

#  * ............................. Deck Definition .............................

. merged_diag_deck_parallel.dk

#end_of_job
