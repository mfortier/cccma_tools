#!/bin/sh
#=======================================================================
# Standard plotmix                                       --- plotmix ---
# $Id: plotmix_jobdef 653 2011-06-23 23:58:03Z acrnrls $
#=======================================================================
#     keyword :: plotmix
# description :: generate plots from diagnostic files
#******************************

  set -a
  . betapath2

#  * ........................... Parmsub Parameters ............................

  # These variables are set when the job string is created

  previous_year=NotSet
  previous_month=NotSet

  current_year=NotSet
  current_month=NotSet

  next_year=NotSet
  next_month=NotSet

  run_start_year=NotSet
  run_start_month=NotSet
  run_stop_year=NotSet
  run_stop_month=NotSet

  oldiag="diag4";
  lopgm="lopgmhr";

  jobname=plotmix;
  runid="job000"; uxxx='uxxx'; pool_uxxx=$uxxx; plotmix_uxxx=$pool_uxxx;
  crawork="${runid}_job"; username="acrnxxx"; user="XXX";
  RUNID=`echo "${runid}"|tr '[a-z]' '[A-Z]'`;
  nqsprfx="${runid}_"; nqsext='';

  # run and days are used as part of several plot titles
  run=$RUNID            # memory99=1
  plotmix_days=NotSet   # memory99=1
  days="DJF"            # memory99=1
  if [ x"$plotmix_days" != "xNotSet" ]; then
    eval days\=$plotmix_days   # memory99=1
  fi
  lc_days=`echo $days|tr '[A-Z]' '[a-z]'`
  case $lc_days in
     jan) lc_days=m01 ;;
     feb) lc_days=m02 ;;
     mar) lc_days=m03 ;;
     apr) lc_days=m04 ;;
     may) lc_days=m05 ;;
     jun) lc_days=m06 ;;
     jul) lc_days=m07 ;;
     aug) lc_days=m08 ;;
     sep) lc_days=m09 ;;
     oct) lc_days=m10 ;;
     nov) lc_days=m11 ;;
     dec) lc_days=m12 ;;
  esac

  year=yyy; mon=mm   # memory99=1

  nextjob=on
  noprint=on

  # ---Start_submit_ignore_code----

  stamp=`date "+%j%H%M%S"$$`

  # Use -e option if recognized by echo
  if [ "X`echo -e`" = "X-e" ]; then
    echo_e() { echo ${1+"$@"}; }
  else
    echo_e() { echo -e ${1+"$@"}; }
  fi

  # bail is a simple error exit routine
  # Note: we write the error directly to a file in ~/.queue so that this
  #       info is not lost if/when stdout is not returned
  error_out="$HOME/.queue/error_plotmix_${runid}_$stamp"
  [ ! -z "$error_out" ] && rm -f $error_out
  bail(){
    echo_e `date`" --- plotmix: $*"
    echo_e `date`" --- plotmix: $*" >>$error_out
    exit 1
  }

  # This invocation of make_file_name_list will process the *_year and *_months
  # variables defined above and output a file containing definitions for
  # start_year, start_mon, stop_year, stop_mon
  tmp_file_list="plotmix_date_list_${runid}_${stamp}"
  make_file_name_list --dates_only $tmp_file_list >>$error_out 2>&1 ||\
    bail "Problem in make_file_name_list"
  rm -f $error_out

  # Verify that the output list is not empty
  [ ! -s "$tmp_file_list" ] && bail "Unable to create file list"

  # A file list was created ...source it
  # This will define start_year, start_mon, stop_year, stop_mon
  : ; . $tmp_file_list
  rm -f $tmp_file_list

  # Define start and stop dates
  plotmix_start_year=$start_year
  plotmix_start_mon=$start_mon
  plotmix_stop_year=$stop_year
  plotmix_stop_mon=$stop_mon

  # ym_range is used in the definition of input (flabel) and
  # output (plpfn) file names as well as in the title of most plots.

  # define a range string in the form YYYYmMM_YYYYmMM to be used in file names
  ym_range="${plotmix_start_year}m${plotmix_start_mon}"
  ym_range="${ym_range}_${plotmix_stop_year}m${plotmix_stop_mon}"

  # In the special case that start and stop dates are identical use a YYYY format
  if [ $plotmix_start_year -eq $plotmix_stop_year ]; then
    if [ $plotmix_start_mon -eq $plotmix_stop_mon ]; then
      ym_range=$plotmix_start_year
    fi
  fi

  plotmix_prefix="${plotmix_uxxx}_${runid}"                       # memory99=1
  plotmix_prefix_=${plotmix_prefix}_                              # memory99=1
  plotmix_flabel_prefix=${plotmix_prefix_}                        # memory99=1
  plotmix_flabel=${plotmix_flabel_prefix}${ym_range}_${lc_days}_  # memory99=1

  # flabel will determine the names of the gp and xp files used
  # ie: files used are ${flabel}gp and ${flabel}xp
  flabel=$plotmix_flabel

  # plpfn is used as part of the resulting gmeta file name
  # The gmeta file name will be of the form uxxx_${plpfn}_${jobname}...
  plpfn=${run}_${lc_days}_${ym_range};

  # plunit is used to determine the disposition of the gmeta file
  plunit="VIC"

  # pldir is the full pathname into which the gmeta file will be copied
  pldir=''

  # resol is used as part of some file names defined here
  resol="128_64";
  case $resol in
     96_48) mask=gcm3_landmask_96x48v5  ;;
    128_64) mask=gcm3_landmask_128x64v5 ;;
    192_96) mask=gcm3_landmask_192x96v5 ;;
         *) echo "Unknown resolution $resol"
            exit 1 ;;
  esac

  # obsday is used as part of some file names here as well as in xfind
  # below, where it forms part of the superlabel that xfind looks for
  obsday=$days
  lc_obsday=`echo $obsday|tr '[A-Z]' '[a-z]'`
  case $lc_obsday in
    jan) lc_obsday=m01 ;;
    feb) lc_obsday=m02 ;;
    mar) lc_obsday=m03 ;;
    apr) lc_obsday=m04 ;;
    may) lc_obsday=m05 ;;
    jun) lc_obsday=m06 ;;
    jul) lc_obsday=m07 ;;
    aug) lc_obsday=m08 ;;
    sep) lc_obsday=m09 ;;
    oct) lc_obsday=m10 ;;
    nov) lc_obsday=m11 ;;
    dec) lc_obsday=m12 ;;
  esac

  obsfile="pd_era40_${resol}_198101_200012_${lc_obsday}_";
  obspcp_xa=pd_xie_arkin_1979_1998_pcpn_${resol}_${lc_obsday};
  obspcp_gpcp=pd_gpcp_month_1979_2005_pcpn_${resol}_${lc_obsday};
  obsisccp=pd_isccp_d2_1983_1997_cloud_data_${resol}_${lc_obsday};
  obsuwisc=pd_uwisc_1988_2007_lwp_${resol}_${lc_obsday};
  obsuwisc_msk=pd_uwisc_1988_2007_lwp_tmask_${resol}_${lc_obsday};
  obsssmi=pd_ssmi_1987_2007_${resol}_${lc_obsday};
  obsssmi_msk=pd_ssmi_1987_2007_tmask_${resol}_${lc_obsday};
  obsceres=pd_ceres_ebaf_toa_200003_200510_grid_${resol}_${lc_obsday};
  obsceres_msk=pd_ceres_ebaf_toa_200003_200510_grid_tmask_${resol}_${lc_obsday};
  ceres_cld=pd_ceres_2d_srbavg2_month_2000_2003_data_${resol}_${lc_obsday};
  warren_cld=pd_warren_cld_${resol};

  t1="        1"; t2="999999999"; t3="   1"; s3="   1";
  r1="        1"; r2="999999999"; r3="   1";
  g1="        1"; g2="999999999"; g3="   1";
  a1="        1"; a2="999999999"; a3="   1";
  lmx="  995";  d="B";
  lay="    2";   coord=" ET15"; topsig="-1.00"; moist=" QHYB"; plid="       50.";
  m01="  012";
  plv="   17"; p01="   10"; p02="   20"; p03="   30"; p04="   50"; p05="   70";
               p06="  100"; p07="  150"; p08="  200"; p09="  250"; p10="  300";
               p11="  400"; p12="  500"; p13="  600"; p14="  700"; p15="  850";
               p16="  925"; p17=" 1000"; p18="     "; p19="     "; p20="     ";
               p21="     "; p22="     "; p23="     "; p24="     "; p25="     ";
               p26="     "; p27="     "; p28="     "; p29="     "; p30="     ";
               p31="     "; p32="     "; p33="     "; p34="     "; p35="     ";
               p36="     "; p37="     "; p38="     "; p39="     "; p40="     ";
               p41="     "; p42="     "; p43="     "; p44="     "; p45="     ";
               p46="     "; p47="     "; p48="     "; p49="     "; p50="     ";

  kax="    1"; kin="    1"; lxp="    0"; map="    0"; b="  +";

  dtime="100"; gptime="600"; stime="600";

  memory1="100mb"; memory2="100mb"; memory3="120mb";

  case $resol in
     96_48)
      lrt="   47"; lmt="   47"; typ="    2"
      lon="   96"; lat="   48"; npg="    2"
      ncx="    2"; ncy="    2"
      delt="   1200.0";
      ;;
    128_64)
      lrt="   63"; lmt="   63"; typ="    2"
      lon="  128"; lat="   64"; npg="    2"
      ncx="    2"; ncy="    2"
      delt="    900.0"
      ;;
    192_96)
      lrt="   95"; lmt="   95"; typ="    2"
      lon="  192"; lat="   96"; npg="    2"
      ncx="    2"; ncy="    2"
      delt="    600.0"
      ;;
         *) echo "Unknown resolution $resol"
            exit 1 ;;
  esac

  # ptitle is a common prefix for all plot titles
  ym_range=`echo $ym_range|sed 's/_/-/g'`
  ptitle="$days $ym_range  $run "

#  * ............................ Condef Parameters ............................

  nocldo=on
  join=3
  gcm2plus=on
  gcmtsav=on
  datatype=specsig
  wxstats=on
  blckcld=on
  noprint=on
  nextjob=on
  debug=off
  obsdat=on
  xtradif=on
  colourplots=on
  shade=off
  stat2nd=off

  # ---Stop_submit_ignore_code----

#  * ............................. Deck Definition .............................

  . plotmixk.dk

#end_of_job
