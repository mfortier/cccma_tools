C #define COUPLED
C #define MPI
C
      SUBROUTINE XIT(NAME,N)
C
C     * JUL 02/09 - F.MAJAESS.- IN THE "MPI" CASE, ALLOW SOME 
C     *                         TIME DELAY BEFORE TERMINATING.
C     * DEC 23/08 - F.MAJAESS.- CALL "MPI_ABORT" IN THE "MPI" 
C     *                         MODE ANORMAL TERMINATION CASE.
C     * APR 24/06 - F.MAJAESS.- LIMIT "MPI_FINALIZE" CALL 
C                               UNDER "MPI" MODE TO JUST THE 
C                               NORMAL TERMINATION CASE.
C     * JAN 07/05 - M.LAZARE. - ADD "COUPLED" TO CONDITIONAL
C     *                         DIRECTIVES ALONG WITH "MPI".
C     * JAN 24/04 - F.MAJAESS. PREVIOUS VERSION XIT.
C 
C     * TERMINATES A PROGRAM BY PRINTING THE PROGRAM NAME AND 
C     * A LINE ACROSS THE PAGE FOLLOWED BY A NUMBER N. 
C 
C     * N.GE.0 IS FOR A NORMAL END. THE LINE IS DASHED. 
C     * NORMAL ENDS TERMINATE WITH   STOP.
C 
C     * N.LT.0 IS FOR AN ABNORMAL END. THE LINE IS DOTTED.
C     * IF N IS LESS THAN -100 THE PROGRAM SIMPLY TERMINATES. 
C     * OTHERWISE IF N IS LESS THAN ZERO THE PROGRAM ABORTS.
C 
#if defined MPI || defined COUPLED
      USE MPI
#endif
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      CHARACTER*(*) NAME
      CHARACTER*80  DASH, STAR
C
C     * COMMON BLOCK TO HOLD THE NUMBER OF NODES AND COMMUNICATOR.
C
      INTEGER*4 IERR
      INTEGER*4 MYNODE
      COMMON /MPINFO/ MYNODE
#if defined MPI || defined COUPLED
      INTEGER(4) :: NNODE, AGCM_COMMWRLD, MY_CMPLX_TYPE, MY_REAL_TYPE
      INTEGER(4) :: MY_INT_TYPE,N4
      COMMON /MPICOMM/ NNODE, AGCM_COMMWRLD, MY_CMPLX_TYPE,MY_REAL_TYPE,
     +                 MY_INT_TYPE
#endif
C 
      DATA DASH( 1:40)/'----------------------------------------'/
      DATA DASH(41:80)/'----------------------------------------'/
      DATA STAR( 1:40)/'****************************************'/
      DATA STAR(41:80)/'****************************************'/
C---------------------------------------------------------------------
C     * IN THE NORMAL TERMINATION CASE, AVOID DUPLICATE OUTPUT 
C     * AND LIMIT IT ONLY TO NODE=0.
C     
      IF (MYNODE.EQ.0 .OR. (N.LT.0 .AND. N.GE.-100) ) THEN

        LENAME=LEN_TRIM(NAME)
        IF (LENAME.LT.1) LENAME=1
  
        IF(N.GE.0) WRITE(6,6010) DASH(1:8),NAME(1:LENAME),
     1                           DASH(1:81-LENAME),N
C 
        IF(N.LT.0) WRITE(6,6010) STAR(1:8),NAME(1:LENAME),
     1                           STAR(1:81-LENAME),N

      ENDIF
C 
      IF ( N.GE.0 .OR. N.LT.-100 ) THEN

#if defined MPI || defined COUPLED
        CALL SYSTEM('sleep 3')
        CALL MPI_FINALIZE(IERR)
#endif
        STOP 
      ELSE
#if defined MPI || defined COUPLED
        N4=N
        CALL SYSTEM('sleep 3')
        CALL MPI_ABORT(AGCM_COMMWRLD,N4,IERR)
#endif
        CALL ABORT
      ENDIF
C 
C---------------------------------------------------------------------
 6008 FORMAT('0*** GCM RUN HAS COMPLETED SUCCESSFULLY ***') 
 6010 FORMAT('0',A8,'  END  ',A,1X,A,I8)
 9000 FORMAT(A4)
      END SUBROUTINE XIT
