.if !dEQ .ds EQ
.if !dEN .ds EN
.if !'\*(.T'ascii' .if !'\*(.T'html' .tm warning: eqn should have been given a `-T\*(.T' option
.if '\*(.T'html' .if !'ascii'ps' .tm warning: eqn should have been given a `-Tps' option
.if '\*(.T'html' .if !'ascii'ps' .tm warning: (it is advisable to invoke groff via: groff -Thtml -e)
.lf 1 /usr/share/groff/1.21/tmac/eqnrc
.\" Startup file for eqn.
.if !d EQ .ds EQ
.if !d EN .ds EN
.EQ
.nr 0C \n(.C
.cp 0
.ds 10
.cp \n(0C
.lf 65
.EN
.lf 1 -
.if !\n(.g .ab GNU tbl requires GNU troff.
.if !dTS .ds TS
.if !dTE .ds TE
.lf 1 -
.lf 1 udunits.3fi
." $Id: udunits.3fi,v 1.7 2003/08/29 18:30:33 steve Exp $
.TH UDUNITS 3f "$Date: 2003/08/29 18:30:33 $" "Printed: \n(yr.\n(mo.\n(dy" "UNIDATA LIBRARY FUNCTIONS"
.SH NAME
udunits, utopen, utmake, uttime, utorigin, utclr, utcpy, utorig, utscal, utmult, utinv, utdiv, utexp, utdec, utcaltime, uticaltime, utenc, utcvt, utfree, utcls \- Unidata units library
.SH SYNOPSIS
.nf
.ft B
f77 -I\fIunidata_inc\fP -L\fIunidata_lib\fP -ludunits ...
.sp
include udunits.inc
.sp
.ta \w'integer function 'u +\w'uticaltime 'u +\w'('u
integer function	utopen	(character*(*) path)
.sp
PTR function	utmake	()
.sp
integer function	uttime	(PTR unit)
.sp
integer function	utorigin	(PTR unit)
.sp
subroutine	utclr	(PTR unit)
.sp
subroutine	utcpy	(PTR source, PTR dest)
.sp
subroutine	utorig	(PTR source, 
			doubleprecision amount, 
			PTR result)
.sp
subroutine	utscal	(PTR source, 
			doubleprecision factor, 
			PTR result)
.sp
subroutine	utmult	(PTR term1, PTR term2, 
			PTR result)
.sp
subroutine	utinv	(PTR source, PTR result)
.sp
subroutine	utdiv	(PTR numer, PTR denom, 
			PTR result)
.sp
subroutine	utexp	(PTR source, integer power, 
			PTR result)
.sp
integer function	utdec	(character*(*) spec, PTR unit)
.sp
integer function	utcaltime	(doubleprecision value,
			PTR unit,
			integer year,
			integer month,
			integer day,
			integer hour,
			integer minute,
			real second)
.sp
integer function	uticaltime	(integer year,
			integer month,
			integer day,
			integer hour,
			integer minute,
			real second
			PTR unit,
			doubleprecision value)
.sp
integer function	utenc	(PTR unit, character*(*) spec)
.sp
integer function	utcvt	(PTR from, PTR to, 
			doubleprecision slope, 
			doubleprecision intercept)
.sp
subroutine	utfree	(PTR unit)
.sp
subroutine	utcls	()
.ft
.fi
.SH DESCRIPTION
.LP
The Unidata units library, \fBudunits\fP, supports conversion of unit
specifications between formatted and binary forms, arithmetic
manipulation of unit specifications, and conversion of values between 
compatible scales of measurement.
.LP
A unit is the amount by which a physical quantity is measured.  For example:
.sp
.TS
.nr 3c \n(.C
.cp 0
.nr 3lps \n[.s]
.de 3init
.ft \n[.f]
.ps \n[.s]
.vs \n[.v]u
.in \n[.i]u
.ll \n[.l]u
.ls \n[.L]
.ad \n[.j]
.ie \n[.u] .fi
.el .nf
.ce \n[.ce]
..
.nr 3ind \n[.i]
.nr 3fnt \n[.f]
.nr 3sz \n[.s]
.nr 3fll \n[.u]
.nr T. 0
.nr 3crow 0-1
.nr 3passed 0-1
.nr 3sflag 0
.ds 3trans
.ds 3quote
.nr 3brule 1
.nr 3supbot 0
.eo
.de 3rmk
.mk \$1
.if !'\n(.z'' \!.3rmk "\$1"
..
.de 3rvpt
.vpt \$1
.if !'\n(.z'' \!.3rvpt "\$1"
..
.de 3keep
.if '\n[.z]'' \{.ds 3quote \\
.ds 3trans \!
.di 3section
.nr 3sflag 1
.in 0
.\}
..
.de 3release
.if \n[3sflag] \{.di
.in \n[3ind]u
.nr 3dn \n[dn]
.ds 3quote
.ds 3trans
.nr 3sflag 0
.if \n[.t]<=\n[dn] \{.nr T. 1
.T#
.nr 3supbot 1
.sp \n[.t]u
.nr 3supbot 0
.mk #T
.\}
.if \n[.t]<=\n[3dn] .tm warning: page \n%: table text block will not fit on one page
.nf
.ls 1
.3section
.ls
.rm 3section
.\}
..
.nr 3tflag 0
.de 3tkeep
.if '\n[.z]'' \{.di 3table
.nr 3tflag 1
.\}
..
.de 3trelease
.if \n[3tflag] \{.br
.di
.nr 3dn \n[dn]
.ne \n[dn]u+\n[.V]u
.ie \n[.t]<=\n[3dn] .tm error: page \n%: table will not fit on one page; use .TS H/.TH with a supporting macro package
.el \{.in 0
.ls 1
.nf
.3table
.\}
.rm 3table
.\}
..
.ec
.ce 0
.nf
.nr 3sep 1n
.nr 3w0 \n(.H
.nr 3aw0 0
.nr 3lnw0 0
.nr 3rnw0 0
.nr 3w1 \n(.H
.nr 3aw1 0
.nr 3lnw1 0
.nr 3rnw1 0
.lf 88 udunits.3fi
.nr 3w0 \n[3w0]>?\w\[tbl]Phisical Quantity\[tbl]
.lf 88
.nr 3w1 \n[3w1]>?\w\[tbl]Possible Unit\[tbl]
.lf 90
.nr 3w0 \n[3w0]>?\w\[tbl]time\[tbl]
.lf 90
.nr 3w1 \n[3w1]>?\w\[tbl]weeks\[tbl]
.lf 91
.nr 3w0 \n[3w0]>?\w\[tbl]distance\[tbl]
.lf 91
.nr 3w1 \n[3w1]>?\w\[tbl]centimeters\[tbl]
.lf 92
.nr 3w0 \n[3w0]>?\w\[tbl]power\[tbl]
.lf 92
.nr 3w1 \n[3w1]>?\w\[tbl]watts\[tbl]
.nr 3w0 \n[3w0]>?(\n[3lnw0]+\n[3rnw0])
.if \n[3aw0] .nr 3w0 \n[3w0]>?(\n[3aw0]+2n)
.nr 3w1 \n[3w1]>?(\n[3lnw1]+\n[3rnw1])
.if \n[3aw1] .nr 3w1 \n[3w1]>?(\n[3aw1]+2n)
.nr 3expand \n[.l]-\n[.i]-\n[3w0]-\n[3w1]-3n
.if \n[3expand]<0 \{.lf 88
.tm1 "warning: file `\n[.F]', around line \n[.c]:
.tm1 "  table wider than line width
.nr 3expand 0
.\}
.nr 3cd0 0
.nr 3cl0 0*\n[3sep]
.nr 3ce0 \n[3cl0]+\n[3w0]
.nr 3cl1 \n[3ce0]+(3*\n[3sep])
.nr 3cd1 \n[3ce0]+\n[3cl1]/2
.nr 3ce1 \n[3cl1]+\n[3w1]
.nr 3cd2 \n[3ce1]+(0*\n[3sep])
.nr TW \n[3cd2]
.in +(u;\n[.l]-\n[.i]-\n[TW]/2>?-\n[.i])
.nr 3ind \n[.i]
.eo
.de T#
.if !\n[3supbot] \{.3rvpt 0
.mk 3vert
.ls 1
.ls
.nr 3passed \n[3crow]
.sp |\n[3vert]u
.3rvpt 1
.\}
..
.ec
.fc 
.3keep
.3rmk 3rt0
\*[3trans].nr 3crow 0
.3keep
.mk 3rs0
.mk 3bot
.3rvpt 0
.ta \n[3ce0]u \n[3ce1]u
.lf 88
\&\h'|\n[3cl0]u'Phisical Quantity\h'|\n[3cl1]u'Possible Unit
.nr 3bot \n[3bot]>?\n[.d]
.sp |\n[3rs0]u
.3rvpt 1
.sp |\n[3bot]u
\*[3trans].nr 3brule 1
.3release
.3keep
\*[3trans].nr 3crow 1
.3rmk 3rt1
.3keep
.mk 3rs1
.mk 3bot
.3rvpt 0
.vs 2p>?\n[.V]u
.ls 1
\&\v'.25m'\h'|\n[3cl0]u'\s[\n[3lps]]\D'l \n[3w0]u 0'\s0\h'|\n[3cl1]u'\s[\n[3lps]]\D'l \n[3w1]u 0'\s0
.ls
.vs
.nr 3bot \n[3bot]>?\n[.d]
.sp |\n[3rs1]u
.3rvpt 1
.sp |\n[3bot]u
\*[3trans].nr 3brule 1
.3release
.3keep
.3rmk 3rt2
\*[3trans].nr 3crow 2
.3keep
.mk 3rs2
.mk 3bot
.3rvpt 0
.ta \n[3ce0]u \n[3ce1]u
.lf 90
\&\h'|\n[3cl0]u'time\h'|\n[3cl1]u'weeks
.nr 3bot \n[3bot]>?\n[.d]
.sp |\n[3rs2]u
.3rvpt 1
.sp |\n[3bot]u
\*[3trans].nr 3brule 1
.3release
.3keep
.3rmk 3rt3
\*[3trans].nr 3crow 3
.3keep
.mk 3rs3
.mk 3bot
.3rvpt 0
.ta \n[3ce0]u \n[3ce1]u
.lf 91
\&\h'|\n[3cl0]u'distance\h'|\n[3cl1]u'centimeters
.nr 3bot \n[3bot]>?\n[.d]
.sp |\n[3rs3]u
.3rvpt 1
.sp |\n[3bot]u
\*[3trans].nr 3brule 1
.3release
.3keep
.3rmk 3rt4
\*[3trans].nr 3crow 4
.3keep
.mk 3rs4
.mk 3bot
.3rvpt 0
.ta \n[3ce0]u \n[3ce1]u
.lf 92
\&\h'|\n[3cl0]u'power\h'|\n[3cl1]u'watts
.nr 3bot \n[3bot]>?\n[.d]
.sp |\n[3rs4]u
.3rvpt 1
.sp |\n[3bot]u
\*[3trans].nr 3brule 1
.3release
.mk 3rt5
.nr 3brule 1
.nr T. 1
.T#
.3init
.fc
.cp \n(3c
.lf 93
.TE
.sp
A unit can have an origin associated with it \-\- in which case
the unit and origin together define a scale.  For example, the
phrase "the temperature is 25 degrees Celsius" specifies a particular point on a
measurement scale; whereas the phrase "the temperature difference is
25 degrees Celsius" specifies a unit with no origin and, hence, no associated
scale.  If not remembered, this subtle distinction can cause problems when
handling units.
.LP
Because the units library passes pointers to units structures and FORTRAN
77 has no pointer type, the word PTR in the above synopsis stands for
whatever FORTRAN type is appropriate for holding a pointer to a C structure
on the given platform.  This is necessarily platform-dependent and,
consequently, \fBIT IS UP TO THE USER TO DECLARE AND USE THE CORRECT FORTRAN 
DATA TYPE\fR.
.LP
.B utopen()
initializes the units package.  If \fIpath\fP is not empty,
then it specifies a units file containing initializing unit definitions;
otherwise, the environment variable \fBUDUNITS_PATH\fR is checked and,
if it exits and is not empty, then it is assumed to contain the pathname
of the units file;
otherwise, a compile-time default pathname is used.
.sp
The definitions in the units file are read into memory.
This function returns 
0 on success, 
UT_ENOFILE if the units file doesn't exist,
UT_ESYNTAX if the units file contains a syntax error,
UT_EUNKNOWN if the units file contains an unknown specification,
UT_EIO if an I/O error occurred while accessing the units file, and
UT_EALLOC if a memory allocation failure occurred.
.LP
.B utdec()
decodes the formatted unit specification \fIspec\fP into a binary
unit representation and stores the result in \fIunit\fP.
The binary representation is used for algebraic manipulation.
This function returns 0 on success,
UT_ENOINIT if the package hasn't been initialized,
UT_EUNKNOWN if the specification contains an unknown unit, and
UT_ESYNTAX if the specification contains a syntax error.
.LP
.B utcaltime()
converts the amount, \fIvalue\fP, of the temporal unit, \fIunit\fP,
into a UTC-referenced date and time
(see, however, the section on HANDLING TIME).
The reference unit shall be a time unit and have an origin.
This function returns 0 on success,
UT_ENOINIT if the package hasn't been initialized and
UT_EINVALID if the unit structure is not a temporal one.
.LP
.B uticaltime()
converts a UTC-referenced date and time into
the amount, \fIvalue\fP, of the temporal unit, \fIunit\fP
(see, however, the section on HANDLING TIME).
The reference unit shall be a time unit and have an origin.
This function returns 0 on success,
UT_ENOINIT if the package hasn't been initialized and
UT_EINVALID if the unit structure is not a temporal one.
.LP
.B utcvt()
returns the coefficients of the Galilean transformation (i.e. y = a*x + b)
necessary to convert the \fIfrom\fP unit into the \fIto\fP unit.
The units must be compatible (i.e., their quotient must be dimensionless).
On successful return, \fIslope\fP and \fIintercept\fP will contain the 
values for the slope and intercept coefficients, respectively.
This function returns
0 on success,
UT_ENOINIT if the package hasn't been initialized,
UT_EINVALID if one of the unit variables is invalid, and
UT_ECONVERT if the units are not convertable.
.LP
.B utenc()
encodes the binary unit variable \fIunit\fP
into a formatted unit specification and stores the string in \fIspec\fP.
This function returns
0 on success,
UT_ENOINIT if the package hasn't been initialized,
UT_EINVALID if the unit variable is invalid, and
UT_ENOROOM if the supplied character buffer is too small.
.LP
.B utclr()
clears a unit variable by setting it to the dimensionless scalar 1.
.LP
.B uttime()
returns 1 if the given unit variable refers to a time unit; 0
otherwise.
This function ignores whether or not the unit has an origin.
.LP
.B utorigin()
returns 1 if the given unit variable has an origin (i.e. defines a scale)
and 0 otherwise.
.LP
.B utcpy()
copies the unit variable \fIsource\fP to the unit variable
\fIdest\fP.
This function correctly handles the case where the same unit variable
is referenced by the source and destination units.
.LP
.B utscal()
scales the unit variable \fIsource\fP by the multiplicative scalar
\fIfactor\fP, storing the result in the unit variable \fIresult\fP.
This function correctly handles the case where the same unit variable
is referenced by the source and result units.
.LP
.B utinv()
inverts the unit variable \fIsource\fP, storing the result in
unit variable \fIresult\fP.
Multiplying a unit by its reciprocal yields the
dimensionless scalar 1.
This function correctly handles the case where the source and result
unit refer to the same variable.
.LP
.B utdiv()
divides unit variable \fInumer\fP by unit variable \fIdenom\fP
and stores the result in unit variable \fIresult\fP.
This function correctly handles the case where the same unit variable
is referenced by two or more arguments.
.LP
.B utmult()
multiplies unit variable \fIterm1\fP by unit variable \fIterm2\fP
and stores the result in unit variable \fIresult\fP.
This function correctly handles the case where the same unit variable
is referenced by two or more arguments.
.LP
.B utexp()
raises the unit variable \fIsource\fP by the power \fPpower\fP,
storing the result in the unit variable \fIresult\fP.
This function correctly handles the case where the same unit variable
is referenced by the source and result units.
.LP
.B utcls()
terminates usage of this package.  In particular, it frees all allocated
memory.  It should be called when the library is no longer needed.
.LP
.B utmake()
Creates a new unit variable.  The value returned by this function may be
used in subsequent calls.  The unit variable is cleared by a call to utclr.
.LP
.B utfree()
Frees the unit variable \fIunit\fP which was returned by a previous call to
\fButmake()\fP.
.SH "HANDLING TIME"
.LP
The \fBudunits\fP(3) package uses a mixed Gregorian/Julian calendar
system.
Dates prior to 1582-10-15 are assumed to use the Julian calendar, which
was introduced by Julius Caesar in 46 BCE and is based on a year that 
is exactly 365.25 days long.
Dates on and after 1582-10-15 are assumed to use the Gregorian calendar,
which was introduced on that date and is based on a year that is
exactly 365.2425 days long.
(A year is actually approximately 365.242198781 days long.)
Seemingly strange behavior of the \fBudunits\fP(3) package can
result if a user-given time interval includes the changeover date.
For example, \fButCalendar\fP() and \fButInvCalendar\fP() can be used
to show that 1582-10-15 *preceeded* 1582-10-14 by 9 days.
.SH "EXAMPLES"
.LP
In the following, it is assumed that a FORTRAN INTEGER data type is
appropriate for storing a pointer to a C structure.  This assumption is
valid for most 32-bit architectures but is invalid for most
64-bit architectures that have 32-bit INTEGERs (e.g. a DEC Alpha).
.LP
Convert two data sets to
a common unit, subtract one from the other, then
save the result in a (different) output unit:
.sp
.RS +4
.nf
      INTEGER   UTOPEN
      ...
      IF (UTOPEN('') .NE. 0) THEN
C         Handle initialization error
      ELSE
          CHARACTER*80        UNITSTRING1, UNITSTRING2
          CHARACTER*80        OUTPUTUNITSTRING
          INTEGER             UNIT1, UNIT2, OUTPUTUNIT
          INTEGER             UTMAKE, UTDEC
          ...
          UNIT1       = UTMAKE()
          UNIT2       = UTMAKE()
          OUTPUTUNIT  = UTMAKE()
          IF (UTDEC(UNITSTRING1, UNIT1) .NE. 0 .OR.
     *        UTDEC(UNITSTRING2, UNIT2) .NE. 0 .OR.
     *        UTDEC(OUTPUTUNITSTRING2, OUTPUTUNIT) .ne. 0) THEN
C
C             Handle decode error
C
          ELSE
              DOUBLEPRECISION INSLOPE, ININTERCEPT
              DOUBLEPRECISION OUTSLOPE, OUTINTERCEPT

              IF (UTCVT(UNIT2, UNIT1, INSLOPE, ININTERCEPT) .NE. 0 
     *            .OR.  UTCVT(UNIT1, OUTPUTUNIT, OUTSLOPE,
     *                        OUTINTERCEPT) .NE. 0) THEN
C
C                 Handle data-incompatibility
C
              ELSE
C                 Process data using:
C                     OUTPUTVALUE = OUTSLOPE*(DATA1VALUE
C                                   - (INSLOPE*DATA2VALUE
     *                                 + ININTERCEPT))
C                                   + OUTINTERCEPT
              ENDIF
              CALL UTFREE(UNIT1)
              CALL UTFREE(UNIT2)
              CALL UTFREE(OUTPUTUNIT)
              CALL UTCLS
          ENDIF
      ENDIF
.fi
.RE
.LP
the above example could be made more efficient
by testing the returned conversion factors for nearness to 1 and 0 and using
appropriately streamlined processing expressions.
.sp
.LP
Compute a threshold value corresponding to an input data value plus a
user-specified delta (the units of the input data value and delta can
differ):
.sp
.RS +4
.nf
      INTEGER         INPUT_UNIT, DELTA_UNIT
      INTEGER         UTOPEN, UTMAKE, UTDEC
      CHARACTER*(80)  INPUT_UNIT_STRING, DELTA_UNIT_STRING
      ...
      INPUT_UNIT      = UTMAKE()
      DELTA_UNIT      = UTMAKE()

      CALL UTOPEN('udunits.dat')
      IF (UTDEC(INPUT_UNIT_STRING, INPUT_UNIT) .NE. 0 .OR.
          UTDEC(DELTA_UNIT_STRING, DELTA_UNIT) .NE. 0) THEN
C
C         Handle decode error
C
      ELSE
          DOUBLEPRECISION     SLOPE, INTERCEPT
          REAL                DELTA_VALUE
          ...
          IF (UTCVT(DELTA_UNIT, INPUT_UNIT, SLOPE, INTERCEPT)
     *        .NE. 0) THEN
C
C             Handle units incompatibility
C
          ELSE
              REAL    INPUT_VALUE
              REAL    THRESHOLD
              ...
              THRESHOLD = INPUT_VALUE + SLOPE*DELTA_VALUE + INTERCEPT
          ENDIF
      ENDIF
      CALL UTCLS
.fi
.RE
.sp
.LP
Compute the number of time intervals from a start time to a 
reference time.  
\fIPTR is a placeholder for the FORTRAN data type equivalent to
a memory address.
.sp
.RS +4
.nf
.lf 1 testcal2.f
      IMPLICIT          NONE
      REAL              REF_SECOND      / 0 /
      INTEGER           UTOPEN
      INTEGER           UTDEC
      INTEGER           UTICALTIME
      INTEGER           STATUS
      INTEGER           REF_YEAR        / 1990 /
      INTEGER           REF_MONTH       / 1 /
      INTEGER           REF_DAY         / 1 /
      INTEGER           REF_HOUR        / 1 /
      INTEGER           REF_MINUTE      / 0 /
      INTEGER           UTMAKE
      INTEGER           TIMECENTERS_UNIT
      DOUBLEPRECISION   REF_VALUE

      STATUS = UTOPEN('udunits.dat')
      IF (STATUS .NE. 0) THEN
          PRINT *, 'Couldn''t open database: status =', STATUS
          CALL ABORT
      ENDIF

      TIMECENTERS_UNIT = UTMAKE()

      STATUS = UTDEC('2 minutes since 1990-1-1', TIMECENTERS_UNIT)
      IF (STATUS .NE. 0)
     *THEN
          PRINT *, 'UTDEC() =', STATUS
      ELSE
C
C         Reference time is start time plus 1 hour.
C
          STATUS = UTICALTIME(REF_YEAR, REF_MONTH, REF_DAY, REF_HOUR, 
     *                    REF_MINUTE, REF_SECOND, TIMECENTERS_UNIT,
     *                    REF_VALUE)
C
C         Number of time intervals between start and reference times:
C
          IF (STATUS .NE. 0) THEN
              PRINT *, 'UTICALTIME() =', STATUS
          ELSE
              IF (30 .NE. REF_VALUE) THEN
                  PRINT *, 'Incorrect result:', REF_VALUE
              ELSE
                  PRINT *, 'Correct result'
              ENDIF
          ENDIF
      ENDIF
      END
.lf 362 udunits.3fi
.sp
.fi
.RE
.SH "FORMATTED UNIT SPECIFICATIONS"
.LP
The following are examples of formatted unit specifications that can be
interpreted by the 
.B utScan()
function:
.sp
.RS +4
.nf
10 kilogram.meters/seconds2
10 kg-m/sec2
10 kg\ m/s^2
(PI\ radian)2
degF
100rpm
geopotential meters
33 feet water
.fi
.RE
.LP
A unit is specified as an arbitrary product of constants and unit names
raised to arbitrary integral powers.
Division is indicated by a slash `/'.
Multiplication is indicated by whitespace, a period `.', or a hyphen `-'.
Exponentiation is indicated by an
integer suffix or by the exponentiation operators `^' and `**'.
Parentheses may be used for grouping and disambiguation.
.LP
Arbitrary Galilean transformations (i.e. y = ax + b) are supported.
In particular, temperature and time conversions are correctly handled.
The specification:
.sp
.RS
degF @ 32
.RE
.sp
indicates a Fahrenheit scale with the origin shifted to thirty-two
degrees Fahrenheit (i.e. to zero degrees Celsius).
The Celsius scale is equivalent to the following unit:
.sp
.RS
.nf
1.8 degR @ 273.15
.fi
.RE
.sp
Besides the character `\fB@\fP', the words `\fBafter\fP', `\fBfrom\fP',
`\fBref\fP', and `\fBsince\fP' may also be used.
Note that multiplication takes precedence over origin-shift.
In order of increasing precedence, the operations are origin-shift, division,
multiplication, and exponentiation.
.LP
Units of time are similarly handled.
The specification:
.sp
.RS
.nf
seconds since 1992-10-8 15:15:42.5 -6:00
.fi
.RE
.sp
indicates seconds since October 8th, 1992 at 3 hours, 15 minutes and 42.5
seconds
in the afternoon in the time zone which is six hours to the west of
Coordinated Universal Time (i.e. Mountain Daylight Time).
The time zone specification
can also be written without a colon using one or two-digits (indicating
hours) or three or four digits (indicating hours and minutes).
.LP
.B utScan()
understands most conventional prefixes and abbreviations:
.sp
.TS
.nr 3c \n(.C
.cp 0
.nr 3lps \n[.s]
.de 3init
.ft \n[.f]
.ps \n[.s]
.vs \n[.v]u
.in \n[.i]u
.ll \n[.l]u
.ls \n[.L]
.ad \n[.j]
.ie \n[.u] .fi
.el .nf
.ce \n[.ce]
..
.nr 3ind \n[.i]
.nr 3fnt \n[.f]
.nr 3sz \n[.s]
.nr 3fll \n[.u]
.nr T. 0
.nr 3crow 0-1
.nr 3passed 0-1
.nr 3sflag 0
.ds 3trans
.ds 3quote
.nr 3brule 1
.nr 3supbot 0
.eo
.de 3rmk
.mk \$1
.if !'\n(.z'' \!.3rmk "\$1"
..
.de 3rvpt
.vpt \$1
.if !'\n(.z'' \!.3rvpt "\$1"
..
.de 3keep
.if '\n[.z]'' \{.ds 3quote \\
.ds 3trans \!
.di 3section
.nr 3sflag 1
.in 0
.\}
..
.de 3release
.if \n[3sflag] \{.di
.in \n[3ind]u
.nr 3dn \n[dn]
.ds 3quote
.ds 3trans
.nr 3sflag 0
.if \n[.t]<=\n[dn] \{.nr T. 1
.T#
.nr 3supbot 1
.sp \n[.t]u
.nr 3supbot 0
.mk #T
.\}
.if \n[.t]<=\n[3dn] .tm warning: page \n%: table text block will not fit on one page
.nf
.ls 1
.3section
.ls
.rm 3section
.\}
..
.nr 3tflag 0
.de 3tkeep
.if '\n[.z]'' \{.di 3table
.nr 3tflag 1
.\}
..
.de 3trelease
.if \n[3tflag] \{.br
.di
.nr 3dn \n[dn]
.ne \n[dn]u+\n[.V]u
.ie \n[.t]<=\n[3dn] .tm error: page \n%: table will not fit on one page; use .TS H/.TH with a supporting macro package
.el \{.in 0
.ls 1
.nf
.3table
.\}
.rm 3table
.\}
..
.ec
.ce 0
.nf
.nr 3sep 1n
.nr 3w0 \n(.H
.nr 3aw0 0
.nr 3lnw0 0
.nr 3rnw0 0
.nr 3w1 \n(.H
.nr 3aw1 0
.nr 3lnw1 0
.nr 3rnw1 0
.nr 3w2 \n(.H
.nr 3aw2 0
.nr 3lnw2 0
.nr 3rnw2 0
.lf 441 udunits.3fi
.nr 3w0 \n[3w0]>?\w\[tbl]Factor\[tbl]
.lf 441
.nr 3w1 \n[3w1]>?\w\[tbl]Prefix\[tbl]
.lf 441
.nr 3w2 \n[3w2]>?\w\[tbl]Abbreviation\[tbl]
.lf 443
.nr 3w0 \n[3w0]>?\w\[tbl]1e24\[tbl]
.lf 443
.nr 3w1 \n[3w1]>?\w\[tbl]yotta\[tbl]
.lf 443
.nr 3w2 \n[3w2]>?\w\[tbl]Y\[tbl]
.lf 444
.nr 3w0 \n[3w0]>?\w\[tbl]1e21\[tbl]
.lf 444
.nr 3w1 \n[3w1]>?\w\[tbl]zetta\[tbl]
.lf 444
.nr 3w2 \n[3w2]>?\w\[tbl]Z\[tbl]
.lf 445
.nr 3w0 \n[3w0]>?\w\[tbl]1e18\[tbl]
.lf 445
.nr 3w1 \n[3w1]>?\w\[tbl]exa\[tbl]
.lf 445
.nr 3w2 \n[3w2]>?\w\[tbl]E\[tbl]
.lf 446
.nr 3w0 \n[3w0]>?\w\[tbl]1e15\[tbl]
.lf 446
.nr 3w1 \n[3w1]>?\w\[tbl]peta\[tbl]
.lf 446
.nr 3w2 \n[3w2]>?\w\[tbl]P\[tbl]
.lf 447
.nr 3w0 \n[3w0]>?\w\[tbl]1e12\[tbl]
.lf 447
.nr 3w1 \n[3w1]>?\w\[tbl]tera\[tbl]
.lf 447
.nr 3w2 \n[3w2]>?\w\[tbl]T\[tbl]
.lf 448
.nr 3w0 \n[3w0]>?\w\[tbl]1e9\[tbl]
.lf 448
.nr 3w1 \n[3w1]>?\w\[tbl]giga\[tbl]
.lf 448
.nr 3w2 \n[3w2]>?\w\[tbl]G\[tbl]
.lf 449
.nr 3w0 \n[3w0]>?\w\[tbl]1e6\[tbl]
.lf 449
.nr 3w1 \n[3w1]>?\w\[tbl]mega\[tbl]
.lf 449
.nr 3w2 \n[3w2]>?\w\[tbl]M\[tbl]
.lf 450
.nr 3w0 \n[3w0]>?\w\[tbl]1e3\[tbl]
.lf 450
.nr 3w1 \n[3w1]>?\w\[tbl]kilo\[tbl]
.lf 450
.nr 3w2 \n[3w2]>?\w\[tbl]k\[tbl]
.lf 451
.nr 3w0 \n[3w0]>?\w\[tbl]1e2\[tbl]
.lf 451
.nr 3w1 \n[3w1]>?\w\[tbl]hecto\[tbl]
.lf 451
.nr 3w2 \n[3w2]>?\w\[tbl]h\[tbl]
.lf 452
.nr 3w0 \n[3w0]>?\w\[tbl]1e1\[tbl]
.lf 452
.nr 3w1 \n[3w1]>?\w\[tbl]deca, deka\[tbl]
.lf 452
.nr 3w2 \n[3w2]>?\w\[tbl]da\[tbl]
.lf 453
.nr 3w0 \n[3w0]>?\w\[tbl]1e-1\[tbl]
.lf 453
.nr 3w1 \n[3w1]>?\w\[tbl]deci\[tbl]
.lf 453
.nr 3w2 \n[3w2]>?\w\[tbl]d\[tbl]
.lf 454
.nr 3w0 \n[3w0]>?\w\[tbl]1e-2\[tbl]
.lf 454
.nr 3w1 \n[3w1]>?\w\[tbl]centi\[tbl]
.lf 454
.nr 3w2 \n[3w2]>?\w\[tbl]c\[tbl]
.lf 455
.nr 3w0 \n[3w0]>?\w\[tbl]1e-3\[tbl]
.lf 455
.nr 3w1 \n[3w1]>?\w\[tbl]milli\[tbl]
.lf 455
.nr 3w2 \n[3w2]>?\w\[tbl]m\[tbl]
.lf 456
.nr 3w0 \n[3w0]>?\w\[tbl]1e-6\[tbl]
.lf 456
.nr 3w1 \n[3w1]>?\w\[tbl]micro\[tbl]
.lf 456
.nr 3w2 \n[3w2]>?\w\[tbl]u\[tbl]
.lf 457
.nr 3w0 \n[3w0]>?\w\[tbl]1e-9\[tbl]
.lf 457
.nr 3w1 \n[3w1]>?\w\[tbl]nano\[tbl]
.lf 457
.nr 3w2 \n[3w2]>?\w\[tbl]n\[tbl]
.lf 458
.nr 3w0 \n[3w0]>?\w\[tbl]1e-12\[tbl]
.lf 458
.nr 3w1 \n[3w1]>?\w\[tbl]pico\[tbl]
.lf 458
.nr 3w2 \n[3w2]>?\w\[tbl]p\[tbl]
.lf 459
.nr 3w0 \n[3w0]>?\w\[tbl]1e-15\[tbl]
.lf 459
.nr 3w1 \n[3w1]>?\w\[tbl]femto\[tbl]
.lf 459
.nr 3w2 \n[3w2]>?\w\[tbl]f\[tbl]
.lf 460
.nr 3w0 \n[3w0]>?\w\[tbl]1e-18\[tbl]
.lf 460
.nr 3w1 \n[3w1]>?\w\[tbl]atto\[tbl]
.lf 460
.nr 3w2 \n[3w2]>?\w\[tbl]a\[tbl]
.lf 461
.nr 3w0 \n[3w0]>?\w\[tbl]1e-21\[tbl]
.lf 461
.nr 3w1 \n[3w1]>?\w\[tbl]zepto\[tbl]
.lf 461
.nr 3w2 \n[3w2]>?\w\[tbl]z\[tbl]
.lf 462
.nr 3w0 \n[3w0]>?\w\[tbl]1e-24\[tbl]
.lf 462
.nr 3w1 \n[3w1]>?\w\[tbl]yocto\[tbl]
.lf 462
.nr 3w2 \n[3w2]>?\w\[tbl]y\[tbl]
.nr 3w0 \n[3w0]>?(\n[3lnw0]+\n[3rnw0])
.if \n[3aw0] .nr 3w0 \n[3w0]>?(\n[3aw0]+2n)
.nr 3w1 \n[3w1]>?(\n[3lnw1]+\n[3rnw1])
.if \n[3aw1] .nr 3w1 \n[3w1]>?(\n[3aw1]+2n)
.nr 3w2 \n[3w2]>?(\n[3lnw2]+\n[3rnw2])
.if \n[3aw2] .nr 3w2 \n[3w2]>?(\n[3aw2]+2n)
.nr 3expand \n[.l]-\n[.i]-\n[3w0]-\n[3w1]-\n[3w2]-6n
.if \n[3expand]<0 \{.lf 441
.tm1 "warning: file `\n[.F]', around line \n[.c]:
.tm1 "  table wider than line width
.nr 3expand 0
.\}
.nr 3cd0 0
.nr 3cl0 0*\n[3sep]
.nr 3ce0 \n[3cl0]+\n[3w0]
.nr 3cl1 \n[3ce0]+(3*\n[3sep])
.nr 3cd1 \n[3ce0]+\n[3cl1]/2
.nr 3ce1 \n[3cl1]+\n[3w1]
.nr 3cl2 \n[3ce1]+(3*\n[3sep])
.nr 3cd2 \n[3ce1]+\n[3cl2]/2
.nr 3ce2 \n[3cl2]+\n[3w2]
.nr 3cd3 \n[3ce2]+(0*\n[3sep])
.nr TW \n[3cd3]
.in +(u;\n[.l]-\n[.i]-\n[TW]/2>?-\n[.i])
.nr 3ind \n[.i]
.eo
.de T#
.if !\n[3supbot] \{.3rvpt 0
.mk 3vert
.ls 1
.ls
.nr 3passed \n[3crow]
.sp |\n[3vert]u
.3rvpt 1
.\}
..
.ec
.fc 
.3keep
.3rmk 3rt0
\*[3trans].nr 3crow 0
.3keep
.mk 3rs0
.mk 3bot
.3rvpt 0
.ta \n[3ce0]u \n[3ce1]u \n[3ce2]u
.lf 441
\&\h'|\n[3cl0]u'Factor\h'|\n[3cl1]u'Prefix\h'|\n[3cl2]u'Abbreviation
.nr 3bot \n[3bot]>?\n[.d]
.sp |\n[3rs0]u
.3rvpt 1
.sp |\n[3bot]u
\*[3trans].nr 3brule 1
.3release
.3keep
\*[3trans].nr 3crow 1
.3rmk 3rt1
.3keep
.mk 3rs1
.mk 3bot
.3rvpt 0
.vs 2p>?\n[.V]u
.ls 1
\&\v'.25m'\h'|\n[3cl0]u'\s[\n[3lps]]\D'l \n[3w0]u 0'\s0\h'|\n[3cl1]u'\s[\n[3lps]]\D'l \n[3w1]u 0'\s0\h'|\n[3cl2]u'\s[\n[3lps]]\D'l \n[3w2]u 0'\s0
.ls
.vs
.nr 3bot \n[3bot]>?\n[.d]
.sp |\n[3rs1]u
.3rvpt 1
.sp |\n[3bot]u
\*[3trans].nr 3brule 1
.3release
.3keep
.3rmk 3rt2
\*[3trans].nr 3crow 2
.3keep
.mk 3rs2
.mk 3bot
.3rvpt 0
.ta \n[3ce0]u \n[3ce1]u \n[3ce2]u
.lf 443
\&\h'|\n[3cl0]u'1e24\h'|\n[3cl1]u'yotta\h'|\n[3cl2]u'Y
.nr 3bot \n[3bot]>?\n[.d]
.sp |\n[3rs2]u
.3rvpt 1
.sp |\n[3bot]u
\*[3trans].nr 3brule 1
.3release
.3keep
.3rmk 3rt3
\*[3trans].nr 3crow 3
.3keep
.mk 3rs3
.mk 3bot
.3rvpt 0
.ta \n[3ce0]u \n[3ce1]u \n[3ce2]u
.lf 444
\&\h'|\n[3cl0]u'1e21\h'|\n[3cl1]u'zetta\h'|\n[3cl2]u'Z
.nr 3bot \n[3bot]>?\n[.d]
.sp |\n[3rs3]u
.3rvpt 1
.sp |\n[3bot]u
\*[3trans].nr 3brule 1
.3release
.3keep
.3rmk 3rt4
\*[3trans].nr 3crow 4
.3keep
.mk 3rs4
.mk 3bot
.3rvpt 0
.ta \n[3ce0]u \n[3ce1]u \n[3ce2]u
.lf 445
\&\h'|\n[3cl0]u'1e18\h'|\n[3cl1]u'exa\h'|\n[3cl2]u'E
.nr 3bot \n[3bot]>?\n[.d]
.sp |\n[3rs4]u
.3rvpt 1
.sp |\n[3bot]u
\*[3trans].nr 3brule 1
.3release
.3keep
.3rmk 3rt5
\*[3trans].nr 3crow 5
.3keep
.mk 3rs5
.mk 3bot
.3rvpt 0
.ta \n[3ce0]u \n[3ce1]u \n[3ce2]u
.lf 446
\&\h'|\n[3cl0]u'1e15\h'|\n[3cl1]u'peta\h'|\n[3cl2]u'P
.nr 3bot \n[3bot]>?\n[.d]
.sp |\n[3rs5]u
.3rvpt 1
.sp |\n[3bot]u
\*[3trans].nr 3brule 1
.3release
.3keep
.3rmk 3rt6
\*[3trans].nr 3crow 6
.3keep
.mk 3rs6
.mk 3bot
.3rvpt 0
.ta \n[3ce0]u \n[3ce1]u \n[3ce2]u
.lf 447
\&\h'|\n[3cl0]u'1e12\h'|\n[3cl1]u'tera\h'|\n[3cl2]u'T
.nr 3bot \n[3bot]>?\n[.d]
.sp |\n[3rs6]u
.3rvpt 1
.sp |\n[3bot]u
\*[3trans].nr 3brule 1
.3release
.3keep
.3rmk 3rt7
\*[3trans].nr 3crow 7
.3keep
.mk 3rs7
.mk 3bot
.3rvpt 0
.ta \n[3ce0]u \n[3ce1]u \n[3ce2]u
.lf 448
\&\h'|\n[3cl0]u'1e9\h'|\n[3cl1]u'giga\h'|\n[3cl2]u'G
.nr 3bot \n[3bot]>?\n[.d]
.sp |\n[3rs7]u
.3rvpt 1
.sp |\n[3bot]u
\*[3trans].nr 3brule 1
.3release
.3keep
.3rmk 3rt8
\*[3trans].nr 3crow 8
.3keep
.mk 3rs8
.mk 3bot
.3rvpt 0
.ta \n[3ce0]u \n[3ce1]u \n[3ce2]u
.lf 449
\&\h'|\n[3cl0]u'1e6\h'|\n[3cl1]u'mega\h'|\n[3cl2]u'M
.nr 3bot \n[3bot]>?\n[.d]
.sp |\n[3rs8]u
.3rvpt 1
.sp |\n[3bot]u
\*[3trans].nr 3brule 1
.3release
.3keep
.3rmk 3rt9
\*[3trans].nr 3crow 9
.3keep
.mk 3rs9
.mk 3bot
.3rvpt 0
.ta \n[3ce0]u \n[3ce1]u \n[3ce2]u
.lf 450
\&\h'|\n[3cl0]u'1e3\h'|\n[3cl1]u'kilo\h'|\n[3cl2]u'k
.nr 3bot \n[3bot]>?\n[.d]
.sp |\n[3rs9]u
.3rvpt 1
.sp |\n[3bot]u
\*[3trans].nr 3brule 1
.3release
.3keep
.3rmk 3rt10
\*[3trans].nr 3crow 10
.3keep
.mk 3rs10
.mk 3bot
.3rvpt 0
.ta \n[3ce0]u \n[3ce1]u \n[3ce2]u
.lf 451
\&\h'|\n[3cl0]u'1e2\h'|\n[3cl1]u'hecto\h'|\n[3cl2]u'h
.nr 3bot \n[3bot]>?\n[.d]
.sp |\n[3rs10]u
.3rvpt 1
.sp |\n[3bot]u
\*[3trans].nr 3brule 1
.3release
.3keep
.3rmk 3rt11
\*[3trans].nr 3crow 11
.3keep
.mk 3rs11
.mk 3bot
.3rvpt 0
.ta \n[3ce0]u \n[3ce1]u \n[3ce2]u
.lf 452
\&\h'|\n[3cl0]u'1e1\h'|\n[3cl1]u'deca, deka\h'|\n[3cl2]u'da
.nr 3bot \n[3bot]>?\n[.d]
.sp |\n[3rs11]u
.3rvpt 1
.sp |\n[3bot]u
\*[3trans].nr 3brule 1
.3release
.3keep
.3rmk 3rt12
\*[3trans].nr 3crow 12
.3keep
.mk 3rs12
.mk 3bot
.3rvpt 0
.ta \n[3ce0]u \n[3ce1]u \n[3ce2]u
.lf 453
\&\h'|\n[3cl0]u'1e-1\h'|\n[3cl1]u'deci\h'|\n[3cl2]u'd
.nr 3bot \n[3bot]>?\n[.d]
.sp |\n[3rs12]u
.3rvpt 1
.sp |\n[3bot]u
\*[3trans].nr 3brule 1
.3release
.3keep
.3rmk 3rt13
\*[3trans].nr 3crow 13
.3keep
.mk 3rs13
.mk 3bot
.3rvpt 0
.ta \n[3ce0]u \n[3ce1]u \n[3ce2]u
.lf 454
\&\h'|\n[3cl0]u'1e-2\h'|\n[3cl1]u'centi\h'|\n[3cl2]u'c
.nr 3bot \n[3bot]>?\n[.d]
.sp |\n[3rs13]u
.3rvpt 1
.sp |\n[3bot]u
\*[3trans].nr 3brule 1
.3release
.3keep
.3rmk 3rt14
\*[3trans].nr 3crow 14
.3keep
.mk 3rs14
.mk 3bot
.3rvpt 0
.ta \n[3ce0]u \n[3ce1]u \n[3ce2]u
.lf 455
\&\h'|\n[3cl0]u'1e-3\h'|\n[3cl1]u'milli\h'|\n[3cl2]u'm
.nr 3bot \n[3bot]>?\n[.d]
.sp |\n[3rs14]u
.3rvpt 1
.sp |\n[3bot]u
\*[3trans].nr 3brule 1
.3release
.3keep
.3rmk 3rt15
\*[3trans].nr 3crow 15
.3keep
.mk 3rs15
.mk 3bot
.3rvpt 0
.ta \n[3ce0]u \n[3ce1]u \n[3ce2]u
.lf 456
\&\h'|\n[3cl0]u'1e-6\h'|\n[3cl1]u'micro\h'|\n[3cl2]u'u
.nr 3bot \n[3bot]>?\n[.d]
.sp |\n[3rs15]u
.3rvpt 1
.sp |\n[3bot]u
\*[3trans].nr 3brule 1
.3release
.3keep
.3rmk 3rt16
\*[3trans].nr 3crow 16
.3keep
.mk 3rs16
.mk 3bot
.3rvpt 0
.ta \n[3ce0]u \n[3ce1]u \n[3ce2]u
.lf 457
\&\h'|\n[3cl0]u'1e-9\h'|\n[3cl1]u'nano\h'|\n[3cl2]u'n
.nr 3bot \n[3bot]>?\n[.d]
.sp |\n[3rs16]u
.3rvpt 1
.sp |\n[3bot]u
\*[3trans].nr 3brule 1
.3release
.3keep
.3rmk 3rt17
\*[3trans].nr 3crow 17
.3keep
.mk 3rs17
.mk 3bot
.3rvpt 0
.ta \n[3ce0]u \n[3ce1]u \n[3ce2]u
.lf 458
\&\h'|\n[3cl0]u'1e-12\h'|\n[3cl1]u'pico\h'|\n[3cl2]u'p
.nr 3bot \n[3bot]>?\n[.d]
.sp |\n[3rs17]u
.3rvpt 1
.sp |\n[3bot]u
\*[3trans].nr 3brule 1
.3release
.3keep
.3rmk 3rt18
\*[3trans].nr 3crow 18
.3keep
.mk 3rs18
.mk 3bot
.3rvpt 0
.ta \n[3ce0]u \n[3ce1]u \n[3ce2]u
.lf 459
\&\h'|\n[3cl0]u'1e-15\h'|\n[3cl1]u'femto\h'|\n[3cl2]u'f
.nr 3bot \n[3bot]>?\n[.d]
.sp |\n[3rs18]u
.3rvpt 1
.sp |\n[3bot]u
\*[3trans].nr 3brule 1
.3release
.3keep
.3rmk 3rt19
\*[3trans].nr 3crow 19
.3keep
.mk 3rs19
.mk 3bot
.3rvpt 0
.ta \n[3ce0]u \n[3ce1]u \n[3ce2]u
.lf 460
\&\h'|\n[3cl0]u'1e-18\h'|\n[3cl1]u'atto\h'|\n[3cl2]u'a
.nr 3bot \n[3bot]>?\n[.d]
.sp |\n[3rs19]u
.3rvpt 1
.sp |\n[3bot]u
\*[3trans].nr 3brule 1
.3release
.3keep
.3rmk 3rt20
\*[3trans].nr 3crow 20
.3keep
.mk 3rs20
.mk 3bot
.3rvpt 0
.ta \n[3ce0]u \n[3ce1]u \n[3ce2]u
.lf 461
\&\h'|\n[3cl0]u'1e-21\h'|\n[3cl1]u'zepto\h'|\n[3cl2]u'z
.nr 3bot \n[3bot]>?\n[.d]
.sp |\n[3rs20]u
.3rvpt 1
.sp |\n[3bot]u
\*[3trans].nr 3brule 1
.3release
.3keep
.3rmk 3rt21
\*[3trans].nr 3crow 21
.3keep
.mk 3rs21
.mk 3bot
.3rvpt 0
.ta \n[3ce0]u \n[3ce1]u \n[3ce2]u
.lf 462
\&\h'|\n[3cl0]u'1e-24\h'|\n[3cl1]u'yocto\h'|\n[3cl2]u'y
.nr 3bot \n[3bot]>?\n[.d]
.sp |\n[3rs21]u
.3rvpt 1
.sp |\n[3bot]u
\*[3trans].nr 3brule 1
.3release
.mk 3rt22
.nr 3brule 1
.nr T. 1
.T#
.3init
.fc
.cp \n(3c
.lf 463
.TE
.LP
The function
.B utPrint()
always encodes a unit specification one way.  To reduce
misunderstandings, it is recommended that
this encoding style be used as the default.  
In general, a unit is printed in terms of basic units, factors, and
exponents.
Basic units are separated by spaces; and any
exponent directly appends its associated unit.
The above examples would be printed as follows:
.sp
.RS +4
.nf
10 kilogram meter second-2
9.8696044 radian2
0.555556 kelvin @ 255.372
10.471976 radian second-1
9.80665 meter2 second-2
98636.5 kilogram meter-1 second-2
.fi
.RE
.sp
Note that the Fahrenheit unit is encoded as a deviation, in fractional 
kelvins, from an origin at 255.372 kelvin.
.SH "UNITS FILE"
.LP
The units file is a formatted file containing unit definitions and is
used to initialize this package.
It is the first place to look to discover the set of
valid names and symbols (of which there are many \-\-  On October 9, 1992, 
it contained 446 entries).
.LP
The format for the units file is documented internally and the file may be
modified by the user as necessary.  In particular, additional units and
constants may be easily added (including variant spellings of existing
units or constants).
.SH ENVIRONMENT
.IP UDUNITS_PATH 17
If \fButInit()\fP
is called without a pathname argument, and if this environment variable is
non-empty, then its value overrides the default
pathname for the units file.
.SH DIAGNOSTICS
.LP
This package prints
(hopefully) self-explanatory error-messages to standard error.
.SH "SEE ALSO"
.LP
.BR udunits (1).
.SH "BUGS AND RESTRICTIONS"
.LP
.B utScan()
is case-sensitive.  If this causes difficulties, you might
try making appropriate additional entries to the units file.
.LP
Some unit abbreviations in the default units file might seem 
counter-intuitive.
In particular, note the following:
.sp
.RS 4
.nf
.ta \w'Celsius   'u +\w'`newton\' or `N\'   'u +\w'`rad\'   'u
For	Use	Not	Which Instead Means
.sp
Celsius	`Celsius'	`C'	coulomb
gram	`gram'	`g'	<standard free fall>
gallon	`gallon'	`gal'	<acceleration>
radian	`radian'	`rad'	<absorbed dose>
Newton	`newton' or `N'	`nt'	nit (unit of photometry)
.fi
.RE
.SH REFERENCES
.LP
NIST Special Publication 811, 1995 Edition: "Guide for the Use of the
International System of Units (SI)" by Barry N. Taylor.  URL
<http://physics.nist.gov/Divisions/Div840/SI.html>.
.LP
ANSI/IEEE Std 260-1978: "IEEE
Standard Letter Symbols for Units of Measurement".
.LP
ASTM Designation: E 380 \- 85: "Standard for METRIC PRACTICE".
.LP
International Standard (ISO) 2955: "Information processing \-\- 
Representation of SI and other units in systems with limited character sets",
Ref. No. ISO 2955-1983 (E).
