      function isleap(year)
        !--- Is this a leap year
        integer :: year
        logical :: isleap
        if ( mod(year,4).eq.0 ) then
          if ( mod(year,100).eq.0 ) then
            if ( mod(year,400).eq.0 ) then
              isleap = .true.
            else
              isleap = .false.
            endif
          else
            isleap = .true.
          endif
        else
          isleap = .false.
        endif
      end function isleap
