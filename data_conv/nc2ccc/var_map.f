      module var_map
c***********************************************************************
c Define a variable map between netcdf and CCCma variable names.
c***********************************************************************

      implicit none

      type vmap_t
        character(512) :: nc_name
        character(4)   :: cc_name
      end type vmap_t

      type(vmap_t), pointer :: vmap(:)

      logical :: vmap_init = .false.
      integer :: nvmap = 0

      contains

      subroutine varmap(nc_name, cc_name, vmap_file)
        !--- Define a CCCma variable name given a netcdf variable name
        implicit none

        !--- Input netcdf variable name
        character*(*)  :: nc_name

        !--- Output CCCma variable name
        character*(*) ::  cc_name

        !--- The name of a file containing variable name pairs
        !--- If this parameter is present then the structure vmap
        !--- will be initialized, destroying any previous definitions
        character*(*), intent(in), optional :: vmap_file

        !--- Local
        logical :: ok, exists
        character(1024) :: strng, line
        character(512) :: nc_lname
        character(4)   :: cc_lname
        integer :: ios, idx1, idx2, vlen, pass, nv
        integer :: verbose=0

        if ( present(vmap_file) ) then
          !--- Initialize vmap

          !--- Ensure that the input file exists
          inquire(file=trim(adjustl(vmap_file)),exist=exists)
          if ( .not.exists ) then
            write(6,'(2a)')
     &        'varmap: Variable name map file does not exist. file = ',
     &        trim(adjustl(vmap_file))
            call xit("VARMAP",-1)
          endif

          !--- Remove any existing variable map
          ! if ( allocated(vmap) ) deallocate(vmap)
          if ( associated(vmap) ) nullify( vmap )

          !--- Read the new map from a user supplied file
          open(504,file=trim(adjustl(vmap_file)),form='formatted')
          nvmap=0
          do pass=1,2
            !--- On the first pass, count the number of variable pairs
            !--- On the second pass assign these to the newly allocated vmap
            nv = 0
            ok = .true.
            do while (ok)
              !--- Read a line from the input file
              line  = " "
              strng = " "
              read(504,'(a)',iostat=ios) line
              if (ios.ne.0) exit
              strng = line

              !--- Ignore blank lines
              if ( len_trim(strng).le.0 ) cycle

              !--- Ignore lines whose first non whitespace character is "#"
              strng = adjustl(strng)
              if ( strng(1:1).eq."#" ) cycle

              !--- The first whitespace separated word is the netcdf variable name
              idx1=1
              idx2=index(strng,' ') - 1
              vlen=idx2-idx1+1
              nc_lname = " "
              nc_lname = strng(idx1:idx2)

              !--- The second whitespace separated word is the CCCma variable name
              strng = adjustl(strng(idx2+1:))
              if ( len_trim(strng).le.0 ) then
                write(6,'(a)')'varmap: Require two whitespace'
     &                        //' separated words on each line.'
                write(6,'(2a)')'line: ',trim(line)
                call xit("VARMAP",-2)
              endif
              idx1=1
              idx2=index(strng,' ') - 1
              vlen=idx2-idx1+1
              cc_lname = " "
              if ( vlen.gt.4 ) then
                !--- The CCCma variable name is greater that 4 chars
                write(6,'(a)')'varmap: A CCCma variable name found '
     &          //'in the input map file is greater than 4 characters'
                write(6,'(2a)')'line: ',trim(line)
                call xit("VARMAP",-3)
              else if (vlen.eq.1) then
                cc_lname(4:4) = strng(idx1:idx2)
              else if (vlen.eq.2) then
                cc_lname(3:4) = strng(idx1:idx2)
              else if (vlen.eq.3) then
                cc_lname(2:4) = strng(idx1:idx2)
              else if (vlen.eq.4) then
                cc_lname(1:4) = strng(idx1:idx2)
              else
                !--- This should never happen
                write(6,'(2a)')'varmap: Invalid CCCma variable name. ',
     &                         strng(idx1:idx2)
                call xit("VARMAP",-4)
              endif

              nv = nv+1
              if (pass.eq.2) then
                vmap(nv)%nc_name = nc_lname
                vmap(nv)%cc_name = cc_lname
                if ( verbose.gt.0 ) then
                  write(6,*)nv,'   nc_name=',trim(vmap(nv)%nc_name),
     &                      '   cc_name=',trim(vmap(nv)%cc_name)
                endif
              endif
            enddo
            rewind(504)
            if (pass.eq.1) nvmap = nv
            if (nvmap.le.0) then
              write(6,'(a)')'varmap: No variable name pairs found in ',
     &                       trim(vmap_file)
              call xit("VARMAP",-5)
            endif
            if (pass.eq.1) allocate( vmap(nvmap) )
          enddo
          close(504)
          vmap_init = .true.
        endif

        if ( .not. vmap_init ) then
          write(6,*)'varmap: The variable map has not been initialized.'
          call xit("VARMAP",-6)
        endif

        !--- If a netcdf variable name the same as the input nc_name
        !--- is found in vmap then return that value in cc_name
        !--- Otherwise return an empty string in cc_name
        cc_name = " "
        if ( len_trim(nc_name).le.0 ) then
          write(6,*)'varmap: Empty input netcdf variable name. Ignored.'
          return
        endif
        do idx1=1,nvmap
          if (trim(adjustl(vmap(idx1)%nc_name)) .eq.
     &        trim(adjustl(nc_name)) ) then
            cc_name = trim(adjustl(vmap(idx1)%cc_name))
            exit
          endif
        enddo

      end subroutine varmap

      end module var_map
