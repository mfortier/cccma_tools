module crec_data
!***********************************************************************
! Hold crec data variables and related info
!
! Larry Solheim ...July 2012
!***********************************************************************
#undef time_read_psfile

  use crec_subs

  private

  !--- Public subroutines
  public :: match_var, define_internal_recs, read_psfile

  !--- Public variables
  public :: ps_grid, lnsp_grid, lnpsa_grid
  public :: prev_ps_match
  public :: dtts_ocn, lat_ocn, lath_ocn, lon_ocn, lonh_ocn, z_ocn, zh_ocn
  public :: var_list, nvar, nvar_max

  !--- pointer to a linked list containing gridded LNSP records
  type (crec_t), pointer :: lnsp_grid => null()

  !--- pointer to a linked list containing gridded records of ln(PSA)
  type (crec_t), pointer :: lnpsa_grid => null()

  !--- pointer to a linked list containing gridded surface pressure in Pa
  type (crec_t), pointer :: ps_grid => null()

  !--- Variable specific pointers (time invariant)
  type (crec_t), pointer :: dtts_ocn => null()
  type (crec_t), pointer :: lat_ocn  => null()
  type (crec_t), pointer :: lath_ocn => null()
  type (crec_t), pointer :: lon_ocn  => null()
  type (crec_t), pointer :: lonh_ocn => null()
  type (crec_t), pointer :: z_ocn    => null()
  type (crec_t), pointer :: zh_ocn   => null()

  !--- An array which is used to hold CCCma type file records as well as
  !--- other info associated with a particular variable
  type (cvar_t), pointer :: var_list(:)

  !--- nvar will be the length of var_list
  integer, save :: nvar = 0

  !--- nvar_max is the maximum number of variables that may be defined
  !--- at any point in time. This is user configurable.
  integer, save :: nvar_max = 5000

  contains

  logical function match_var(cvar, kind, name, label, ftype) result (found)
    !-----------------------------------------------------------------------
    !--- Given at least one of kind, name, label or ftype, determine if the
    !--- current variable (cvar) matches all input strings
    !---
    !--- This function will compare kind, name or label with the first node
    !--- in cvar%rec. It is assumed that all nodes will contain the same
    !--- values for these 3 parameters
    !-----------------------------------------------------------------------
    type (cvar_t) :: cvar
    character(*), intent(in), optional :: kind
    character(*), intent(in), optional :: name
    character(*), intent(in), optional :: label
    character(*), intent(in), optional :: ftype

    !--- local
    logical :: valid, match(4)

    found = .false.

    !--- Initialize all matches to F
    match(:) = .false.

    !--- valid will be set true if at least one of
    !--- kind, name, label or ftype are present
    valid = .false.

    !--- ftype ---
    if ( present(ftype) ) then
      valid = .true.
      if (len_trim(ftype).le.0 .and. len_trim(cvar%ftype).le.0) then
        !--- Both are empty strings  ...match
        match(4) = .true.
      else if (len_trim(ftype).gt.0 .and. len_trim(cvar%ftype).gt.0) then
        !--- Both are non-empty strings
        !--- Match if they are equal, don't match if they are not equal
        match(4) = trim(adjustl(ftype)) .eq. trim(adjustl(cvar%ftype))
      else
        !--- One is empty the other is not ...no match
        match(4) = .false.
      endif
    else
      !--- If not present then assume a match
      match(4) = .true.
    endif

    if ( .not.associated( cvar%rec ) ) then
      !--- If the type(crec_t) rec is not defined then return
      if ( present(kind) .or. present(name) .or. present(label) ) then
        !--- The user wants to match one of these parameters but no can do
        write(6,'(2a)')'match_var: Attempting to match one of kind, name or label', &
          ' but none of thee parameters are defined'
        call print_cvar_t(cvar)
        call abort
      endif
      if ( .not. present(ftype) ) then
        !--- ftype is required in this case
        write(6,'(a)')'match_var: ftype was not input and rec is not defined.'
        call print_cvar_t(cvar)
        call abort
      endif

      !--- Return here (result is T if ftype matches)
      found = match(4)
      return
    endif

    !--- kind ---
    if ( present(kind) ) then
      valid = .true.
      if (len_trim(kind).le.0 .and. len_trim(cvar%rec%kind).le.0) then
        !--- Both are empty strings  ...match
        match(1) = .true.
      else if (len_trim(kind).gt.0 .and. len_trim(cvar%rec%kind).gt.0) then
        !--- Both are non-empty string
        !--- Match if they are equal, don't match if they are not equal
        match(1) = trim(adjustl(kind)) .eq. trim(adjustl(cvar%rec%kind))
      else
        !--- One is empty the other is not ...no match
        match(1) = .false.
      endif
    else
      !--- If not present then assume a match
      match(1) = .true.
    endif

    !--- name ---
    if ( present(name) ) then
      valid = .true.
      if (len_trim(name).le.0 .and. len_trim(cvar%rec%name).le.0) then
        !--- Both are empty strings  ...match
        match(2) = .true.
      else if (len_trim(name).gt.0 .and. len_trim(cvar%rec%name).gt.0) then
        !--- Both are non-empty string
        !--- Match if they are equal, don't match if they are not equal
        match(2) = trim(adjustl(name)) .eq. trim(adjustl(cvar%rec%name))
      else
        !--- One is empty the other is not ...no match
        match(2) = .false.
      endif
    else
      !--- If not present then assume a match
      match(2) = .true.
    endif

    !--- label ---
    if ( present(label) ) then
      valid = .true.
      if (len_trim(label).le.0 .and. len_trim(cvar%rec%label).le.0) then
        !--- Both are empty strings  ...match
        match(3) = .true.
      else if (len_trim(label).gt.0 .and. len_trim(cvar%rec%label).gt.0) then
        !--- Both are non-empty string
        !--- Match if they are equal, don't match if they are not equal
        match(3) = trim(adjustl(label)) .eq. trim(adjustl(cvar%rec%label))
      else
        !--- One is empty the other is not ...no match
        match(3) = .false.
      endif
    else
      !--- If not present then assume a match
      match(3) = .true.
    endif

    if (.not.valid) then
      !--- None of kind, name, label or ftype are present
      write(6,'(a)')'match_var: None of kind, name, label or ftype are present.'
      call print_cvar_t(cvar)
      call abort
    endif

    !--- Assign the result
    if ( all(match) ) then
      found = .true.
    else
      found = .false.
    endif

  end function match_var

  subroutine define_internal_recs(kind, name, label, time)
    !--------------------------------------------------------------------------
    !--- If the current record (stored in module iocom) contains a field that
    !--- may be required in the future then store that record internally using
    !--- one of the crec_t pointers defined above
    !--- If the record is not deemed "storeable" then this routine does nothing
    !--------------------------------------------------------------------------

    !--- iocom defines
    !---   maxx, ibuf(8), idat(2*maxx) and wrk(maxx)
    use iocom

    use time_subs, only : time_t
    use nccrip_params, only : verbose
    use nccrip_params, only : ocn_delt
    use nccrip_params, only : psfile

    implicit none

    character(*), intent(in) :: kind, name, label
    type(time_t), intent(in),  pointer, optional :: time

    !--- Local
    type (crec_t), pointer :: curr_rec
    integer :: idx, nwrds, xbuf(8), vbmax
    real, allocatable :: wrka(:)

    !--- vbmax sets a value for verbose, above which this subroutine will
    !--- write diagnostic messages to stdout
    vbmax=2

    if ( trim(adjustl(label)) .eq. "OCEAN MODEL NUMERICAL PARAMETERS" ) then
      if ( name(1:4) .eq. "DTTS" ) then
        !--- This is the time step in seconds for the ocean
        !--- Store this record in memory and assign ocn_delt
        !--- which is found in the modpar module
        call crec_free(dtts_ocn)
        call crec_init(dtts_ocn, ibuf, wrk, label=label)
        if (verbose.gt.vbmax) call crec_print(dtts_ocn)
        ocn_delt = dtts_ocn%data(1)
      endif
    endif

    if ( trim(adjustl(label)) .eq. "DATA DESCRIPTION" ) then
      !--- Look for particular time invariant records found
      !--- in the ocean data description section
      !--- Store these record in memory for use later

      !---   Ocean tracer grid is LAT , LON
      !--- Ocean velocity grid is LATH, LONH
      !---
      !---                U_i,j
      !---     -------------
      !---     |           |
      !---     |           |
      !---     |   T_i,j   |
      !---     |           |
      !---     |           |
      !---     -------------
      if ( name .eq. " LAT" ) then
        !--- Ocean latitude (tracer grid)
        if ( .not.associated(lat_ocn) ) then
          !--- Use the first record found
          call crec_init(lat_ocn, ibuf, wrk, label=label)
        endif
        if (verbose.gt.vbmax) call crec_print(lat_ocn)
      endif
      if ( name .eq. " LON" ) then
        !--- Ocean longitude (tracer grid)
        if ( .not.associated(lon_ocn) ) then
          !--- Use the first record found
          call crec_init(lon_ocn, ibuf, wrk, label=label)
        endif
        if (verbose.gt.vbmax) call crec_print(lon_ocn)
      endif
      if ( name .eq. "LATH" ) then
        !--- Ocean latitude (velocity grid)
        if ( .not.associated(lath_ocn) ) then
          !--- Use the first record found
          call crec_init(lath_ocn, ibuf, wrk, label=label)
        endif
        if (verbose.gt.vbmax) call crec_print(lath_ocn)
      endif
      if ( name .eq. "LONH" ) then
        !--- Ocean longitude (velocity grid)
        if ( .not.associated(lonh_ocn) ) then
          !--- Use the first record found
          call crec_init(lonh_ocn, ibuf, wrk, label=label)
        endif
        if (verbose.gt.vbmax) call crec_print(lonh_ocn)
      endif

      !--- Ocean vertical levels
      !---       ----    0   ---- ZH(1)
      !---       ----    5   ----  Z(1)
      !---       ----   10   ---- ZH(2)
      !---       ----   15   ----  Z(2)
      !---           ....
      !---       ---- 5038   ---- ZH(40)
      !---       ---- 5233.3 ----  Z(40)
      !---       ---- 5428.6 ---- ZH(41)
      !---
      !--- Most ocean variables use 40 levels
      if ( name .eq. "   Z" ) then
        !--- Ocean vertical levels (full levels)
        if ( .not.associated(z_ocn) ) then
          !--- Use the first record found
          call crec_init(z_ocn, ibuf, wrk, label=label)
        endif
        if (verbose.gt.vbmax) call crec_print(z_ocn)
      endif
      if ( name .eq. "  ZH" ) then
        !--- Ocean vertical levels (half levels)
        if ( .not.associated(zh_ocn) ) then
          !--- Use the first record found
          call crec_init(zh_ocn, ibuf, wrk, label=label)
        endif
        if (verbose.gt.vbmax) call crec_print(zh_ocn)
      endif

    endif

    if ( .not. present(time) ) then
      !--- The variables defined below will all require a time record
      !--- If one has not been passed into this subroutine then assume that the
      !--- user does not want the remaining variables defined at this point
      return
    endif

    !--- Gridded LNSP (natural log of surface pressure)
    if ( name .eq. "LNSP" ) then

      if ( kind .eq. "SPEC" ) then
        !--- Convert to a gaussian grid
        xbuf(1:8) = ibuf(1:8)
        if ( allocated(wrka) ) deallocate( wrka )
        allocate( wrka( size(wrk) ) )
        call sp2gg(wrka,wrk,xbuf,0)

        !--- sp2gg will modify xbuf values appropriately for the output GRID record
        !--- Store this data record in memory
        call crec_append(lnsp_grid, curr_rec, ibuf=xbuf, data=wrka, time=time)
        if (verbose.gt.vbmax) call crec_print( curr_rec )

        !--- Also append to ps_grid unless the user has supplied this data
        if ( len_trim(psfile).le.0 ) then
          !--- The user did not supply a surface pressure file on the command line
          !--- Use LNSP to populate ps_grid (gridded surface pressure in Pa)
          nwrds = xbuf(5)*xbuf(6)
          !--- Convert LNSP to surface pressure in Pa
          do idx=1,nwrds
            wrka(idx) = 100.0*exp(wrka(idx))
          enddo

          xbuf(3) = transfer("  PS", 1)
          call crec_append(ps_grid, curr_rec, ibuf=xbuf, data=wrka, time=time)
          if (verbose.gt.vbmax) call crec_print( curr_rec )
        endif

        deallocate( wrka )
      else
        !--- Store this data record in memory
        !--- It will have been already been converted to GRID
        call crec_append(lnsp_grid, curr_rec, ibuf=ibuf, data=wrk, time=time)
        if (verbose.gt.vbmax) call crec_print( curr_rec )

        !--- Also append to ps_grid unless the user has supplied this data
        if ( len_trim(psfile).le.0 ) then
          !--- The user did not supply a surface pressure file on the command line
          !--- Use LNSP to populate ps_grid (gridded surface pressure in Pa)
          nwrds = ibuf(5)*ibuf(6)
          if ( allocated( wrka) ) deallocate( wrka )
          allocate( wrka(nwrds) )
          !--- Convert LNSP to surface pressure in Pa
          do idx=1,nwrds
            wrka(idx) = 100.0*exp(wrk(idx))
          enddo

          xbuf(1:8) = ibuf(1:8)
          xbuf(3) = transfer("  PS", 1)
          call crec_append(ps_grid, curr_rec, ibuf=xbuf, data=wrka, time=time)
          if (verbose.gt.vbmax) call crec_print( curr_rec )
        
          deallocate( wrka )
        endif
      endif
    endif

    !--- Gridded PSA (daily accumulated surface pressure)
    if ( .not. associated(lnsp_grid) .and. name .eq. " PSA" ) then
      !--- Store this data record in memory but only if lnsp_grid is not available

      !--- PSA is in hPa, convert to ln(ps in hPa)
      nwrds = ibuf(5)*ibuf(6)
      if ( allocated( wrka) ) deallocate( wrka )
      allocate( wrka(nwrds) )
      do idx=1,nwrds
        wrka(idx) = log(wrk(idx))
      enddo

      call crec_append(lnpsa_grid, curr_rec, ibuf=ibuf, data=wrka, time=time)
      if (verbose.gt.vbmax) call crec_print( curr_rec )

      !--- Also append to ps_grid unless the user has supplied this data
      if ( len_trim(psfile).le.0 ) then
        !--- The user did not supply a surface pressure file on the command line
        !--- Use PSA to populate ps_grid (gridded surface pressure in Pa)
        !--- Convert PSA to surface pressure in Pa
        do idx=1,nwrds
          wrka(idx) = 100.0*wrk(idx)
        enddo

        xbuf(1:8) = ibuf(1:8)
        xbuf(3) = transfer("  PS", 1)
        call crec_append(ps_grid, curr_rec, ibuf=xbuf, data=wrka, time=time)
        if (verbose.gt.vbmax) call crec_print( curr_rec )
        
      endif
      if ( allocated( wrka) ) deallocate( wrka )
    endif

    !--- PS from gp or xp files
    if ( trim(adjustl(label)) .eq. "PS" .or. trim(adjustl(label)) .eq. "(PS)") then
      !--- This is likely surface pressure from the gp or xp file
      if ( name .eq. "  PS" ) then
        !--- Store this data record in memory

        !--- Append to ps_grid unless the user has supplied this data via an external file
        if ( len_trim(psfile).le.0 ) then
          !--- The user did not supply a surface pressure file on the command line
          !--- Use PS to populate ps_grid (gridded surface pressure in Pa)
          nwrds = ibuf(5)*ibuf(6)
          if ( allocated( wrka) ) deallocate( wrka )
          allocate( wrka(nwrds) )

          !--- Convert PS to surface pressure in Pa
          do idx=1,nwrds
            wrka(idx) = 100.0*wrk(idx)
          enddo

          xbuf(1:8) = ibuf(1:8)
          xbuf(3) = transfer("  PS", 1)
          call crec_append(ps_grid, curr_rec, ibuf=xbuf, data=wrka, time=time)
          if (verbose.gt.vbmax) call crec_print( curr_rec )

          if ( allocated( wrka) ) deallocate( wrka )
        endif
      endif
    endif

  end subroutine define_internal_recs

  subroutine read_psfile(fname)
    !-----------------------------------------------------------------
    !--- Read a CCCma binary file that contains only surface pressure
    !--- and store all records in the internal "ps_grid" list after
    !--- some possible preprocessing (e.g. change hPa to Pa or convert
    !--- SPEC records to GRID records)
    !-----------------------------------------------------------------
    use nccrip_params, only : verbose
    use nccrip_params, only : ps_convert_hPa_to_Pa
    use nccrip_params, only : usr_ib2_fmt, int_ib2_fmt
    use nccrip_params, only : allow_ps_as_data
    use nccrip_params, only : start_time, stop_time
    use nccrip_params, only : seq_ps_grid
    use strings, only : split_on_delim
    use time_subs, only : time_t, set_time
    use crec_subs, only : crec_t, crec_append, crec_print
    use ccc_io, only : newunit, cc_open, cc_close
    use ccc_io, only : guess_ib2_fmt

    !--- iocom defines
    !---   maxx, abuf(8), adat(2*maxx) and awrk(maxx)
    !--- Use the alternate buffer space to avoid overwriting
    !--- existing values in ibuf etc
    use iocom

    implicit none

    character(*) :: fname

    !--- Local
    integer :: iu
    logical :: remote, ok, is_first_ps
    character(len=4) :: kind, name
    real, allocatable :: xwrk(:)
    type(time_t), pointer :: curr_time
    type (crec_t), pointer :: curr_ps, prev_ps
    character(10) :: ib2fmt
    integer :: nflds
    integer(kind=8) :: ibuf2, prev_ibuf2
    character(len=512) :: flds(40)
#ifdef time_read_psfile
    real :: t1, t2, t3
    integer :: iutime, itcount
#endif

#ifdef time_read_psfile
    call cpu_time(t3)
    itcount = 0
    iutime = newunit(400)
    open(iutime,file='time_read_psfile',form='formatted')
#endif

    if (len_trim(fname).le.0) then
      write(6,'(a)')'read_psfile: Input file name is missing.'
      call abort
    endif

    !--- Break the input file name string into colon (:) separated fields
    flds(:) = " "
    call split_on_delim(flds,nflds,fname,":")

    !--- Replace fname with the first colon separated field
    !--- (which may be the same as fname)
    fname = trim(adjustl(flds(1)))

    !--- If there is a second field then assume it is the ibuf2 format string
    if (nflds.gt.1) then
      ib2fmt = trim(adjustl(flds(2)))
    else
      ib2fmt = " "
    endif

    iu = newunit(400)
    call cc_open(iu, fname, remote, verbose)

    seq_ps_grid = .true.
    is_first_ps = .true.
    ok = .true.
    do while (ok)
#ifdef time_read_psfile
      call cpu_time(t1)
      itcount = itcount + 1
#endif
      call getfld2(iu,awrk,-1,-1,-1,-1,abuf,maxx,ok)
      if (verbose.gt.10) then
        write(6,'(a,1x,a4,1x,i10,1x,a4,1x,5i8)')'PSFILE: ',abuf(1:8)
      endif
      if (.not.ok) exit

      kind = transfer(abuf(1), kind)
      name = transfer(abuf(3), name)

      !--- Ignore LABL and CHAR records
      if ( kind.eq."LABL" .or. kind.eq."CHAR" ) cycle

      !--- Ignore DATA records unless these are requested
      if ( kind.eq."DATA" .and. .not. allow_ps_as_data ) cycle

      !--- Ignore records outside the range start_time <= abuf(2) <= stop_time
      if ( (start_time.gt.0 .and. abuf(2).lt.start_time) .or. &
           (stop_time.gt.0  .and. abuf(2).gt.stop_time) ) then
        cycle
      endif

      if ( kind .eq. "SPEC") then
        !--- Convert any spectral field to a gaussian grid

        if ( allocated(xwrk) ) deallocate( xwrk )
        allocate( xwrk( size(awrk) ) )
        call sp2gg(xwrk, awrk, abuf, 0)
        awrk(1:abuf(5)*abuf(6)) = xwrk(1:abuf(5)*abuf(6))
        deallocate( xwrk )

        !--- sp2gg will modify abuf values appropriately for the output GRID record
        !--- Redefine kind to indicate a GRID record
        kind = "GRID"
      endif

      if ( ps_convert_hPa_to_Pa ) then
        !--- This field needs to be converted from hPa to Pa
        awrk(1:abuf(5)*abuf(6)) = 100.0 * awrk(1:abuf(5)*abuf(6))
      endif

      !--- Ensure that ib2fmt is set and contains a valid string
      if (len_trim(ib2fmt).le.0) then
        if ( len_trim(usr_ib2_fmt).gt.0 ) then
          !--- Assume the user knows what they want
          ib2fmt = trim(adjustl(usr_ib2_fmt))
        else if ( len_trim(int_ib2_fmt).gt.0 ) then
          !--- The ibuf2 value has been set elsewhere
          ib2fmt = trim(adjustl(int_ib2_fmt))
        else
          !--- If the user has not explicitly supplied a format and the
          !--- format has not been defined elsewhere then make a guess
          ibuf2 = abuf(2)
          ib2fmt = guess_ib2_fmt(ibuf2)
        endif
        ib2fmt = trim(adjustl(ib2fmt))
      endif

!xxx      !--- Determine a time to associate with this record
!xxx      !--- Add the verbatim_ib2=.true. option to mitigate problems with YYYYMM format
!xxx      !--- ibuf2 values that are reset internal to set_time using values found in
!xxx      !--- int_ib2_year and int_ib2_mon, resulting in the same time associated with
!xxx      !--- every surface pressure record that is read from the current file
!xxx      call set_time(curr_time, abuf, verbose=0, var_ib2_fmt=trim(ib2fmt), verbatim_ib2=.true.)
!xxx
!xxx      !--- Store this data record in memory
!xxx      call crec_append(ps_grid, curr_ps, ibuf=abuf, data=awrk, time=curr_time)

      !--- Determine a time to associate with this record
      !--- Add the verbatim_ib2=.true. option to mitigate problems with YYYYMM format
      !--- ibuf2 values that are reset internal to set_time using values found in
      !--- int_ib2_year and int_ib2_mon, resulting in the same time associated with
      !--- every surface pressure record that is read from the current file
      !--- Add use_master_time=.false. so that the master time list is not searched
      !--- and the curretn time is always appended to master_list
      call set_time( curr_time, abuf, verbose=0, var_ib2_fmt=trim(ib2fmt), &
                     verbatim_ib2=.true., use_master_time=.false. )

      !--- Store this data record in memory
      if ( is_first_ps ) then
        !--- This is the first time through this loop
        !--- Start appending to the ps_grid list
        call crec_append(ps_grid, curr_ps, ibuf=abuf, data=awrk, time=curr_time)
        is_first_ps = .false.

        !--- Initialize prev_ibuf2 used to determine if the data is sequential in time
        prev_ibuf2 = abuf(2)
      else
        !--- Continue appending to the ps_grid list but use the penultimate entry
        !--- as the entry point for crec_append to start looking through this list
        call crec_append(prev_ps, curr_ps, ibuf=abuf, data=awrk, time=curr_time)

        !--- Test to verify that the data is sequential in time
        if ( prev_ibuf2 .gt. abuf(2) ) then
          !--- Values must be monotonically increasing (or not changing)
          seq_ps_grid = .false.
        endif
        prev_ibuf2 = abuf(2)
      endif

      !--- Ensure this pointer into ps_grid is always the most recent entry
      prev_ps => curr_ps

      if (verbose.gt.10) then
        call crec_print( curr_ps )
      endif

#ifdef time_read_psfile
      call cpu_time(t2)
      write(iutime,'(a,i8,1x,a4,1x,i10,1x,a4,1x,5i8,$)')'read_psfile: ',itcount,abuf(1:8)
      write(iutime,'(a,g16.8)')': millisecs this iteration ',1000.0*(t2-t1)
      call flush(iutime)
#endif

    enddo

    call cc_close(iu, fname, remote, verbose)

    write(6,'(a,l1)')'read_psfile: seq_ps_grid = ',seq_ps_grid

#ifdef time_read_psfile
    call cpu_time(t2)
    write(iutime,'(a,g16.8)')'read_psfile: millisecs this call ',1000.0*(t2-t3)
    close(iutime)
#endif

  end subroutine read_psfile
        
end module crec_data
