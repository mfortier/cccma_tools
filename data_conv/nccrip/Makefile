#
# Compile nccrip
# A program to convert CCCma binary data to netcdf
#
# Larry Solheim Jan 5,2011


# Define a string to append to certain file names
STAMP := $(shell date "+%Y%b%d_%H%M%S")

# Determine kernel type and machine name
MACH_TYPE := $(shell uname -s|tr '[A-Z]' '[a-z]')
MACH_NAME := $(word 1,$(subst ., ,$(shell uname -n|awk -F'.' '{print $1}' -)))

ifeq ($(MACH_TYPE), linux)
  # Determine dns domain name
  DOMAIN := $(subst int.cmc.,cmc.,$(shell dnsdomainname))
endif

ifeq ($(MACH_TYPE), aix)
  # Determine dns domain name
  DOMAIN := $(subst int.cmc.,cmc.,$(word 2,$(shell grep -E 'search|domain' /etc/resolv.conf)))
endif

# Determine location, typically either cccma or cmc
LOCATION := $(word 1,$(subst ., ,$(DOMAIN)))
ifndef LOCATION
  $(error Unable to determine a value for LOCATION.)
endif

ifndef RMACH
  ifeq ($(LOCATION), cccma)
    RMACH := lxsrv
  endif

  ifeq ($(LOCATION), cmc)
    RMACH := joule
  endif

  ifndef RMACH
    $(error Unable to determine RMACH. Invalid LOCATION = $(LOCATION))
  endif
endif

BIN_FILES := $(filter-out %~,$(wildcard bin/*))

# REM_REPO_DIR is the name of a directory on the remote machine to contain the nccrip.git repo
REM_REPO_DIR := '$$CCRNSRC/ccc2nc_dir'

MIRROR_DORVAL := /users/tor/acrn/src/ccc2nc_dir/nccrip.git
MIRROR_VIC    := /home/acrnsrc/ccc2nc_dir/nccrip.git

# PUB_BINDIR is the install location
PUB_BINDIR := '$$CCRNSRC/ccc2nc_dir/bin'

# REM_SRCDIR is the full pathname on joule where this source tree
# will be copied when the sync target is invoked
REM_SRCDIR := '$$HOME/src/nccrip/'

# kernel default defs

ifeq ($(MACH_TYPE), linux)
  CC := gcc
  CFLAGS :=
  LDFLAGS := -lm
  CXX :=
  CPPFLAGS := -DpgiFortran
  FC := pgf90
  KIND8 := -i8 -r8
  FFLAGS_FIXED := -O2 -gopt -Mbyteswapio -Mbackslash -Mpreprocess -Mlfs -Kieee -uname -Ktrap=fp -Bstatic $(KIND8)
  FFLAGS := -Mfree $(FFLAGS_FIXED)
  # INCS := -I/home/rls/wrk/AR5/CMOR/lxsrv3/include -I.
  # LIBD := -L/home/rls/wrk/AR5/CMOR/lxsrv3/lib -L.
  # COMMLIB := /home/rls/wrk/AR5/CMOR/lxsrv3/lib/libLOSUB_comm_cmor.a
  # The expat library in /home/rls/wrk/AR5/CMOR/lxsrv3/lib is required by udunits2
  # LIBS := $(LIBD) -lcmor -lnetcdf -lhdf5_hl -lhdf5 -ludunits2 -lexpat -lm -lz -luuid
  # LIBS := $(LIBD) -lnetcdf

  EXPAT_LIB := -L$(wildcard ~acrnopt/package/expat/linux64/lib) -lexpat
  EXPAT_INC := -I$(wildcard ~acrnopt/package/expat/linux64/include)

  CMOR_INCS := -I${HOME}/local/linux64/include $(EXPAT_INC)
  CMOR_LIBS := -L${HOME}/local/linux64/lib -lcmor -ludunits2 -luuid $(EXPAT_LIB)

  NETCDF_INCS := -I${HOME}/local/linux64/netcdf3.6.3/include
  NETCDF_LIBS := -L${HOME}/local/linux64/netcdf3.6.3/lib -lnetcdf

  # NETCDF_INCS := -I/home/rls/wrk/AR5/CMOR/lxsrv3/include
  # NETCDF_LIBS := -L/home/rls/wrk/AR5/CMOR/lxsrv3/lib -lnetcdf
  MAINEXE := nccrip_linux
  ifeq ($(LOCATION), cmc)
    # Assume joule

    CMOR_INCS := -I/data/ords/acrn/acrnrls/pub/linux64/include
    CMOR_LIBS := -L/data/ords/acrn/acrnrls/pub/linux64/lib -lcmor -ludunits2 -luuid

    NETCDF_INCS := -I${HOME}/local/linux64/netcdf3.6.3/include
    NETCDF_LIBS := -L${HOME}/local/linux64/netcdf3.6.3/lib -lnetcdf

    MAINEXE := nccrip_linux64
  endif
endif

ifeq ($(MACH_TYPE), aix)
  FC := xlf90
  KIND8 := -qintsize=8 -qrealsize=8
  COM_FFLAGS := -O -qmaxmem=-1 -qarch=auto -q64 -qextname $(KIND8)
  FFLAGS := -qfree=f90 $(COM_FFLAGS)
  FFLAGS_FIXED := -qfixed=72 $(COM_FFLAGS)

  CMOR_INCS := -I/data/ords/acrn/acrnrls/pub/aix64/include
  CMOR_LIBS := -L/data/ords/acrn/acrnrls/pub/aix64/lib -lcmor -ludunits2 -luuid -lexpat

  NETCDF_INCS := -I${HOME}/local/aix64/netcdf3.6.3/include
  NETCDF_LIBS := -L${HOME}/local/aix64/netcdf3.6.3/lib -lnetcdf

  MAINEXE := nccrip_aix
endif

LIBS := $(CMOR_LIBS) $(NETCDF_LIBS)
INCS := $(CMOR_INCS) $(NETCDF_INCS)

ifndef MAINEXE
  $(error Unable to determine MAINEXE. Invalid MACH_TYPE = $(MACH_TYPE))
endif

# Keep module names and certain files separate from other subroutines
# so that these can be compiled first
MSRC := nccrip_params.F90 iocom.F90 strings.F90 var_meta_data.F90 derived_vars.F90 ccc_io.F90 \
        file_subs.F90 time_subs.F90 crec_subs.F90 crec_data.F90 ccc_subs.F90 grid_info.F90 \
        nc_subs.F90 ccc_time_series.F90
MOBJ := $(patsubst %.f90,%.o,$(patsubst %.F90,%.o,$(patsubst %.F,%.o,$(patsubst %.f,%.o,$(MSRC)))))

# Get the list of dependencies from a list of *.[fF] files in the cwd
FSUBS := $(filter-out $(MSRC) nccrip_main.F90, $(wildcard *.f *.F *.f90 *.F90))
OBJS := $(patsubst %.f90,%.o,$(patsubst %.F90,%.o,$(patsubst %.F,%.o,$(patsubst %.f,%.o,$(FSUBS)))))

# Define an internal list of all CCCma source file that are required
CCCFSUBS := getfld2.f putfld2.f xit2.cdk bf1bi64.f bf1i64b.f bf2bi64.f bf2i64b.f cf1ci64.f \
            cf1i64c.f decodr2.f encodr2.f gbytesb.f ieeepk.f ieeeup.f inteflt.f lblchk.f \
            lendian.f lvcode.f lvdcode.f me32o64.f nc4to8.f paccrn.f prtlab.f rdbuf.f rdskip.f \
            recget.f recpk2.f recput.f recup2.f reverse.f sbytesb.f wrtbuf.f coordab.f dimgt.f \
            epscal.f gaussg.f nivcal.f ordleg.f trigl.f alpst2.f ffgfw.f ftsetup.f qpassf.f \
            rpassf.f staf.f vfft.f eapl.f lwbw2.f gridinf.f betainf.f
CCCOBJS  := $(patsubst %.F,%.o,$(patsubst %.f,%.o,$(CCCFSUBS)))
CCCOBJS  := $(sort $(patsubst %.cdk,%.o,$(patsubst %.dk,%.o,$(CCCOBJS))))

VPATH := $(CCRNSRC)/source/lssub/comm/io \
         $(CCRNSRC)/source/lssub/comm/gen \
         $(CCRNSRC)/source/lssub/comm/nrecipes \
         $(CCRNSRC)/source/lssub/comm/trans \
         $(CCRNSRC)/source/lssub/comm/ocean \
         $(CCRNSRC)/source/lssub/diag/gen \
         $(CCRNSRC)/source/lssub/diag/trans

.SILENT: clean

all: $(MAINEXE)

.PHONY: debug
debug:
	@echo DOMAIN = $(DOMAIN)
	@echo LOCATION = $(LOCATION)
	@echo REM_SRCDIR = $(REM_SRCDIR)
	@echo LIBS = $(LIBS)
	@echo INCS = $(INCS)
	@echo MSRC = $(MSRC)
	@echo MOBJ = $(MOBJ)
	@echo FSUBS = $(FSUBS)
	@echo OBJS = $(OBJS)
	@echo CCCFSUBS = $(CCCFSUBS)
	@echo CCCOBJS = $(CCCOBJS)

depends: nccrip_main.F90
	@echo "Creating dependency file for $<"
	fordep --filepath=DEPENDS --filepath_copy --nofilepath_macros --clean $<

$(MAINEXE): $(MOBJ) $(OBJS) $(CCCOBJS) nccrip_main.F90
	$(FC) $(FFLAGS) $(MOBJ) $(INCS) $(LIBS) -o $(MAINEXE) nccrip_main.F90 $(OBJS) $(CCCOBJS)

%.o: %.f
	$(FC) -c $(FFLAGS_FIXED) $< $(INCS) -o $*.o

%.o: %.F
	$(FC) -c $(FFLAGS) $< $(INCS) -o $*.o

%.o: %.F90
	$(FC) -c $(FFLAGS) $< $(INCS) -o $*.o

# Cancel the implicit RCS extraction rule to avoid getting out of date files
# or unwanted source files copied to the build directory
% :: RCS/%,v

# Define an explicit rule for xit
xit2.o: xit2.cdk
	up2cpp --out_file=xit2.F --overwrite --quiet modver=gcm17 xit2.cdk
	$(FC) $(FFLAGS_FIXED) -c xit2.F -o xit2.o && rm -f xit2.F

xit2.cdk:

sync:
ifeq ($(LOCATION), cccma)
	@# the remote machine will always be joule in this case
	rsync -Cav Makefile nccrip *.[Ffh] *.F90 bin joule:$(REM_SRCDIR)
	rsync -Cav Makefile nccrip *.[Ffh] *.F90 bin lxwrk1:$(REM_SRCDIR)
else
	@echo "rsync is only allowed when originating in Victoria"
endif

install: $(MAINEXE)
	@# Copy executables to ~acrnsrc on RMACH
	scp nccrip bin/ts2nc $(MAINEXE) $(RMACH):$(PUB_BINDIR)
	ssh $(RMACH) chmod 755 $(PUB_BINDIR)/nccrip* $(PUB_BINDIR)/ts2nc
#	ssh $(RMACH) chmod 755 $(PUB_BINDIR)/ts2nc

install_ts2nc:
	@# Copy executables to ~acrnsrc on RMACH
	scp nccrip bin/ts2nc $(RMACH):$(PUB_BINDIR)
	ssh $(RMACH) chmod 755 $(PUB_BINDIR)/ts2nc

# This target will delete and then recreate the remote mirrored dir as an empty repo
# It can then be repopulated using the mirror target below
create_nccrip.git_dorval:
ifneq ($(MACH_NAME), lxlp01)
	$(error The create_nccrip.git_dorval target may only excute on lxlp01)
endif
	ssh joule cd $(REM_REPO_DIR)\; \
	  rm -fr nccrip.git \&\& git init --shared=0755 --bare nccrip.git

# This target will delete and then recreate the remote mirrored dir as an empty repo
# It can then be repopulated using the mirror target below
create_nccrip.git_vic:
ifneq ($(MACH_NAME), lxlp01)
	$(error The create_nccrip.git_vic target may only excute on lxlp01)
endif
	ssh lxwrk1 cd $(REM_REPO_DIR)\; \
	  rm -fr nccrip.git \&\& git init --shared=0755 --bare nccrip.git

# Push to the mirrored repository
mirror:
ifneq ($(MACH_NAME), lxlp01)
	$(error The sync target may only excute on lxlp01)
endif
	git push --mirror joule.cmc.ec.gc.ca:$(MIRROR_DORVAL)
	git push --mirror lxwrk1.cccma.ec.gc.ca:$(MIRROR_VIC)

clean:
	rm -fr $(OBJS) $(MOBJ) *.mod $(MAINEXE)

veryclean:
	rm -fr *.o *.mod nccrip_linux nccrip_linux64 nccrip_aix
