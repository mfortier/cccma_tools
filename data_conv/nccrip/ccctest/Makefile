# Make ccma diag programs used for testing nccrip
#
#   Larry Solheim Sep,2012

# Define a string to append to certain file names
STAMP := $(shell date "+%Y%b%d_%H%M%S")

# Determine kernel type and machine name
MACH_TYPE := $(shell uname -s|tr '[A-Z]' '[a-z]')
MACH_NAME := $(word 1,$(subst ., ,$(shell uname -n|awk -F'.' '{print $1}' -)))

ifeq ($(MACH_TYPE), linux)
  # Determine dns domain name
  DOMAIN := $(subst int.cmc.,cmc.,$(shell dnsdomainname))
endif

ifeq ($(MACH_TYPE), aix)
  # Determine dns domain name
  DOMAIN := $(subst int.cmc.,cmc.,$(word 2,$(shell grep -E 'search|domain' /etc/resolv.conf)))
endif

# Determine location, typically either cccma or cmc
LOCATION := $(word 1,$(subst ., ,$(DOMAIN)))
ifndef LOCATION
  $(error Unable to determine a value for LOCATION.)
endif

# Set defaults
FC := 
FFLAGS := 
BINDIR := $(shell echo ${HOME}/bin)
PUB_BINDIR := '$$CCRNSRC/ccc2nc_dir/bin'
NETCDF_INCS := 
NETCDF_LIBS := 

# kernel default defs

ifeq ($(MACH_TYPE), linux)
  CC := gcc
  CFLAGS :=
  LDFLAGS := -lm
  CXX :=
  CPPFLAGS := -DpgiFortran
  FC := pgf90
  FFLAGS_FIXED := -O2 -gopt -Mbyteswapio -Mbackslash -Mpreprocess -Mlfs -Kieee -uname -Ktrap=fp -Bstatic
  FFLAGS := -Mfree $(FFLAGS_FIXED)
  # INCS := -I/home/rls/wrk/AR5/CMOR/lxsrv3/include -I.
  # LIBD := -L/home/rls/wrk/AR5/CMOR/lxsrv3/lib -L.
  # COMMLIB := /home/rls/wrk/AR5/CMOR/lxsrv3/lib/libLOSUB_comm_cmor.a
  # The expat library in /home/rls/wrk/AR5/CMOR/lxsrv3/lib is required by udunits2
  # LIBS := $(LIBD) -lcmor -lnetcdf -lhdf5_hl -lhdf5 -ludunits2 -lexpat -lm -lz -luuid
  # LIBS := $(LIBD) -lnetcdf

  # NETCDF_INCS := -I${HOME}/local/linux/include
  # NETCDF_LIBS := -L${HOME}/local/linux/lib -lnetcdf

  NETCDF_INCS := -I/home/rls/wrk/AR5/CMOR/lxsrv3/include
  NETCDF_LIBS := -L/home/rls/wrk/AR5/CMOR/lxsrv3/lib -lnetcdf
  MAINEXE := nccrip_linux
  ifeq ($(LOCATION), cmc)
    # Assume pollux
    NETCDF_INCS := -I${HOME}/local/linux64/netcdf3.6.3/include
    NETCDF_LIBS := -L${HOME}/local/linux64/netcdf3.6.3/lib -lnetcdf
    MAINEXE := nccrip_linux64
  endif
  LIBS := $(NETCDF_LIBS)
  INCS := -I. $(NETCDF_INCS)
endif

# ifeq ($(MACH_TYPE), linux)
#  # NETCDF_INCS := -I${HOME}/local/linux/include
#  # NETCDF_LIBS := -L${HOME}/local/linux/lib -lnetcdf
#  BINDIR := $(shell echo ${HOME}/bin_linux)
#  MAINEXE := cccrip_linux
#
#  # Intel fortran compiler
#  # FC := ifort
#  # FFLAGS := -r8 -i8 -Vaxlib -nbs -WB
#
#  # Portland Group f90 compiler
#  FC := pgf90
#  FFLAGS := -r8 -i8 -O2 -gopt -Mbyteswapio -Mbackslash -Mpreprocess -Mlfs -Kieee -uname -Ktrap=fp -Bstatic
#  # FFLAGS := -r8 -i8 -O2 -Mbounds -gopt -Mbyteswapio -Mbackslash -Mpreprocess -Mlfs -Kieee -uname -Ktrap=fp -Bstatic
#endif

ifeq ($(MACH_TYPE), aix)
  FC := xlf90
  COM_FFLAGS := -O -qmaxmem=-1 -qarch=auto -q64 -qextname
  FFLAGS := -qfree=f90 $(COM_FFLAGS)
  FFLAGS_FIXED := -qfixed=72 $(COM_FFLAGS)
  NETCDF_INCS := -I${HOME}/local/aix64/netcdf3.6.3/include
  NETCDF_LIBS := -L${HOME}/local/aix64/netcdf3.6.3/lib -lnetcdf
  LIBS := $(NETCDF_LIBS)
  INCS := $(NETCDF_INCS)
  MAINEXE := nccrip_aix
endif

# Machine specific defs

# ifeq ($(findstring ib-fe,$(MACH_NAME)), ib-fe)
#   # NETCDF_INCS := -I/users/tor/acrn/rls/local/linux/include
#   # NETCDF_LIBS := -L/users/tor/acrn/rls/local/linux/lib -lnetcdf
#   BINDIR := $(shell echo ${HOME}/bin_linux)
#   FC := pgf90
#   FFLAGS := -O2 -gopt -Mbyteswapio -Mbackslash -Mpreprocess -Kieee -uname -Ktrap=fp -Bstatic -Mlfs
# endif

VPATH := $(CCRNSRC)/source/lssub/comm/io \
         $(CCRNSRC)/source/lssub/comm/gen \
         $(CCRNSRC)/source/lssub/comm/nrecipes \
         $(CCRNSRC)/source/lssub/comm/trans \
         $(CCRNSRC)/source/lssub/comm/ocean \
         $(CCRNSRC)/source/lssub/diag/gen \
         $(CCRNSRC)/source/lssub/diag/trans
INCS := -I. $(NETCDF_INCS)
LIBS := $(NETCDF_LIBS)
RM := rm -f
CP := cp -f
SHELL := /bin/sh

# Define dependencies for cccrip

# Keep module names separate from other subroutines
# so that the modules can be compiled first
MSRC  := modpar.f configure.f time_def.f startup.f files_out.f crec_def.f crec_data.f
MOBJ  := $(patsubst %.f,%.o,$(MSRC))

# Get the list of dependencies from a file named DEPENDS
# There is a target below that will create this file
FSUBS := $(filter-out $(MSRC) cccrip.f, $(notdir $(shell cat DEPENDS)))
OBJS  := $(patsubst %.F,%.o,$(patsubst %.f,%.o,$(FSUBS)))
OBJS  := $(sort $(patsubst %.cdk,%.o,$(patsubst %.dk,%.o,$(OBJS))))

# Determine the subset of dependent files that live in the cwd
# This will be used with the sync target
# Note the shell command "pwd" must be invoked with /bin/sh explicitly in
# order to get the correct path on machines in Dorval
CWD := $(shell /bin/sh -c pwd)
LOCAL_FSUBS := $(notdir $(filter $(CWD)/%, $(shell cat DEPENDS)))

# Define a prefix for commands used by the sync target that will
# disable these commands unless the make is run on lx01
SYNC_PREFIX := $(if $(findstring lx01,$(MACH_NAME)), ,@echo "sync allowed on lx01 only:")

# Determine the subset of dependent files that live in ~acrnsrc
# This will be used with the veryclean target
# Ensure that this variable is null when make is run on lx01
CCRNSRC_FSUBS := $(if $(findstring lx01,$(MACH_NAME)), , \
                        $(notdir $(filter $(CCRNSRC)/%, $(shell cat DEPENDS))))

# Define dependencies for cofagg

# Keep module names separate from other subroutines
# so that the modules can be compiled first
MSRC_COFAGG  := 
MOBJ_COFAGG  := $(patsubst %.f,%.o,$(MSRC_COFAGG))

# Get the list of dependencies from a file named DEPENDS_COFAGG
# There is a target below that will create this file
FSUBS_COFAGG := $(filter-out $(MSRC_COFAGG) cofagg.f, $(notdir $(shell cat DEPENDS_COFAGG)))
OBJS_COFAGG  := $(patsubst %.F,%.o,$(patsubst %.f,%.o,$(FSUBS_COFAGG)))
OBJS_COFAGG  := $(sort $(patsubst %.cdk,%.o,$(patsubst %.dk,%.o,$(OBJS_COFAGG))))

# Define dependencies for gsapl

# Keep module names separate from other subroutines
# so that the modules can be compiled first
MSRC_GSAPL  := 
MOBJ_GSAPL  := $(patsubst %.f,%.o,$(MSRC_GSAPL))

# Get the list of dependencies from a file named DEPENDS_GSAPL
# There is a target below that will create this file
FSUBS_GSAPL := $(filter-out $(MSRC_GSAPL) gsapl.f, $(notdir $(shell cat DEPENDS_GSAPL)))
OBJS_GSAPL  := $(patsubst %.F,%.o,$(patsubst %.f,%.o,$(FSUBS_GSAPL)))
OBJS_GSAPL  := $(sort $(patsubst %.cdk,%.o,$(patsubst %.dk,%.o,$(OBJS_GSAPL))))

.SILENT: clean veryclean

all: cccrip

debug:
	@echo MSRC = $(MSRC)
	@echo FSUBS = $(FSUBS)
	@echo OBJS = $(OBJS)
	@echo FSUBS_COFAGG = $(FSUBS_COFAGG)
	@echo OBJS_COFAGG = $(OBJS_COFAGG)
	@echo BINDIR = $(BINDIR)
	@echo NETCDF_INCS = $(NETCDF_INCS)
	@echo NETCDF_LIBS = $(NETCDF_LIBS)
	@echo VPATH = $(VPATH)
	@echo MACH_TYPE = $(MACH_TYPE)
	@echo MACH_NAME = $(MACH_NAME)
	@echo CCRNSRC = $(CCRNSRC)
	@echo CWD = $(CWD)
	@echo LOCAL_FSUBS = $(LOCAL_FSUBS)
	@echo CCRNSRC_FSUBS = $(CCRNSRC_FSUBS)
	@echo SYNC_PREFIX = $(SYNC_PREFIX)

depends_cofagg:
	@echo "Creating dependency file for cofagg"
	fordep --filepath=DEPENDS_COFAGG --filepath_copy --nofilepath_macros --clean cofagg.f

cofagg: $(MOBJ_COFAGG) $(OBJS_COFAGG) cofagg.f
	$(FC) $(FFLAGS_FIXED) $(MOBJ_COFAGG) $(OBJS_COFAGG) $(INCS) $(LIBS) -o cofagg cofagg.f

depends_gsapl:
	@echo "Creating dependency file for gsapl"
	fordep --filepath=DEPENDS_GSAPL --filepath_copy --nofilepath_macros --clean gsapl.f

gsapl: $(MOBJ_GSAPL) $(OBJS_GSAPL) gsapl.f
	$(FC) $(FFLAGS_FIXED) $(MOBJ_GSAPL) $(OBJS_GSAPL) $(INCS) $(LIBS) -o gsapl gsapl.f


%.o: %.f
	$(FC) -c $(FFLAGS_FIXED) $< $(INCS) $(LIBS) -o $*.o

%.o: %.F
	$(FC) -c $(FFLAGS) $< $(INCS) $(LIBS) -o $*.o

%.o: %.F90
	$(FC) -c $(FFLAGS) $< $(INCS) $(LIBS) -o $*.o

# Cancel the implicit RCS extraction rule to avoid getting out of date files
# or unwanted source files copied to the build directory
% :: RCS/%,v

# Define an explicit rule for xit
xit2.o: xit2.cdk
	up2cpp --out_file=xit2.F --overwrite --out_dir=' ' --quiet --gcmver=15g xit2.cdk
	$(FC) $(FFLAGS_FIXED) -c xit2.F -o xit2.o && rm -f xit2.F

xit2.cdk:

install: cccrip
ifeq ($(LOCATION), cccma)
	@# Copy to ~acrnsrc on the local machine
	scp cccrip $(MAINEXE) lx01:$(PUB_BINDIR)
	ssh lx01 chmod 755 $(PUB_BINDIR)/cccrip*
	@# Copy to ~acrnsrc on lxsrv3
	scp cccrip $(MAINEXE) lxsrv3:$(PUB_BINDIR)
	ssh lxsrv3 chmod 755 $(PUB_BINDIR)/cccrip*
endif
ifeq ($(LOCATION), cmc)
	@# Copy to ~acrnsrc on alef
	scp cccrip $(MAINEXE) alef:$(PUB_BINDIR)
	ssh alef chmod 755 $(PUB_BINDIR)/cccrip*
endif

clean:
	rm -fr $(OBJS) $(MOBJ) *.mod $(MAINEXE)

veryclean: clean
	$(RM) *~ $(CCRNSRC_FSUBS)
