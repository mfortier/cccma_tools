#!/bin/bash
# Concatenate CMAM20 netcdf files that were previously converted from CCCma time series
# Larry Solheim  ...Oct,2012

# Create time stamp to be used in file names etc
stamp=`date "+%j%H%M%S"$$`

# Identify, by name, the machine this script is running on
this_host=`hostname|cut -d'.' -f1`

Runame=$(basename $0)
bail(){
  echo "${Runame}: *** ERROR *** $1"
  exit 1
}

# echo without return (ie: emulate -n if not recognized by echo)
if [ "X`echo -n`" = "X-n" ]; then
  echo_n() { echo ${1+"$@"}'\c'; }
else
  echo_n() { echo -n ${1+"$@"}; }
fi

# Add a string to a list without adding any duplicates
add2list(){
  # usage: add2list list_name list_item [start]
  [ -z "$1" ] && bail "add2list: Missing list name"
  [ -z "$2" ] && bail "add2list: Missing list item"
  insert_at_start=0
  if [ -n "$3" ]; then
    if [ "$3" = "start" ]; then
      insert_at_start=1
    else
      bail "add2list: Invalid 3rd arg --> $3 <--"
    fi
    [ $# -gt 3 ] && bail "add2list: Too many command line args --> $* <--"
  fi
  eval THIS_LIST=\"\$$1\"
  if echo $THIS_LIST|/bin/grep -Evq "(^| )$2($| )" ; then
    if [ $insert_at_start -eq 1 ]; then
      # Prepend this string to the list
      if [ -z "$THIS_LIST" ]; then
        THIS_LIST="$2"
      else
        THIS_LIST="$2 $THIS_LIST"
      fi
    else
      # Append this string to the list
      if [ -z "$THIS_LIST" ]; then
        THIS_LIST="$2"
      else
        THIS_LIST="$THIS_LIST $2"
      fi
    fi
  fi
  eval $1=\"$THIS_LIST\"
}

# Get the runid from the ocmmand line
runid=$1
shift
[ -z "$runid" ] && bail "The runid is required on the command line"

# Process the remaining command line args
file_list_cl=''
for arg in "$@"; do
  case $arg in
    *=*) var=`echo $arg|awk -F\= '{printf "%s",$1}' -`
         val=`echo "$arg"|awk '{i=index($0,"=")+1;printf "%s",substr($0,i)}' -`
         case $var in
           file_list) file_list_cl="$val" ;;
           *) bail "Invalid variable definition --> $arg <--" ;;
         esac
         ;;
      *) bail "Invalid command line arg --> $arg <--" ;;
  esac
done

case $runid in
  r02f) # Regular cmam20 experiment

    # ncin_dir will contain all the netcdf file to be concatenated
    ncin_dir=/rd40/data/cmam20/netcdf/r02f_repair/monthly

    # ncout_dir is the dir in which concatenated file will be placed
    ncout_dir=$ncin_dir/concat

    # This will be used as part of a template below
    tag=smf_cmam20

    # Identify a file containing a list of files to be preocessed
    flist_file=${file_list_cl:=file_list_mavg}
    ;;

  yb94cr2f3) # Extended cmam20 experiment

    # ncin_dir will contain all the netcdf file to be concatenated
    ncin_dir=/rd40/data/cmam20/netcdf/extended/monthly

    # ncout_dir is the dir in which concatenated file will be placed
    ncout_dir=$ncin_dir/concat

    # This will be used as part of a template below
    tag=sm

    # Identify a file containing a list of files to be preocessed
    flist_file=${file_list_cl:=file_list_mavg}
    ;;

  *) bail "Invalid runid $runid" ;;

esac

echo "     runid = $runid"
echo "  ncin_dir = $ncin_dir"
echo " ncout_dir = $ncout_dir"
echo "       tag = $tag"
echo " file_list = $flist_file"

# This will be used as a template to find all input netcdf file names
ncname_template='*'_${tag}_${runid}_'[0-9][0-9][0-9][0-9][0-9][0-9]_[0-9][0-9][0-9][0-9][0-9][0-9]_*.nc'

# Possibly set flist to a specific set of file names
# If not set then first look in the file $flist_file, if defined, otherwise
# read the list of files from $cccdir
flist=''

if [ -z "$flist" ]; then
  if [ -n "$flist_file" ]; then
    # Read a file to get the list of file names
    [ ! -f $flist_file ] && bail "$flist_file is not a regular file"
    flist=$(cat $flist_file | grep -v data_description | \
              sed '/^ *#/d; s/\.[0-9][0-9][0-9]$//')
  else
    # Get a list of file names in $ncin_dir
    flist=$(cd ${ncin_dir}; ls -1 $ncname_template | grep -v data_description)
  fi
else
  # Ensure that there are no data_description files in this list
  flist=$(echo $flist | tr '[ ]' '[\n]' | grep -v data_description)
fi

# After the following loop finishes, template will contain indicies of the form "${pfx}-${sfx}"
# that identify a list of unique prefix and suffix pairs found in the list of input files.
# The value assigned to each index will be a file name template that may be used to list
# all files associated with a particular variable.
# File names are of the form ${pfx}_${range}_${sfx}
declare -A template

for data_file in $flist; do

  [ -z "$data_file" ] && bail "Empty input file name found in flist"

  # Extract a prefix, date range and suffix from this file name
  # The netcdf file name is of the form
  #     *_${runid}_${start}_${stop}_${ftype}_${label}.nc
  # where
  #   vname = netcdf variable name
  #   runid = experiment id
  #   start = starting date in the form YYYYMM
  #   stop  = stopping date in the form YYYYMM
  #   ftype = file type (either gp6 or td)
  #   label = super label modified to be part of a valid file name
  parts=$(echo $data_file | awk -F '_' -v runid=$runid '\
            BEGIN {pfx=""; range=""; sfx=""}
            { for (i=1; i<=NF; i++) {
                if ($i == runid) {
                  r = i    # index of runid
                  a = i+1  # index of start
                  b = i+2  # index of stop
                  f = i+3  # index of ftype
                  if (f>NF) {
                    printf "File name too short: %s",$0 > "/dev/stderr"
                    exit 1
                  }
                  if ($a !~ /[0-9][0-9]*/) {
                    printf "Invalid starting date range: %s",$0 > "/dev/stderr"
                    exit 1
                  }
                  if ($b !~ /[0-9][0-9]*/) {
                    printf "Invalid stopping date range: %s",$0 > "/dev/stderr"
                    exit 1
                  }
                  # Prefix is every field from the start of the file name to runid
                  for (k=1; k<=r; k++) {
                    if (k==r) {
                      pfx = pfx sprintf("%s",$k)
                    } else {
                      pfx = pfx sprintf("%s_",$k)
                    }
                  }
                  range = sprintf("%d_%d",$a,$b)
                  for (k=f; k<=NF; k++) {
                    if (k==NF) {
                      sfx = sfx sprintf("%s",$k)
                    } else {
                      sfx = sfx sprintf("%s_",$k)
                    }
                  }
                  exit
                }
              }
            }
            END {printf "%s %s %s",pfx,range,sfx}' -) ||
    bail "Unable to extract parts from file name $data_file"

  # Split parts into separate variables
  read pfx range sfx <<< "$parts"

  [ -z "$pfx" ]   && bail "Unable to determine prefix from $data_file"
  [ -z "$range" ] && bail "Unable to determine range from  $data_file"
  [ -z "$sfx" ]   && bail "Unable to determine suffix from $data_file"

  # echo "$data_file   pfx=$pfx range=$range sfx=$sfx"

  template[${pfx}-${sfx}]="${pfx}_*_${sfx}"

  # echo "template = ${template[${pfx}-${sfx}]}"

done

# Concatenate all files containing the same variable over the entire time period
# and put the output in $ncout_dir
[ -z "$ncout_dir" ] && bail "ncout_dir is not defined"
[ ! -d "$ncout_dir" ] && bail "$ncout_dir is not a directory"
cd $ncout_dir
nlist_all=-1
for patt in "${template[@]}"; do
  # This will list all files containing the current variable in alphabetical order
  # which implies ascending order of time
  flist=$(ls -1 $ncin_dir/$patt | tr '[\n]' '[ ]') || \
    bail "Failure trying to find files matching $patt"
  [ -z "$flist" ] && bail "Unable to find any files matching $patt"

  # Determine a first and last date from the list of date ranges found in
  # the list of files associated with the current variable
  RANGE_LIST=''
  for data_file in $flist; do
    [ -z "$data_file" ] && bail "Empty input file name found in $flist"

    # Extract the date range embedded in this file name
    range=$(echo $data_file | awk -F '_' -v runid=$runid '\
              { for(i=1; i<=NF; i++) {
                  if ($i == runid) {
                    a = i+1
                    b = i+2
                    printf "%d_%d",$a,$b
                    exit
                  }}}' -)
    [ -z "$range" ] && bail "Unable to determine date range from $data_file"

    # Add this range to the range list if not already there
    add2list RANGE_LIST $range
  done

  # Ensure range list is sorted in ascending order
  RANGE_LIST=$(echo "$RANGE_LIST" | sort -n)
  nlist=$(echo "$RANGE_LIST" | wc -w)

  # Verify that all variables contain same number of sub ranges
  if [ $nlist_all -eq -1 ]; then
    nlist_all=$nlist
  else
    if [ $nlist -ne $nlist_all ]; then
      # bail "Abnormal range list for pattern --> ${patt} <-- $RANGE_LIST"
      echo "Abnormal range list for pattern --> ${patt} <-- $RANGE_LIST"
      continue
    fi
  fi

  # Extract the first and last date range from this list
  n=0
  date1=''
  date2=''
  for range in $RANGE_LIST; do
    n=`expr $n + 1`
    if [ $n -eq 1 ]; then
      # This is the first range in the list
      date1=$(echo $range | awk -F_ '{print $1}' -)
    fi
    if [ $n -eq $nlist ]; then
      # This is the last range in the list
      date2=$(echo $range | awk -F_ '{print $2}' -)
    fi
  done
  [ -z "$date1" ] && bail "Unable to determine start date from range: $RANGE_LIST"
  [ -z "$date2" ] && bail "Unable to determine stop date from range: $RANGE_LIST"

  pfx=$(echo "$patt"|sed 's/_\*_.*$//')
  sfx=$(echo "$patt"|sed 's/^.*_\*_//')

  # The output file name will contain first and last dates from all input files
  oname="${pfx}_${date1}_${date2}_${sfx}"

  # Concatenate all input files for the current variable
  # echo "ncrcat -o $oname -h -O $flist"
  ncrcat -o $oname -h -O $flist
  echo "Created $ncout_dir/$oname"
done
