#!/bin/sh

#   Dec 23/08 - F.Majaess (Change default to "link" mode, "nocp=no" forces "cp")
#   May 25/05 - F.Majaess (Use "-L" instead of parsing "file" command output)
#   Jan 21/99 - F.Majaess (Added yo/yonaka)
#   Jul 14/97 - F.Majaess (Revise for hiru)
#   Aug 26/96 - F.Majaess (Revise to use "samefs" instead of relying on "df")
#   Jul 15/96 - F.Majaess (Revise for SX4)
#   Dec 08/95 - F.Majaess (Replace 'hostname' calls by "$HOSTID")
#   Sep 25/95 - F. Majaess (Add '-h' to 'file' on SGI)
#   Aug 15/95 - F. Majaess (Implement 'scanobe' switch feature)
#   Jun 05/95 - F. Majaess (Re-enable checking sx3 from sx3r)
#   May 29/95 - F. Majaess (temporarily disable checking sx3 from sx3r)
#   Feb 13/95 - F. Majaess (enforce "pfn" to be in lower case)
#   Jan 04/95 - F. Majaess (Add nocp switch)
#   Nov 16/94 - F. Majaess (Modify for sx3r)
#   Jan 31/94 - F. Majaess (Ensure error detection due to filesystem failure)
#   Dec 29/93 - F. Majaess (Modify for accessing files from other than
#                           "$RUNPATH")
#   Nov 25/92 - E. Chan (Modify for accessing files across multiple file
#                        systems)
#   Sep 10/91 - E. Chan

#id access - creates a symbolic link to a file saved using the "save" script
 
#   AUTHOR  - E. Chan

#hd PURPOSE - If "nocp=no"; then:
#hd            "access" copies the permanent dataset into "fn" file in the 
#hd             current directory as well as creating ".fn"_Link soft link 
#hd             to the permanent dataset.
#hd           otherwise,
#hd            "access" creates a symbolic link to a file residing in either 
#hd             the user's own '$RUNPATH' directory, or in some other file
#hd             system. Paths to files on other file systems are found using
#hd             the symbolic links created in the official CCRD/N data 
#hd             directory ('$DATAPATH') by the "save" script. 
#hd
#hd           By default, the link is made to the highest edition of the file 
#hd           in the directory (the edition number is included in the filename
#hd           as a three digit extension following a period).

#pr PARAMETERS:
#pr
#pr   POSITIONAL
#pr
#pr     fn     = name of link in directory from which "access" is invoked
#pr   
#pr     pfn    = name of saved file   
#pr
#pr   PRIMARY
#pr
#pr     ed     = edition number of saved file (default: the latest edition)
#pr                                                   
#pr     dpalist= switch which can be set to enable/disable the use of 
#pr              ".datapath.list.access" local cache file of DATAPATH 
#pr              subdirectory listing to speedup the script execution.
#pr              This mode is used only if no "ed" input is specified.
#pr              And, for interactive session, it's honoured only if 
#pr              explicitily specified,(ie. dpalist=... ),on the command
#pr              line otherwise it's assumed to be set to "off/no" 
#pr              equivalent (see below).
#pr              =on/yes; create ".datapath.list.access" file if not present,
#pr                       then use it to extract "pfn" highest edition found
#pr              =new;    create ".datapath.list.access" file then use it 
#pr                       to extract "pfn" highest edition found
#pr              =off/no; delete ".datapath.list.access" file if present,
#pr                       scan DATAPATH subdirectory for "pfn" highest edition
#pr              unset;  default mode is to scan DATAPATH subdirectory for 
#pr                      "pfn" highest edition
#pr
#pr   PRIMARY/SECONDARY
#pr
#pr     na     = switch that, if set, ensures the return of exit status 0 
#pr              (i.e. 'no abort') even if access fails (=''/'na')
#pr
#pr     nocp   = switch which can be set to 'no' (ie. "nocp=no") in order 
#pr              to force making a local "fn" copy of the targeted "pfn".
#pr              (='yes'/'no')
#pr
#ex EXAMPLE:
#ex   
#ex     access data datfile ed=3
#ex
#ex     The above example creates a symbolic link called 'data' in the 
#ex     directory from which "access" is invoked to 'datfile.003' is residing
#ex     on the same filesystem, otherwise file 'datfile.003' is copied into
#ex     'data' and a link '.data_Link' pointing to 'datfile.003' is established.
#ex     The saved file may reside in either the user's own '$RUNPATH' directory 
#ex     or in another file system as long a symbolic link to that file exists
#ex     on '$DATAPATH' (the official CCRD/N data directory).
#ex
#ex     access data datfile ed=3 nocp=no
#ex
#ex     The above will result in copying 'datfile.003' into 'data' file in the
#ex     subdirectory from which 'access' was invoked. 
#ex
#ex     access data datfile dpalist=on
#ex
#ex     The above will result in creating ".datapath.list.access" cache file 
#ex     of DATAPATH subdirectory listing if not already present, then use it 
#ex     to scan for "pfn" highest edition and setup "data" symbolic link to it.
#ex

#  * Reset field separators (otherwise those defined in the parent process
#  * will be used and these may cause problems if they include special 
#  * characters used in this script). 

IFS=' '
export IFS

#  * Capture command line arguments in "arg_list"

arg_list=$@

# Ensure "Shell_Mode" variable is defined.

if [ "$SITE_ID" = 'Dorval' ] ; then
 case "$-" in
  *i*)  Shell_Mode='interactive' ;;
  *) Shell_Mode='not interactive' ;;
 esac
 if [ -n "$SSH_TTY" ] ; then Shell_Mode='interactive' ; fi
fi

# Restrict "dpalist" option use for "interactive" mode 
# to "dpalist=..." be specified on the command line.

if [ "$SITE_ID" = 'Dorval' -a "$Shell_Mode" = 'interactive' ] ; then
  dpalist=''
fi

#  * Obtain the file names and any specified option.

for arg in $arg_list
do 
  case $arg in
      -*) set $arg                                         ;;
    ed=*) ed=`expr $arg : 'ed=\([0-9]*\)'`                 ;;
     *=*) eval "$arg"                                      ;;
      na) na='na'                                          ;;
    nocp) nocp='yes'                                       ;;
       *) tmp1=${fn:+$arg} ; pfn=${pfn:=$tmp1}              ; 
          fn=${fn:=$arg}
  esac
done

#  * Set variable 'AWK' to the version of "awk" to be used.

AWK=${AWK:='awk'}

# nocp=${nocp:='no'}
# Enable the following line to have default (ie. to favour) "link" setup.
  nocp=${nocp:='yes'}
if [ "$nocp" = 'on' ] ; then
 nocp='yes'
elif [ "$nocp" = 'off' ] ; then
 nocp='no'
fi
pfn=`echo $pfn | tr '[A-Z]' '[a-z]'`
DIRLIST='.datapath.list.access'

# For "interactive" session case, set "dpalist='no'" if it's not 
# explicitly specified in the comand line.

if [ "$SITE_ID" = 'Dorval' ] ; then
 if [ "$Shell_Mode" = 'interactive' -a -z "$dpalist" ] ; then
  dpalist='no'
 fi
 if [ "$dpalist" = 'on' ] ; then
  dpalist='yes'
 elif [ "$dpalist" = 'off' ] ; then
  dpalist='no'
 fi
 if [ "$dpalist" = 'no' ] ; then
  (\rm -f $DIRLIST || : )
 fi
fi

unset DefDATAPATH
if [ "$SITE_ID" = 'Dorval' ] ; then
 HOSTIDf=${HOSTIDf:=`echo $HOSTID | cut -c 1-3`}
 JHOMEinDATAPATH=`echo $DATAPATH | sed -e 's/^.*JHOME_.*$/Yes/'`
 if [ "$OS" = 'AIX' ] ; then
  case $HOSTIDf in
   c1f*|c1h*|c1r*|c1s* ) if [ "$DATAPATH" != '/fs/dev/crb/spd_data/data' ] ; then DefDATAPATH='/fs/dev/crb/spd_data/data' ; fi ;;
   c2f*|c2h*|c2r*|c2s* ) if [ "$DATAPATH" != '/fs/dev/crb/had_data/data' ] ; then DefDATAPATH='/fs/dev/crb/had_data/data' ; fi ;;
                  c8f* ) if [ "$DATAPATH" != '/fs/c8/crb/ald_data/data' ] ; then DefDATAPATH='/fs/c8/crb/ald_data/data' ; fi ;;
     * ) unset DefDATAPATH ;;
  esac
 elif [ "$OS" = 'Linux' ] ; then
  case $HOSTIDf in
   # ale|erg ) if [ "$DATAPATH" != '/fs/cava/dev/crb/sas/ccrnd/lnx_data/data' ] ; then DefDATAPATH='/fs/cava/dev/crb/sas/ccrnd/lnx_data/data' ; fi ;;
   ib3|ib4|ib8) if [ "$DATAPATH" != '/fs/cava/dev/crb/sas/ccrnd/lnx_data/data' ] ; then DefDATAPATH='/fs/cava/dev/crb/sas/ccrnd/lnx_data/data' ; fi ;;
         * ) unset DefDATAPATH ;;
  esac

 fi

fi


#  * Generate the edition number and file name to be accessed.

if [ -n "$ed" ] ; then

  #  * Case 1: Edition number is provided.

  #  * Format edition number
 
  ed=`echo $ed | $AWK '{printf "%03d",$1}'`

  #  * Obtain line containing the absolute path to the desired file.

  line=`ls -l $DATAPATH/$pfn.$ed 2>/dev/null`
  if [ -z "$line" ] ; then
    if [ -n "$DefDATAPATH" -a -d "${DefDATAPATH}/." ] ; then line=`ls -l $DefDATAPATH/$pfn.$ed 2>/dev/null` ; fi
    if [ -n "$line" ] ; then
     file=`expr "$line" : '.* \{1,\}\([^ ]*\)$'`
     unset line
     if [ "$DATAPATH" = "$RUNPATH" -a -s "$file" ] ; then
      filexx=`basename $file`
      if [ "$JHOMEinDATAPATH" = 'Yes' ] ; then
       if samefs $file $RUNPATH ; then
        ln $file $RUNPATH/$filexx &&
        echo "access: Setup $RUNPATH/$filexx hardlink to $file " ||
        (cp -p $file $RUNPATH && echo "access: Copied $file into $RUNPATH" )
       else
        cp -p $file $RUNPATH &&
        echo "access: Copied $file into $RUNPATH"
       fi 
     #else
     # ln -s $file $RUNPATH/$filexx
     # echo "access: Setup $RUNPATH/$filexx softlink to $file "
      fi
      line=`ls -l $RUNPATH/$pfn.$ed 2>/dev/null`
      if [ -f $DIRLIST ] ; then
       (\rm -f $DIRLIST || : )
       dirplist > $DIRLIST
      fi
     fi
     unset file
    fi
  fi

else

  #  * Case 2: Edition number is not provided. 


  #  * Obtain line containing the absolute path to the latest existing 
  #  * edition of the desired file.

  # if [ "$SITE_ID" = 'Dorval' -a \( "$dpalist" = 'yes' -o "$dpalist" = 'new' \)  -a "$Shell_Mode" = 'not interactive' ] ; then
  if [ "$SITE_ID" = 'Dorval' -a \( "$dpalist" = 'yes' -o "$dpalist" = 'new' \) ] ; then
   # create a file list of DATAPATH, if it does not exist
   #if [ ! -s $DIRLIST -o ${DATAPATH}/. -nt $DIRLIST ] ; then
   if [ "$dpalist" = 'new' ] ; then
    (\rm -f $DIRLIST || : )
   fi
   if [ ! -s $DIRLIST ] ; then
    echo "Create $DIRLIST"
    dirplist > $DIRLIST
   fi
   
   # get the highest edition number
   ed=`egrep "^${pfn}"'[.][0-9][0-9][0-9]$' $DIRLIST | sort | tail -1 | sed -e 's/.*[.]\([0-9][0-9][0-9]\)/\1/'`

   if [ -n "$ed" ] ; then

    #  * Obtain line containing the absolute path to the latest existing 
    #  * edition of the desired file.

    line=`ls -l $DATAPATH/$pfn.$ed 2>/dev/null`
   fi

  elif [ "$SITE_ID" = 'Dorval' ] ; then
   line=`dirplist | sed -e 's/\@ *$//g' | egrep "^${pfn}"'[.][0-9][0-9][0-9]$' | sort |  tail -1`
   if [ -n "$line" ] ; then
    line=`ls -l $DATAPATH/${line} 2>/dev/null`
   fi
  else
   line=`ls -l $DATAPATH/$pfn.[0-9][0-9][0-9] 2>/dev/null | tail -1` 
  fi
  if [ -z "$line" ] ; then
   if [ -n "$DefDATAPATH" -a -d "${DefDATAPATH}/." ] ; then line=`ls -l $DefDATAPATH/$pfn.[0-9][0-9][0-9] 2>/dev/null | tail -1` ; fi
   if [ -n "$line" ] ; then
    file=`expr "$line" : '.* \{1,\}\([^ ]*\)$'`
    unset line
    if [ "$DATAPATH" = "$RUNPATH" -a -s "$file" ] ; then
     # cp -p $file $RUNPATH
     # echo "access: Copied $file into $RUNPATH"
     filexx=`basename $file`
     if [ "$JHOMEinDATAPATH" = 'Yes' ] ; then
      if samefs $file $RUNPATH ; then
       ln $file $RUNPATH/$filexx &&
       echo "access: Setup $RUNPATH/$filexx hardlink to $file " ||
       (cp -p $file $RUNPATH && echo "access: Copied $file into $RUNPATH" )
      else
       cp -p $file $RUNPATH
       echo "access: Copied $file into $RUNPATH"
      fi
    #else
    # ln -s $file $RUNPATH/$filexx
    # echo "access: Setup $RUNPATH/$filexx softlink to $file "
     fi
     line=`ls -l $RUNPATH/$pfn.[0-9][0-9][0-9] 2>/dev/null | tail -1`
     if [ -f $DIRLIST ] ; then
      (\rm -f $DIRLIST || : )
      dirplist > $DIRLIST
     fi
    fi
    unset file
   fi
  fi

  if [ -z "$line" ] ; then
   line=`ls -l $CCRNTMP/recover/$pfn.999 2>/dev/null | tail -1`
  fi

fi

#  * Possibly check the other back-end machine $DATAPATH subdirectory
#  * if running on the back-end and "$scanobe is 'on'".

## scanobe=${scanobe:='off'}
## if [ "$scanobe" = 'on' ] ; then
## if [ "$HOSTID" = 'hiru' -o "$HOSTID" = 'yonaka' -o "$HOSTID" = 'asa' ] ; then
##  if [ -z "$line" ] ; then
##   case "$HOSTID" in
##       asa ) sbemchn='hiru'    ; sdatapath='/sx/crba_src/data'    ;;
##       hiru) sbemchn='asa'     ; sdatapath='/data/asa_ccrd/src/data' ;;
##     yonaka) sbemchn='asa'     ; sdatapath='/data/asa_ccrd/src/data' ;;
##          *) ;;
##   esac
##   if [ -n "$ed" ] ; then
##    line=`rsh $sbemchn ls -l $sdatapath/$pfn.$ed | tail -1 | grep $pfn`
##   else
##    line=`rsh $sbemchn ls -l $sdatapath/$pfn.[0-9][0-9][0-9] | tail -1 | grep $pfn`
##   fi
##  fi
## fi
## fi

scanobe=off

#  * Extract the absolute path to the desired file.

file=`expr "$line" : '.* \{1,\}\([^ ]*\)$'`

#  * Set the error level based on the 'na' option. Also set the file
#  * status parameter (=0 if $fn exists and >0 if $fn doesn't exist).

if [ -n "$na" ] ; then
  error='Warning'
  level=0
else
  error='  Abort'
  level=1
fi

#  * Verify that the file to be linked exists and that the name of the
#  * link does not already exist.

fn_status=`ls $fn 2>/dev/null`

if [ -z "$file" ] ; then

  ed=${ed:='001'}
  echo "$error in ACCESS: file $pfn.$ed does not exist"
  exit $level

elif [ -n "$fn_status" ] ; then

  echo "$error in ACCESS: $fn already exists" 
  exit $level

fi

#  * Extract the path and compare against the current filesystem.
#  * If the file resides on the same filesystem as the current
#  * subdirectory then create a symbolic link to that file with 
#  * the name '$fn', otherwise, copy the file into the current
#  * directory possibly from a remote back-end machine and setup
#  * ".""$fn""_Link" link to keep track where the local file came
#  * from.

path=`expr $file : '\(.*\)/'`
# # if [ -n "$sbemchn" ] ; then
#  pflstm=`df $path 2>/dev/null | tail -1 | sed -n -e 's/ .*$//p' `
# # fi
# cflstm=`df ./    | tail -1 | sed -n -e 's/ .*$//p' `
# 
# if [ "$pflstm"  = "$cflstm" -o "$nocp" != 'no' ] ; then
### xx=`samefs ./ $path 2>/dev/null`
# if [ "$?" -eq 0 -o "$nocp" != 'no' ] ; then
### if [ \( "$?" -eq 0 -a "$nocp" != 'no' \) -o "$nocp" = 'yes' ] ; then
if [ "$nocp" != 'no' ] ; then
   Lfn=".""$fn""_Link"
   (\rm -f $Lfn 2>/dev/null || : )
   ln -s $file $fn
   if [ "$?" -ne 0 ] ; then
     echo "$error in ACCESS: link $fn can not be established" 
     echo "                  to $file file"
     exit $level
   fi
else
# if [ -n "$sbemchn" ] ; then
#  Xpctsz=`rsh $sbemchn ls -al $file | $AWK '{ print \$5 ; }' ` 
#  rcp -p $sbemchn:$file $fn
#  Actlsz=`ls -al $fn | $AWK '{ print \$5 ; }' `
#  if [ -z "$Xpctsz" -o "$Xpctsz" != "$Actlsz" ] ; then
#   echo "$error in ACCESS: file $sbemchn:$file has not been copied"
#   echo "                  into $fn"
#   exit $level
#  fi
# else
   cp -p $file $fn
   if [ "$?" -ne 0 ] ; then
     echo "$error in ACCESS: file $file has not been copied"
     echo "                  into $fn"
     exit $level
   fi
# fi
  Lfn=".""$fn""_Link"
  unset link
 #if [ -L "$Lfn" ] ; then
 #  \rm -f $Lfn 2>/dev/null
 #fi
  (\rm -f $Lfn 2>/dev/null || : )
  ln -s $file $Lfn
  if [ "$?" -ne 0 ] ; then
   echo "$error in ACCESS: link $Lfn can not be established" 
   echo "                  to $file file"
   exit $level
  fi
fi
if [ "$path" = "$CCRNTMP/recover" ] ; then
  echo "Warning from ACCESS: file $file is used for $fn, you should"
  echo "                     run 'rcvrfils' prior to resubmitting" 
  echo "Warning from ACCESS: file $file is used for $fn, you should" >> haltit
  echo "                     run 'rcvrfils' prior to resubmitting" >> haltit
fi

exit
