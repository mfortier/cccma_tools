#! /bin/sh
 
#    Mar 01/06 - F.Majaess (Revised for IBM "rigel" at CMC )
#    Jun 20/03 - F.Majaess (Revised for IBM "azur"  at CMC )
#    May 28/98 - F.Majaess (Revised to check for [empty,blank] [first,last] line)
#    JUN 10/96 - F.Majaess
 
#id  obtnfil - Obtain a copy of the file specified.
 
#    AUTHOR  - F.Majaess
 
#hd  PURPOSE - "obtnfil" script is used to obtain a copy of the 
#hd            file specified unless a copy of it already exist
#hd            in the current subdirectory in which case a checking
#hd            for code consistency is done instead.
#hd            NOTE: mainly used to gather files processed by "archsub".
 
#pr  PARAMETERS:
#pr
#pr    POSITIONAL     
#pr
#pr      pathfil = path/filename containing source code. 
#pr
#pr    SECONDARY
#pr
#pr       nocp  = switch to allow linking instead of copying;
#pr               mainly used to verify code consistency.
#pr               (=no/yes).
#pr
#pr
#ex  EXAMPLE: 
#ex
#ex    obtnfil ~acrnsrc/source_linux/lssub/comm/gen/xit.f 
#ex
#ex   The above will result in simply copying "xit.f" content into a file
#ex   in the local subdirectory if none exists, otherwise it will perform
#ex   a check of the local copy versus that in the specified location.

AWK=${AWK:-awk}

#  * Obtain the file names and any specified option.

arg_list=$@
for arg in $arg_list
do
  case $arg in
      -*) set $arg                                  ;;
    nocp) nocp=yes                                  ;;
       *) tmp1=${pathfil:+$arg} ; 
          pathfil=${pathfil:=$arg}
  esac
done
 
if [ -d "$pathfil" ] ; then
# echo "Warning from 'obtnfil': skipped processing subdirectory $pathfil"
  exit 0
elif [ ! -f "$pathfil" ] ; then
  echo "Abort in 'obtnfil': target file $pathfil does not exist!"
  exit 1
fi

#  * Extract filename and path information ...

tarpath=`expr $pathfil : '\(.*\)/'`
tarfile=`expr //$pathfil : '.*/\(.*\)'`

cdpath=`pwd`
nocp=${nocp:='no'}

if [ "$cdpath" = "$tarpath" ] ; then
  echo "Warning from 'obtnfil': current and target subdirectories should be different!"
  exit 0
fi

#   ****   Task of the script...   ****
 
if [ ! -f "$tarfile" ] ; then
 if [ "$nocp" != 'no' ] ; then
  ln -s $pathfil $tarfile
 else
  \cp $pathfil .
 fi
else
 if [ "$SITE_ID" = 'Victoria' -o \( "$SITE_ID" = 'Dorval' -a "$OS" = 'AIX' \) ] ; then
  diff $tarfile $pathfil > /dev/null || abnormal='true'
 else
  diff -q $tarfile $pathfil || abnormal='true'
 fi
fi
if [ -n "$abnormal" -a "$abnormal" = "true" ] ; then
 touch Files_Differ
 \ls -al $tarfile $pathfil
fi
if [ -s "$tarfile" ] ; then
  chkblnk=`sed -n -e '1s/^$/ ---> Empty first line.../p' \
                  -e '1s/^ *$/ ---> Blank first line.../p' \
                  -e '$s/^$/ ---> Empty last  line.../p' \
                  -e '$s/^ *$/ ---> Blank last  line.../p' $tarfile`
  if [ -n "$chkblnk" ] ; then
   echo "\n0 obtnfil: Warning, detected the following(s) in :> $tarfile <: file:"
   echo "${chkblnk}"
  fi
else
  echo "\n0 obtnfil: Warning, :> $tarfile <: file is empty!\n"
fi
exit
