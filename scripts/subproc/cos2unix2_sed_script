#  *        cos2unix_sed_script
#  * ----------------------------------- E. Chan - Jan 12/94.
#  *                                     Sed script for "cos2unix" script.
#  *                                     Translates COS job deck into the 
#  *                                     equivalent Unix code. 

#
#  * Some preliminary transformations.
#

#  * Remove all *EOR/*EOF lines except the last one.

/^\*EO[RF]/{
$!d
}

#  * Translate the DECK and COMDECK directives.

s/^\*DECK/#deck/
s/^\*COMDECK/#comdeck/

#  * Translate job card. Support 2 formats.

s/^ *JOB *( *JN=\([^,]*\), *T=\([^,]*\), *MFL=\([^)]*\)).*$/#jobname=\1 ; time=\2 ; memory=\3/

/^JOB(JN=\([^,]*\),T=\(\$[^$]*\$\)\$\([^$]*\)\$/{
s//#jobname=\1 ; time=\2 ; memory=$\3 %_%/
s/%_%[^(]*(QUEUE=\([^)]*\)).*$/; queue=\1/
s/%_%.*//
s/JCLX  */memory1 /
s/JCLX1  */memory2 /
s/JCLXB  */memory3 /
s/JCLX/memory/
}

#  * Add temporary label if this is a comdeck with data.

2{
/^+/s/^\(.*\)$/cat Input_Cards\
\1/
}

#
#  * Transformations applicable to both jcl and input sections.
#

#  * Translate update CALL directives to Unix equivalent.

s/^ *\*CALL \{1,\}\([^ ]*\)/.   \1.cdk/

#  * Translate condef constructs to Unix equivalent.

/=IF,/{
y/ABCDEFGHIJKLMNOPQRSTUVWXYZ/abcdefghijklmnopqrstuvwxyz/
s/gcm2\+/gcm2plus/
s/,+\([^ ]*\)/\1" = on ] ; then/
s/,-\([^ ]*\)/\1" != on ] ; then/
s/=if/    if [ "$/
}
s/^ *=END,.*/    fi/

#  * Insert braces into parmsub variables before translation to shell
#  * variables (only when needed).

s/\$\([^ $]*\)\$/$\1%+%/g
s/\$\([^ $]*\)%+%\([a-zA-Z0-9*]\)/${\1}$\2/g
s/%+%/$/g

#
#  * Translate jcl portion of job deck.
#   

/cat .*Input_Cards/,$!{

/ *\.  *JCLDAT.cdk/,$!{

#  * Loop over each filename on a command line and compress  
#  * out any blanks (The commented out line replaces the blanks
#  * with underscores). Also translate asterisks in filenames to
#  * underscores.

/^# /!{
/ *if/!{
:label1
s/\([(,[] *[^] ,)]\{1,\}\)\*\{1,\}\([^] ,)]\{1,\}\)/\1_\2/g
#s/\([(,[] *[^] ,)]\{1,\}\) \{1,\}\([^] ,)]\{1,\}\)/\1_\2/g
s/\([(,[] *[^] ,)]\{1,\}\) \{1,\}\([^] ,)]\{1,\}\)/\1\2/g
t label1
}
}

#  * Translate all lines to lower case.

1,$y/ABCDEFGHIJKLMNOPQRSTUVWXYZ/abcdefghijklmnopqrstuvwxyz/

#  * Remove trailing blanks on all lines.

s/ *$//

#  * Insert underscores between any two consequetive commas.

s/,,/,_,/g
s/,,/,_,/g

#  * Modify COS access, save, delete, rewind, and release commands to
#  * remove optional parameters other than "na" or "ed". Also remove the
#  * "dn" and "pdn" specifiers in the command lines. This is required
#  * for the Unix script equivalents of these commands.

/ed=/s/, *\(ed=[^,]*\)\(.*\))/\2%,\1)/
/na[ ,)%]/s/, *\(na\)[^%]*%*\(.*\))/%\2,\1)/
/pdn=/s/( *dn=\(.*,\) *pdn=\([^,%]*\)[^%]*%*\(.*\))/ \1\2\3/
/dn=/s/( *dn=\([^%)]*\)[^%]*%*\(.*\))/ \1\2/

#  * Translate COS loops to a Unix while-do-done loop. "EXITLOOP" is
#  * replaced by a Unix conditional block that breaks out of the loop
#  * when the test condition is met.

/^loop./s//while [ "$?" -eq 0 ]\
    do/

/exitloop/{
s/exitloop\(.*\)/if\1/
a\
\        break\
\      fi
}

s/endloop./done/

#  * The COS command "SET" is replaced with a function "set_switch"
#  * defined and invoked in "com_script" at the beginning of every job.
#  * "Set_switch" stores global switches in a temporary file for the job.
#  * The switch is read back in with the function "get_switch". A "$"
#  * is added to the first character of each variable on the RHS of the 
#  * expression to enable Unix parameter substitution to take place when
#  * the script is executed.

/^set(/{
s/set(\(.*\))/set_switch(\1)/
s/\([-+*/= ]\)\([a-z]\)/\1$\2/g
}

#
#  * Translate conditional blocks to the equivalent in Unix.
#

/^if(/{ 

#  * Translate ".OR." and ".AND." and add a call to the function 
#  * "get_switch"

s/.or./ -o /
s/.and./ -a /
s/if(/get_switch()\
if(/

#  * Loop over and translate all the test conditions. 

:label2
s/(\(.*\))\nif\(.*[( ]\)\([^ ]*\)\.\([a-z][a-z]\)\.\(.*\))/(\1,\3)\
if\2 "$\3" -\4 \5)/
t label2

#  * Recognize string comparisons when quotes are used.

/"\$\(["'].*["']\)"/{
s//\1/g
s/get_switch(.*)\n//
}
/-eq '/s/-eq/=/g
/-ne '/s/-ne/!=/g

#  * Perform a final translation to the Unix "if" conditional test
#  * and transform parentheses to square brackets.

/(,\([^)]*\))/s// \1 /
s/if(/    if [/
/) *$/s// ] ; then/

}

#  * Translate COS "ELSE" to Unix "else".

s/else./    else/

#  * Translate COS "ENDIF" to Unix "fi".

s/endif./    fi/

#  * Use "ccc" script to invoke all programs. All lines are translated
#  * so non-program executable statements must be translated back.
#  * Delete lines involving rewind and exit.

/^ *[a-z]/s/^/ccc /
s/ccc access/    access/
s/ccc save/    save/
s/ccc delete/    delete/
s/ccc libncar/    libncar/
s/ccc get_switch/    get_switch/
s/ccc set_switch/    set_switch/
s/ccc \( *\)if/\1if/
s/ccc \( *\)fi/\1fi/
s/ccc \( *\)else/\1else/
s/ccc while/    while/
s/ccc done/    done/
s/ccc copyd *( *i=\([^ ]*\) *, *o=\([^ ]*\).*$/    cp \1 \2/
s/ccc \( *\)release/    \1rm/
s/ccc \( *\)mv/    \1mv/
/ccc rewind/d
/ccc exit/d
/ccc[^(]*$/s/ccc/   /

#  * Translate commas, parentheses, and colons to spaces. Also remove
#  * terminating periods on command lines.

/ *#/!s/[:,()]/ /g
/ *#/!s/\. *$//

#  * Convert parmsub variables to shell variables.

s/\$\([^ $]*\)\$/$\1/g

#  * Shift all comment lines after line 2 one space to the right.

3,$s/^#/# /

#  * Add leading blanks to the beginning of lines within conditional blocks
#  * and loops.

/^  *while/,/^  *done/{
/^  *while/b
/^  *done/b
s/if \[/  if [/
s/^ccc/ccc  /
s/^\([#.]\)/\1  /
s/^ /   /
}

/^  *if \[/,/^  *fi/{
/^  *if \[/b
/^  * fi/b
s/^ccc/ccc  /
s/^\([#.]\)/\1  /
s/^ /   /
}


#  * Insert lines to complete the transformation of the jcl section.

/\. *comjcl.cdk/a\
\
cat > Execute_Script <<'end_of_script'\
 

s/^\. \{1,\}endjcl.*$/\
end_of_script\
\
cat > Input_Cards <<end_of_data\
/

#  * Remove "#" from jobname line.

s/#jobname/jobname/

}

}

#
#  * Translate input section of job deck
#

/cat .*Input_Cards/,${

s/ *\. \{1,\}JCLDAT.*$/+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :/

#  * Translate all comdeck calls to lower case.

/^ *\. \{1,\}[^ ]\{1,\}/{
y/ABCDEFGHIJKLMNOPQRSTUVWXYZ/abcdefghijklmnopqrstuvwxyz/
s/^ *\.  /./
}

#  * Format the condef blocks.

s/^ *if/if/
s/^ *fi/fi/
s/\n *if/\
\
if/

#  * Translate the section separator.

s/+W 00.*$/+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :/

#  * Append lines to complete the input section and the script. Generate
#  * a non-empty file 'lastline' to indicate that an enddata statement
#  * existed in the job and has been translated. This is passed
#  * on to the script 'jobtran', if required.

$a\
end_of_data

/^ *\.  *enddata.*$/{
w lastline
a\
\
. endjcl.cdk\
\

d
}

/^\*EO[RF]/d

}
