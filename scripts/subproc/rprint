#! /bin/sh
 
#    NOV 19/99 - F.Majaess (Disable targeting "lj4" for CCRDHP option)
#    DEC 22/94 - F.Majaess (Add CCRDHP option for "HP4SI/MX;lj4")
#    NOV 08/93 - F.Majaess (Add P860 option)
#    FEB 24/93 - F.Majaess (enforce max. 133 characters/line)
#    FEB 10/93 - F.Majaess (ensure file(s) do not exist on XL60)
#    FEB 05/93 - F.Majaess (filter P810 output data by "lwf")
#    JAN 19/93 - F.Majaess (Increase IBM disk space allocation 
#                           from 40/10 to 80/20)
#    OCT 21/92 - F.Majaess (Do file(s) removal outside ftp)
#    FEB 20/92 - F.Majaess
 
#id  rprint  - Sends a file to a remote printer.
 
#    AUTHOR  - F.Majaess
 
#hd  PURPOSE - "rprint" script is used to dispose "fln" file from current
#hd            machine to a remote printer "un".
#hd            Note: a job is assembled and submitted to the mainframe
#hd                  controlling the output printer in the back-ground.
 
#pr  PARAMETERS:
#pr
#pr    POSITIONAL     
#pr
#pr      fln    = local filename to be printed on "un" printer.
#pr
#pr    PRIMARY/SECONDARY
#pr
#pr      rmv    = switch controlling deletion of input file "fln"(=no/yes).
#pr      wait   = switch signaling the process to wait (if necessary) for
#pr               transfered input data (=no/yes).
#pr
#pr    PRIMARY
#pr
#pr      ncopies= number of copies requested (=1)
#pr      Tcaller= holds caller scriptname of rprint (=RPRINT).
#pr      time   = back-end CPU time in seconds (=1)
#pr      mfl    = back-end MFL      in words   (=100000)
#pr      box    = box to dispose printout if desired (=$BOX).
#pr      un     = usernumber for printout (=$DESTIN). 
#pr      una    = user's account  (=$USER).
 
#ex  EXAMPLE: 
#ex
#ex    rprint fln=path/local_fln un=ccrn 
  
#   * code used to set switches and deal with parameter=value arguments 
#   * as well as setting parameters to their primary/secondary defaults.
#   * The list of other arguments (if any) is returned in "prmtrl" var...
 
. $SUBPROC/check_set_swtches_prmtrs

#  * Set variable 'AWK' to the version of "awk" to be used.

AWK=${AWK:='awk'}
 
#   * make sure arguments list variable "prmtrl" is initialized...
 
prmtrl=${prmtrl=}
  
#   * code used to set hard coded primary defaults...
 
eval "Tcaller=${Tcaller='RPRINT'}" 
eval "ncopies=${ncopies='1'}" 
eval "time=${time='1'}" 
eval "mfl=${mfl='100000'}"
 
#   * The following code deals with checking/setting positional 
#   * parameters ...
#   * Note: script will abort if too many arguments are specified.
 
if [ -n "$prmtrl" ] ; then
  i=1
  for prmtr in $prmtrl
  do
    case $i in
      1) fln=$prmtr;;
      *) eval "echo '$0 : too many parameters !' " ; exit ;;
    esac
    i=`expr $i + 1`
  done
fi
 
#   * Prompt for a positional parameter value(s) if none was/were
#   * specified...
 
while [ -z "$fln" ] ; do
  echo "please enter local filename  > \\c"
  read fln
done
 
Tun=`echo $un | tr '[a-z]' '[A-Z]'`
 
Tfln=`echo $fln | tr '[a-z]' '[A-Z]'`
 
 
wait=`echo $wait | tr '[A-Z]' '[a-z]'`

Uuna=`echo $una | tr '[a-z]' '[A-Z]'`
 
#   ****   Task of the script...   ****
# set -x
# set -v
 
#   * Wait for the file to arrive from ...
 
if [ "$wait" = "yes" ] ; then
ntimes=120

while [ ! -f "$fln" ]
  do
    ntimes=`expr $ntimes - 1`;
    if [ $ntimes -lt 1 ] ; then
      eval "echo $fln file does not exist !"
      exit 1
    fi
    sleep 15
  done
sleep 70
 
# Osize=`ls -l $fln | ($AWK '{print $5}')`
Osize=`ls -l $fln ` 
Nsize='-99X'
while [ "$Osize" != "$Nsize" ]
  do
    sleep 10
    Nsize=`ls -l $fln ` 
  done
 
fi

#   * check if a valid filename is specified, also setup needed parameters...
  
if [ -f $fln -a -s $fln -a -r $fln ] ; then
  :
else
  eval "echo $fln file is not valid !"
  exit 1
fi
 
#   * Routing via front-end...
 
#   * Make sure printer id is a valid one...
 
if [ "$SITE_ID" = 'Dorval' ] ; then
 case $Tun in
# CCRDHP|P860|P810|CCRN|CCRN4|CCRNO|ASPOAAY|DCC ) : ;;
  CCRDHP|P860|P810|LPL ) : ;;
  * ) eval "echo '$0 : illegal printer ID=$un ! ' ";
      exit 2 ;;
 esac
else
 case $Tun in
  LPL|P810 ) : ;;
  * ) eval "echo '$0 : illegal printer ID=$un ! ' ";
     echo "\n0 rprint : legal printer ID are LPL and P810 \n";
     echo "\n0 rprint : best is to set DESTIN in .zzzbvar \n";
     exit 2 ;;
 esac
fi
 
Tpid=$$
while [ $Tpid -lt 1000 ] ; do
  Tpid=`expr $Tpid \* 10 `  
done
while [ $Tpid -gt 9999 ] ; do
  Tpid=`echo $Tpid | sed 's/^.//'`
done
 
# Tmpfl="/users/tor/acrn/src/tmp/prntjbs"
# Tmpfl="/data/ccc/acrn/gen/prntjbs"
if [ "$SITE_ID" = 'Dorval' ] ; then
#if [ "$OS" = 'Linux' ] ; then
 #Tmpfl="/fs/ib_ccrn0/dib_data/prntjbs"
 #Tmpfl="/data/cava_ccrn0/dib_data/prntjbs"
 #Tmpfl="/data/cava_ccrn3/lnx_data/prntjbs"
  Tmpfl="`dirname $DATAPATH`/prntjbs"
 #Tmpfl=`dirname $DATAPATH` ; Tmpfl="${Tmpfl}/prntjbs"
 else
# Tmpfl="/data/ccrn/gen/prntjbs"
#fi
else
 Tmpfl="$CCRNSRC/tmp/prntjbs"
fi
Tmpfl="$Tmpfl""/RPRINT.$Tun.$Tcaller.$Tpid"
   
if [ "$Tcaller" = "LI" -a "$Tun" != "P810" -a "$Tun" != "P860" -a "$Tun" != "CCRDHP" -a "$Tun" != "LPL" ] ; then 
  eval "( cat $fln | sed '1,\$s/^\$/ /
  1s/.*/1&/
  2,\$s/.*/ &/
  2,\$s/^.*/1 /
  2,\$s/^ ~e\$/1/
  2,\$s/^ \*EOR\$/  * * &/
  2,\$s/^ \*EOF\$/  * * &/
  2,\$s/^ \EOF\$/1/'
   ) | $AWK '{ print substr(\$0,1,133)}' > $Tmpfl "
else
  if [ "$Tcaller" = "LSH" ] ; then 
    eval "copys $fln $Tmpfl /dev/null"
  else
   if [ "$Tun" = "P810" -o "$Tun" = "P860" -o "$Tun" = "CCRDHP" -o "$Tun" = "LPL" ] ; then 
   #eval "( cat $fln | sed '1,\$s/^.*/1 /' 
    eval "( cat $fln | sed '1,\$s/^W/1 W/
                            1,\$s/^Request/ Request/
                            1,\$s/^logout/ logout/
                            1,\$s/^.*^L/1 /
                            2,\$s/^*EOR/  * * &/
                            2,\$s/^\*EOR\$/  * * &/
                            2,\$s/^\*EOF\$/  * * &/
                            2,\$s/^\EOF\$/1/'
    ) | $AWK '{ print substr(\$0,1,140)}' > $Tmpfl "
   else
#   eval "\cat $fln > $Tmpfl"
    eval "( cat $fln | sed '1,\$s/^.*/1 /' 
    ) | $AWK '{ print substr(\$0,1,133)}' > $Tmpfl "
   fi
  fi
fi
 
chmod ug+rwx $Tmpfl
# chown acrnsrc $Tmpfl
 
if [ "$Tun" = "P810" -o "$Tun" = "P860" -o "$Tun" = "CCRDHP" -o "$Tun" = "LPL" ] ; then
# cat $Tmpfl | rsh $CCRN "lpr -Plw -#$ncopies"
 case $Tun in
# P810 ) cat $Tmpfl | rsh $CCRN "/apps/bin/lwf -s9 -l -m | lpr -Plw -#$ncopies" ;;
# P810 ) cat $Tmpfl | ssh $CCRN "/apps/bin/lwf -s9 -l -m | lpr -Plw -#$ncopies" ;;
  P810 ) cat $Tmpfl | ssh $CCRN "enscript -r -q -fCourier9 -dlp1 -n$ncopies" ;;
#  CCRDHP ) cat $Tmpfl | rsh $CCRN "/apps/bin/lwf -s9 -l -m | lpr -Plj4 -#$ncopies" ;;
#  CCRDHP ) cat $Tmpfl | rsh $CCRN "enscript -r -q -fCourier9 -dlj4 -n$ncopies" ;;
# CCRDHP  ) cat $Tmpfl | rsh $CCRN "enscript -r -q -fCourier9       -n$ncopies" ;;
  CCRDHP  ) cat $Tmpfl | ssh $CCRN "enscript -r -q -fCourier9       -n$ncopies" ;;
      LPL ) cat $Tmpfl | ssh $CCRN "enscript -r -q -fCourier9 -dlpl -n$ncopies" ;;
#  *    ) cat $Tmpfl | rsh $CCRN "enscript -r -q -fCourier9 -dlp0 -n$ncopies" ;;
#  *    ) cat $Tmpfl | rsh $CCRN "enscript -r -q -fCourier9 -dlp  -n$ncopies" ;;
   *    ) cat $Tmpfl | ssh $CCRN "enscript -r -q -fCourier9 -dlp  -n$ncopies" ;;
 esac
 \rm $Tmpfl
else
 echo "RPRINT: Sorry, can not proceed (Tun=$Tun)!"
 exit 1
 case $Tun in
  CCRN4            ) RMTP='U04'  ;;
  CCRNO|ASPOAAY|DCC) RMTP='N1R0' ;;
  *                ) RMTP='U08'  ;;
 esac
 if [ "$ncopies" = "1" ] ; then
 ((cat << woofhdr) ; cat $Tmpfl ; (cat << wooftrl)) > $Tmpfl.one
1 CCRN  RPRINT($Tcaller,$fln,$Tun,$ncopies) --------------- BOX=$box UN=$Uuna
woofhdr
1 CCRN  END OF OUTPUT FOR $fln --------------- BOX=$box UN=$Uuna
  PLEASE DETACH AFTER THIS PAGE AND PUT IN ABOVE BOX. THANKS.
wooftrl
chmod ug+rwx $Tmpfl.one 
# chown acrnsrc $Tmpfl.one 
\rm $Tmpfl
 if [ "$RMTP" = "N1RX" ] ; then
  npr $Tmpfl.one $box
  rm $Tmpfl.one 
  exit
 else
  (cat << woofmvsjcl) > $Tmpfl.mvsjcl
//ACRN$Tpid JOB (ACRNAF8),'$Uuna',USER=ACRNAF8,PASSWORD=BWXR,
// TIME=1,MSGCLASS=X
/*DELIVER BOX$box/$Uuna
/*JOBPARM L=150 
/*ROUTE PRINT $RMTP 
//COPYF     EXEC PGM=FILEAID
//DD01   DD DSN=ACRNAF8.A$Tpid.CHAR,DISP=(SHR,DELETE),
//          DCB=(RECFM=FBA,LRECL=133,BLKSIZE=23408) 
//DD01O  DD SYSOUT=A
//SYSIN  DD *
\$\$DD01    COPYALL
/*
//
woofmvsjcl
chmod ug+rwx $Tmpfl.mvsjcl
# chown acrnsrc $Tmpfl.mvsjcl
Zzzdmy="Zzdmyz.rprint.$Tpid"

  (cat << woofunixjcl) > $Tmpfl.unixjcl
#! /bin/sh
/usr/ucb/ftp -n -i << woof
open osdmf01
user acrnaf8 bwxr
ascii
del A$Tpid.CHAR
quote site unit(TEMPDA) space(80,20) TRACK 
quote site recfm(FB) lrecl(133) blksize(23408)
put $Tmpfl.one A$Tpid.CHAR
ls A$Tpid.CHAR $Zzzdmy
quote site submit
put $Tmpfl.mvsjcl
quit
woof
if [ -f "$Zzzdmy" ] ; then 
 if [ -s "$Zzzdmy" ] ; then 
   \rm $Tmpfl.one 
   \rm $Tmpfl.mvsjcl 
   \rm $Tmpfl.unixjcl
   \rm $Zzzdmy
   exit
 else
   #chown acrnsrc $Zzzdmy
   exit 1
 fi
fi
exit 2
woofunixjcl
chmod ug+rwx $Tmpfl.unixjcl
#chown acrnsrc $Tmpfl.unixjcl
fi
else

 (cat << woof) >> $Tmpfl
1    
woof
 
 (cat << woofhdr) > $Tmpfl.hdr
1 CCRN  RPRINT($Tcaller,$fln,$Tun,$ncopies) --------------- BOX=$box UN=$Uuna
woofhdr
 chmod ug+rwx $Tmpfl.hdr 
 #chown acrnsrc $Tmpfl.hdr
  
 (cat << wooftrl) > $Tmpfl.trl
1 CCRN  END OF OUTPUT FOR $fln --------------- BOX=$box UN=$Uuna
  PLEASE DETACH AFTER THIS PAGE AND PUT IN ABOVE BOX. THANKS.
wooftrl
chmod ug+rwx $Tmpfl.trl
#chown acrnsrc $Tmpfl.trl
 
  (cat << woofmvsjcl) > $Tmpfl.mvsjcl
//ACRN$Tpid JOB (ACRNAF8),'$Uuna',USER=ACRNAF8,PASSWORD=BWXR,
// TIME=1,MSGCLASS=X
/*DELIVER BOX$box/$Uuna
/*JOBPARM L=150 
/*ROUTE PRINT $RMTP 
//COPYF     EXEC PGM=FILEAID
//DD01   DD DSN=ACRNAF8.A$Tpid.CHAR.HDR,DISP=(SHR,DELETE),
//          DCB=(RECFM=FBA,LRECL=133,BLKSIZE=23408) 
woofmvsjcl
  Tncopies="$ncopies"
  while [ $Tncopies -gt 0 ] ; do
  (cat << woofmvsjcl) >> $Tmpfl.mvsjcl
//       DD DSN=ACRNAF8.A$Tpid.CHAR.BDY,DISP=(SHR,DELETE),
//          DCB=(RECFM=FBA,LRECL=133,BLKSIZE=23408) 
woofmvsjcl
  Tncopies=`expr $Tncopies - 1 `  
  done
  (cat << woofmvsjcl) >> $Tmpfl.mvsjcl
//       DD DSN=ACRNAF8.A$Tpid.CHAR.TRL,DISP=(SHR,DELETE),
//          DCB=(RECFM=FBA,LRECL=133,BLKSIZE=23408) 
//DD01O  DD SYSOUT=A
//SYSIN  DD *
\$\$DD01    COPYALL
/*
//
woofmvsjcl
chmod ug+rwx $Tmpfl.mvsjcl
#chown acrnsrc $Tmpfl.mvsjcl
Zzzdmy="Zzdmyz.rprint.$Tpid"

  (cat << woofunixjcl) > $Tmpfl.unixjcl
#! /bin/sh
/usr/ucb/ftp -n -i << woof
open osdmf01
user acrnaf8 bwxr
ascii
del A$Tpid.CHAR.HDR
quote site unit(TEMPDA) space(10,5) recfm(FB) lrecl(133) blksize(23408)
put $Tmpfl.hdr A$Tpid.CHAR.HDR
del A$Tpid.CHAR.BDY
quote site unit(TEMPDA) space(80,20) recfm(FB) lrecl(133) blksize(23408)
put $Tmpfl A$Tpid.CHAR.BDY
del A$Tpid.CHAR.TRL
quote site unit(TEMPDA) space(10,5) recfm(FB) lrecl(133) blksize(23408)
put $Tmpfl.trl A$Tpid.CHAR.TRL
ls A$Tpid.CHAR.BDY $Zzzdmy
quote site submit
put $Tmpfl.mvsjcl
quit
woof
if [ -f "$Zzzdmy" ] ; then 
 if [ -s "$Zzzdmy" ] ; then 
   \rm $Tmpfl.hdr 
   \rm $Tmpfl 
   \rm $Tmpfl.trl 
   \rm $Tmpfl.mvsjcl 
   \rm $Tmpfl.unixjcl
   \rm $Zzzdmy
   exit
 else
   #chown acrnsrc $Zzzdmy
   exit 1
 fi
fi
exit 2
woofunixjcl
 
chmod ug+rwx $Tmpfl.unixjcl
#chown acrnsrc $Tmpfl.unixjcl
 
fi 
 
# cat << woof
( at now + 01 minutes << woof ) > /dev/null 2>&1
($Tmpfl.unixjcl > /dev/null ||
(eval "echo problem in routing $fln ! " ;
 eval "echo using $Tmpfln.--- " ; exit 3 ))
woof
 eval "echo $fln data will be routed in the back-ground " 
 fi

if [ "$rmv" = "yes" ] ; then
  \rm $fln
  eval "echo $fln file removed as requested"
fi
