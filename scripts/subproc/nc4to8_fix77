#! /bin/sh
#  *            fix77
#  * ----------------------------------- FM,MB - Mar 18/94.
#  *                                     Massage Fortran source code to
#  *                                     get around the problem with 
#  *                                     '4H....' when running in 64-bit 
#  *                                     integer mode.

IFS=' '
export IFS
# set -x
if [ -f $1 ] ; then
 Tmpfil="Tmp_fix77""$$"
#float=${float:='_float2'}
 OS=${OS=`uname -s`}
 AWK=${AWK='awk'}
 if [ -n "$float" ] ; then
  F77HOLD="F77""${float}"
  eval "F77HOLD=\${${F77HOLD}}"
  F77HOLD=`echo "${F77HOLD}" | $AWK '{ print $1 ; }'`
 else
  F77HOLD=`echo ${F77} | $AWK '{ print $1 ; }'`
 fi
 if [ "$OS" = 'AIX' ] ; then
  F77CUT3=${F77CUT3:=`basename ${F77HOLD} | sed -e 's/^\(...\).*$/\1/g'`}
  F77CUT3=${F77CUT3:=`basename ${F77HOLD} | sed -e 's/^.*\(f[7-9][0-9]\).*$/\1/g'`}
 elif [ "$OS" = 'Linux' -a "$SITE_ID" = 'DrvlSC' ] ; then
  F77CUT3=${F77CUT3:=`basename ${F77HOLD} | sed -e 's/^\(...\).*$/\1/g'`}
  F77CUT3=${F77CUT3:=`basename ${F77HOLD} | sed -e 's/^.*\(f[7-9][0-9]\).*$/\1/g'`}
 else
  F77CUT3=${F77CUT3:=`basename ${F77HOLD} | sed -e 's/^.*\(pgf[7-9][0-9]\).*$/\1/g'`}
  F77CUT3=${F77CUT3:=`basename ${F77HOLD} | sed -e 's/^.*\(f[7-9][0-9]\).*$/\1/g'`}
  F77CUT3=${F77CUT3:=`basename ${F77HOLD} | sed -e 's/^\(...\).*$/\1/g'`}
 fi
 if [ "$OS" != 'AIX' -a "$HOSTHW" != 'SX-6' ] ; then
  DiffSwtch='-q'
 else
  unset DiffSwtch
 fi

 if [ "$float" = '_float2' -o "$float" = '_openmp' -o "$float" = '_multi' -o "$xf90" = 'yes' ] ; then

  # ***** "float2" mode

  if [ \( "$HOSTHW" = 'SX-6' -a "$F77CUT3" = 'f90' \) -o "$xf90" = 'yes' -o \
       \( "$OS" = 'AIX'      -a "$F77CUT3" = 'xlf' \)  -o "$F77CUT3" = 'ftn'  ] ; then
  # "f90" on SX6, or "xlf" on IBM ...
  $AWK '{
     if ( substr($0,1,1) ~ /[Cc]/ ) {
      print $0
      }
     else {
      print substr($0,1,72)
      }
     }' $1 | sed '/^[Cc]/b
     /^      DATA/b
     /^ [Ff][Uu][Nn][Cc][Tt][Ii][Oo][Nn] *[Ll][Nn][Bb][Ll][Nn][Kk]/b
     /^ [Ff][Uu][Nn][Cc][Tt][Ii][Oo][Nn] *[Cc][Vv][Mm][Gg][TtPp]/b
     /\/ *4H/b
     /[^ ,.=\/]4H/b
     /AIMAG\( *(\)/s// IMAG\1/g
     /VR IMAG/s//VRAIMAG/g

     /4H..../{ 
      s/4H\(....\)/NC4TO8("\1")/g
      s/ *$//g
      /^.\{73,\}/s/^\(.\{72\}\)\(.*\)/\1\
     +\2/
               }
     /[Aa][Cc][Cc][Ee][Ss][Ss]=.[Aa][Pp][Pp][Ee][Nn][Dd]./{
      s/[Aa][Cc][Cc][Ee][Ss][Ss]/POSITION/g
      s/ *$//g
      /^.\{73,\}/s/^\(.\{72\}\)\(.*\)/\1\
     +\2/
               }
     /[Cc][Vv][Mm][Gg][Tt]/{
      s/[Cc][Vv][Mm][Gg][Tt] *(/MERGE(/g
               }
     /[Ll][Nn][Bb][Ll][Nn][Kk]/{
      s/[Ll][Nn][Bb][Ll][Nn][Kk]/LEN_TRIM/g
      /^.\{73,\}/s/^\(.\{72\}\)\(.*\)/\1\
     +\2/
               }
    ' | sed -e 's/^[Cc]/\!/' -e 's/; *$//' > $Tmpfil 
  diff $DiffSwtch $1 $Tmpfil > /dev/null && \rm -f $Tmpfil || \cp $Tmpfil $1
  elif [ \( "$OS" = 'IRIX64' -a \( "$F77CUT3" = 'f90' -o "$F77CUT3" = 'f95' \) \) -o \
         \( "$OS" = 'Linux'  -a \( "$F77CUT3" = 'ifc' -o "$F77CUT3" = 'ifort' -o "$F77CUT3" = 'pgf90' \) \) ] ; then
  # "f90"/"f95" on Irix, "ifc"/"ifort" Intel, or "pgf90" PGI compiler on Linux ... 
  $AWK '{
     if ( substr($0,1,1) ~ /[Cc]/ ) {
      print $0
      }
     else {
      print substr($0,1,72)
      }
     }' $1 | sed '/^[Cc]/b
     /^      DATA/b
     /^ [Ff][Uu][Nn][Cc][Tt][Ii][Oo][Nn] *[Ll][Nn][Bb][Ll][Nn][Kk]/b
     /^ [Ff][Uu][Nn][Cc][Tt][Ii][Oo][Nn] *[Cc][Vv][Mm][Gg][TtPp]/b
     /\/ *4H/b
     /[^ ,.=\/]4H/b
     /AIMAG\( *(\)/s// IMAG\1/g
     /VR IMAG/s//VRAIMAG/g
     /^ *[Cc][Oo][Mm][Pp][Ll][Ee][Xx]\*16/!s/^\( *\)[Cc][Oo][Mm][Pp][Ll][Ee][Xx] */\1COMPLEX*16 /
     s/[Dd][Ff][Ll][Oo][Aa][Tt]\( *(\)/FLOAT\1/g
     /[Ff][Ll][Oo][Aa][Tt]/{
      s/[Ff][Ll][Oo][Aa][Tt] *(/DFLOAT(/g
      s/ *$//g
      /^.\{73,\}/s/^\(.\{72\}\)\(.*\)/\1\
     +\2/
               }

     /4H..../{ 
      s/4H\(....\)/NC4TO8("\1")/g
      s/ *$//g
      /^.\{73,\}/s/^\(.\{72\}\)\(.*\)/\1\
     +\2/
               }
     /[Aa][Cc][Cc][Ee][Ss][Ss]=.[Aa][Pp][Pp][Ee][Nn][Dd]./{
      s/[Aa][Cc][Cc][Ee][Ss][Ss]/POSITION/g
      s/ *$//g
      /^.\{73,\}/s/^\(.\{72\}\)\(.*\)/\1\
     +\2/
               }
     /[Cc][Vv][Mm][Gg][Tt]/{
      s/[Cc][Vv][Mm][Gg][Tt] *(/MERGE(/g
               }
     /[Ll][Nn][Bb][Ll][Nn][Kk]/{
      s/[Ll][Nn][Bb][Ll][Nn][Kk]/LEN_TRIM/g
      /^.\{73,\}/s/^\(.\{72\}\)\(.*\)/\1\
     +\2/
               }
    ' | sed -e 's/^[Cc]/\!/' -e 's/; *$//' > $Tmpfil 
  diff $DiffSwtch $1 $Tmpfil > /dev/null && \rm -f $Tmpfil || \cp $Tmpfil $1
  elif [ \( "$OS" = 'IRIX64' -a "$F77CUT3" = 'f77' \) ] ; then
  # "f77" on Irix, ... 
  $AWK '{
     if ( substr($0,1,1) ~ /[Cc]/ ) {
      print $0
      }
     else {
      print substr($0,1,72)
      }
     }' $1 | sed '/^[Cc]/b
     /^      DATA/b
     /^ [Ff][Uu][Nn][Cc][Tt][Ii][Oo][Nn] *[Ll][Nn][Bb][Ll][Nn][Kk]/b
     /^ [Ff][Uu][Nn][Cc][Tt][Ii][Oo][Nn] *[Cc][Vv][Mm][Gg][TtPp]/b
     /\/ *4H/b
     /[^ ,.=\/]4H/b
     /AIMAG\( *(\)/s// IMAG\1/g
     /VR IMAG/s//VRAIMAG/g
     /^ *[Cc][Oo][Mm][Pp][Ll][Ee][Xx]\*16/!s/^\( *\)[Cc][Oo][Mm][Pp][Ll][Ee][Xx] */\1COMPLEX*16 /
     s/[Dd][Ff][Ll][Oo][Aa][Tt]\( *(\)/FLOAT\1/g
     /[Ff][Ll][Oo][Aa][Tt]/{
      s/[Ff][Ll][Oo][Aa][Tt] *(/DFLOAT(/g
      s/ *$//g
      /^.\{73,\}/s/^\(.\{72\}\)\(.*\)/\1\
     +\2/
               }

     /4H..../{ 
      s/4H\(....\)/NC4TO8("\1")/g
      s/ *$//g
      /^.\{73,\}/s/^\(.\{72\}\)\(.*\)/\1\
     +\2/
               }
     /[Pp][Oo][Ss][Ii][Tt][Ii][Oo][Nn]=.[Aa][Pp][Pp][Ee][Nn][Dd]./{
      s/[Pp][Oo][Ss][Ii][Tt][Ii][Oo][Nn]/ACCESS/g
      s/ *$//g
      /^.\{73,\}/s/^\(.\{72\}\)\(.*\)/\1\
     +\2/
               }
     /[Mm][Ee][Rr][Gg][Ee]/{
      s/[Mm][Ee][Rr][Gg][Ee] *(/CVMGT(/g
               }
     /[Ll][Ee][Nn][_][Tt][Rr][Ii][Mm]/{
      s/[Ll][Ee][Nn][_][Tt][Rr][Ii][Mm]/LNBLNK/g
               }
    ' > $Tmpfil 
  diff $DiffSwtch $1 $Tmpfil > /dev/null && \rm -f $Tmpfil || \cp $Tmpfil $1
  elif [ "$OS" = 'Linux' -a \( "$F77CUT3" = 'f90' -o "$F77CUT3" = 'f95' \) ] ; then
  # Linux Absoft compiler
  $AWK '{
     if ( substr($0,1,1) ~ /[Cc]/ ) {
      print $0
      }
     else {
      print substr($0,1,72)
      }
     }' $1 | sed '/^[Cc]/b
     /^      DATA/b
     /^ [Ff][Uu][Nn][Cc][Tt][Ii][Oo][Nn] *[Ll][Nn][Bb][Ll][Nn][Kk]/b
     /^ [Ff][Uu][Nn][Cc][Tt][Ii][Oo][Nn] *[Cc][Vv][Mm][Gg][TtPp]/b
     /\/ *4H/b
     /[^ ,.=\/]4H/b
     /AIMAG\( *(\)/s// IMAG\1/g
     /VR IMAG/s//VRAIMAG/g
     /^ *[Cc][Oo][Mm][Pp][Ll][Ee][Xx]\*16/!s/^\( *\)[Cc][Oo][Mm][Pp][Ll][Ee][Xx] */\1COMPLEX*16 /
     s/[Dd][Ff][Ll][Oo][Aa][Tt]\( *(\)/FLOAT\1/g
     /[Ff][Ll][Oo][Aa][Tt]/{
      s/[Ff][Ll][Oo][Aa][Tt] *(/DFLOAT(/g
      s/ *$//g
      /^.\{73,\}/s/^\(.\{72\}\)\(.*\)/\1\
     +\2/
               }

     /4H..../{ 
      s/4H\(....\)/NC4TO8("\1")/g
      s/ *$//g
      /^.\{73,\}/s/^\(.\{72\}\)\(.*\)/\1\
     +\2/
               }
      /OPEN(UNIT,FILE=FILNAME,FORM=FORMAT,ACCESS='"'"'APPEND'"'"',IOSTAT=IOS)/{
       s/OPEN(UNIT,FILE=FILNAME,FORM=FORMAT,ACCESS='"'"'APPEND'"'"',IOSTAT=IOS)/OPEN(UNIT,FILE=FILNAME,CONVERT='"'"'BIG_ENDIAN'"'"',FORM=FORMAT,POSITION='"'"'APPEND'"'"',IOSTAT=IOS)/g
       s/ *$//g
       /^.\{73,\}/s/^\(.\{72\}\)\(.*\)/\1\
     +\2/
               }
      /OPEN(UNIT,FILE=FILNAME,FORM=FORMAT,POSITION='"'"'APPEND'"'"',IOSTAT=IOS)/{
       s/OPEN(UNIT,FILE=FILNAME,FORM=FORMAT,POSITION='"'"'APPEND'"'"',IOSTAT=IOS)/OPEN(UNIT,FILE=FILNAME,CONVERT='"'"'BIG_ENDIAN'"'"',FORM=FORMAT,POSITION='"'"'APPEND'"'"',IOSTAT=IOS)/g
       s/ *$//g
       /^.\{73,\}/s/^\(.\{72\}\)\(.*\)/\1\
     +\2/
               }
     /[Aa][Cc][Cc][Ee][Ss][Ss]=.[Aa][Pp][Pp][Ee][Nn][Dd]./{
      s/[Aa][Cc][Cc][Ee][Ss][Ss]/POSITION/g
      s/ *$//g
      /^.\{73,\}/s/^\(.\{72\}\)\(.*\)/\1\
     +\2/
               }
     /FORM='"'"'UNFORMATTED'"'"'/{
      s/\(FORM='"'"'UNFORMATTED'"'"'\)/CONVERT='"'"'BIG_ENDIAN'"'"',\1/g
      s/ *$//g
      /^.\{73,\}/s/^\(.\{72\}\)\(.*\)/\1\
     +\2/
               }
     /[Cc][Vv][Mm][Gg][Tt]/{
      s/[Cc][Vv][Mm][Gg][Tt] *(/MERGE(/g
               }
     /[Ll][Nn][Bb][Ll][Nn][Kk]/{
      s/[Ll][Nn][Bb][Ll][Nn][Kk]/LEN_TRIM/g
      /^.\{73,\}/s/^\(.\{72\}\)\(.*\)/\1\
     +\2/
               }
    ' > $Tmpfil 
  diff $DiffSwtch $1 $Tmpfil > /dev/null && \rm -f $Tmpfil || \cp $Tmpfil $1
  elif [ "$OS" = 'Linux' -a "$F77CUT3" = 'g77' ] ; then
  # "g77" on Linux case ...
   echo "float2 switch not allowed with g77"
   exit 77
  fi
 elif [ "$float" = '_float1' -o "$float" = '_float1_openmp' ] ; then

  # ***** "float1" mode

  if [ \( "$HOSTHW" = 'SX-6' -a "$F77CUT3" = 'f90' \) -o "$xf90" = 'yes' -o \
       \( "$OS" = 'AIX'      -a "$F77CUT3" = 'xlf' \) -o "$F77CUT3" = 'ftn'    ] ; then
  # "f90" on SX6, or "xlf" on IBM ...
  $AWK '{
     if ( substr($0,1,1) ~ /[Cc]/ ) {
      print $0
      }
     else {
      print substr($0,1,72)
      }
     }' $1 | sed '/^[Cc]/b
     /^      DATA/b
     /^ [Ff][Uu][Nn][Cc][Tt][Ii][Oo][Nn] *[Ll][Nn][Bb][Ll][Nn][Kk]/b
     /^ [Ff][Uu][Nn][Cc][Tt][Ii][Oo][Nn] *[Cc][Vv][Mm][Gg][TtPp]/b
     /\/ *4H/b
     /[^ ,.=\/]4H/b
     /AIMAG\( *(\)/s// IMAG\1/g
     /VR IMAG/s//VRAIMAG/g

     /4H..../{ 
      s/4H\(....\)/NC4TO8("\1")/g
      s/ *$//g
      /^.\{73,\}/s/^\(.\{72\}\)\(.*\)/\1\
     +\2/
               }
     /[Aa][Cc][Cc][Ee][Ss][Ss]=.[Aa][Pp][Pp][Ee][Nn][Dd]./{
      s/[Aa][Cc][Cc][Ee][Ss][Ss]/POSITION/g
      s/ *$//g
      /^.\{73,\}/s/^\(.\{72\}\)\(.*\)/\1\
     +\2/
               }
     /[Cc][Vv][Mm][Gg][Tt]/{
      s/[Cc][Vv][Mm][Gg][Tt] *(/MERGE(/g
               }
     /[Ll][Nn][Bb][Ll][Nn][Kk]/{
      s/[Ll][Nn][Bb][Ll][Nn][Kk]/LEN_TRIM/g
      /^.\{73,\}/s/^\(.\{72\}\)\(.*\)/\1\
     +\2/
               }
    ' | sed -e 's/^[Cc]/\!/' -e 's/; *$//' > $Tmpfil 
  diff $DiffSwtch $1 $Tmpfil > /dev/null && \rm -f $Tmpfil || \cp $Tmpfil $1
  elif [ \( "$OS" = 'IRIX64' -a \( "$F77CUT3" = 'f90' -o "$F77CUT3" = 'f95' \) \) -o \
         \( "$OS" = 'Linux'  -a \( "$F77CUT3" = 'ifc' -o "$F77CUT3" = 'ifort' -o "$F77CUT3" = 'pgf90' \) \) ] ; then
  # "f90"/"f95" on Irix, "ifc"/"ifort" Intel, or "pgf90" PGI compiler on Linux ... 
  $AWK '{
     if ( substr($0,1,1) ~ /[Cc]/ ) {
      print $0
      }
     else {
      print substr($0,1,72)
      }
     }' $1 | sed '/^[Cc]/b
     /^      DATA/b
     /^ [Ff][Uu][Nn][Cc][Tt][Ii][Oo][Nn] *[Ll][Nn][Bb][Ll][Nn][Kk]/b
     /^ [Ff][Uu][Nn][Cc][Tt][Ii][Oo][Nn] *[Cc][Vv][Mm][Gg][TtPp]/b
     /\/ *4H/b
     /[^ ,.=\/]4H/b
     /AIMAG\( *(\)/s// IMAG\1/g
     /VR IMAG/s//VRAIMAG/g

     /4H..../{ 
      s/4H\(....\)/NC4TO8("\1")/g
      s/ *$//g
      /^.\{73,\}/s/^\(.\{72\}\)\(.*\)/\1\
     +\2/
               }
     /[Aa][Cc][Cc][Ee][Ss][Ss]=.[Aa][Pp][Pp][Ee][Nn][Dd]./{
      s/[Aa][Cc][Cc][Ee][Ss][Ss]/POSITION/g
      s/ *$//g
      /^.\{73,\}/s/^\(.\{72\}\)\(.*\)/\1\
     +\2/
               }
     /[Cc][Vv][Mm][Gg][Tt]/{
      s/[Cc][Vv][Mm][Gg][Tt] *(/MERGE(/g
               }
     /[Ll][Nn][Bb][Ll][Nn][Kk]/{
      s/[Ll][Nn][Bb][Ll][Nn][Kk]/LEN_TRIM/g
      /^.\{73,\}/s/^\(.\{72\}\)\(.*\)/\1\
     +\2/
               }
    ' | sed -e 's/^[Cc]/\!/' -e 's/; *$//' > $Tmpfil 
  diff $DiffSwtch $1 $Tmpfil > /dev/null && \rm -f $Tmpfil || \cp $Tmpfil $1
  elif [ \( "$OS" = 'IRIX64' -a "$F77CUT3" = 'f77' \) ] ; then
  # "f77" on Irix ...
  $AWK '{
     if ( substr($0,1,1) ~ /[Cc]/ ) {
      print $0
      }
     else {
      print substr($0,1,72)
      }
     }' $1 | sed '/^[Cc]/b
     /^      DATA/b
     /^ [Ff][Uu][Nn][Cc][Tt][Ii][Oo][Nn] *[Ll][Nn][Bb][Ll][Nn][Kk]/b
     /^ [Ff][Uu][Nn][Cc][Tt][Ii][Oo][Nn] *[Cc][Vv][Mm][Gg][TtPp]/b
     /\/ *4H/b
     /[^ ,.=\/]4H/b
     /AIMAG\( *(\)/s// IMAG\1/g
     /VR IMAG/s//VRAIMAG/g

     /4H..../{ 
      s/4H\(....\)/NC4TO8("\1")/g
      s/ *$//g
      /^.\{73,\}/s/^\(.\{72\}\)\(.*\)/\1\
     +\2/
               }
     /[Pp][Oo][Ss][Ii][Tt][Ii][Oo][Nn]=.[Aa][Pp][Pp][Ee][Nn][Dd]./{
      s/[Pp][Oo][Ss][Ii][Tt][Ii][Oo][Nn]/ACCESS/g
      s/ *$//g
      /^.\{73,\}/s/^\(.\{72\}\)\(.*\)/\1\
     +\2/
               }
     /[Mm][Ee][Rr][Gg][Ee]/{
      s/[Mm][Ee][Rr][Gg][Ee] *(/CVMGT(/g
               }
     /[Ll][Ee][Nn][_][Tt][Rr][Ii][Mm]/{
      s/[Ll][Ee][Nn][_][Tt][Rr][Ii][Mm]/LNBLNK/g
               }
    ' > $Tmpfil 
  diff $DiffSwtch $1 $Tmpfil > /dev/null && \rm -f $Tmpfil || \cp $Tmpfil $1
  elif [ "$OS" = 'Linux' -a \( "$F77CUT3" = 'f90' -o "$F77CUT3" = 'f95' \) ] ; then
  # Linux Absoft compiler
  $AWK '{
     if ( substr($0,1,1) ~ /[Cc]/ ) {
      print $0
      }
     else {
      print substr($0,1,72)
      }
     }' $1 | sed '/^[Cc]/b
     /^      DATA/b
     /^ [Ff][Uu][Nn][Cc][Tt][Ii][Oo][Nn] *[Ll][Nn][Bb][Ll][Nn][Kk]/b
     /^ [Ff][Uu][Nn][Cc][Tt][Ii][Oo][Nn] *[Cc][Vv][Mm][Gg][TtPp]/b
     /\/ *4H/b
     /[^ ,.=\/]4H/b
     /AIMAG\( *(\)/s// IMAG\1/g
     /VR IMAG/s//VRAIMAG/g

     /4H..../{ 
      s/4H\(....\)/NC4TO8("\1")/g
      s/ *$//g
      /^.\{73,\}/s/^\(.\{72\}\)\(.*\)/\1\
     +\2/
               }
      /OPEN(UNIT,FILE=FILNAME,FORM=FORMAT,ACCESS='"'"'APPEND'"'"',IOSTAT=IOS)/{
       s/OPEN(UNIT,FILE=FILNAME,FORM=FORMAT,ACCESS='"'"'APPEND'"'"',IOSTAT=IOS)/OPEN(UNIT,FILE=FILNAME,CONVERT='"'"'BIG_ENDIAN'"'"',FORM=FORMAT,POSITION='"'"'APPEND'"'"',IOSTAT=IOS)/g
       s/ *$//g
       /^.\{73,\}/s/^\(.\{72\}\)\(.*\)/\1\
     +\2/
               }
      /OPEN(UNIT,FILE=FILNAME,FORM=FORMAT,POSITION='"'"'APPEND'"'"',IOSTAT=IOS)/{
       s/OPEN(UNIT,FILE=FILNAME,FORM=FORMAT,POSITION='"'"'APPEND'"'"',IOSTAT=IOS)/OPEN(UNIT,FILE=FILNAME,CONVERT='"'"'BIG_ENDIAN'"'"',FORM=FORMAT,POSITION='"'"'APPEND'"'"',IOSTAT=IOS)/g
       s/ *$//g
       /^.\{73,\}/s/^\(.\{72\}\)\(.*\)/\1\
     +\2/
               }
     /[Aa][Cc][Cc][Ee][Ss][Ss]=.[Aa][Pp][Pp][Ee][Nn][Dd]./{
      s/[Aa][Cc][Cc][Ee][Ss][Ss]/POSITION/g
      s/ *$//g
      /^.\{73,\}/s/^\(.\{72\}\)\(.*\)/\1\
     +\2/
               }
     /FORM='"'"'UNFORMATTED'"'"'/{
      s/\(FORM='"'"'UNFORMATTED'"'"'\)/CONVERT='"'"'BIG_ENDIAN'"'"',\1/g
      s/ *$//g
      /^.\{73,\}/s/^\(.\{72\}\)\(.*\)/\1\
     +\2/
               }
     /[Cc][Vv][Mm][Gg][Tt]/{
      s/[Cc][Vv][Mm][Gg][Tt] *(/MERGE(/g
               }
     /[Ll][Nn][Bb][Ll][Nn][Kk]/{
      s/[Ll][Nn][Bb][Ll][Nn][Kk]/LEN_TRIM/g
      /^.\{73,\}/s/^\(.\{72\}\)\(.*\)/\1\
     +\2/
               }
    ' > $Tmpfil 
  diff $DiffSwtch $1 $Tmpfil > /dev/null && \rm -f $Tmpfil || \cp $Tmpfil $1
  elif [ "$OS" = 'Linux' -a "$F77CUT3" = 'g77' ] ; then
  # "g77" on Linux case ...
  $AWK '{
     if ( substr($0,1,1) ~ /[Cc]/ ) {
      print $0
      }
     else {
      print substr($0,1,72)
      }
     }' $1 | sed '/^[Cc]/b
     /[Bb][Aa][Cc][Kk][Ss][Pp][Aa][Cc][Ee]/{
      s/[Bb][Aa][Cc][Kk][Ss][Pp][Aa][Cc][Ee]\(.*\)$/CALL G77BACK(\1)/
      s/ *$//g
      /^.\{73,\}/s/^\(.\{72\}\)\(.*\)/\1\
     +\2/
     }
     /^      DATA/b
     /^ [Ff][Uu][Nn][Cc][Tt][Ii][Oo][Nn] *[Ll][Nn][Bb][Ll][Nn][Kk]/b
     /^ [Ff][Uu][Nn][Cc][Tt][Ii][Oo][Nn] *[Cc][Vv][Mm][Gg][TtPp]/b
     /\/ *4H/b
     /[^ ,.=\/]4H/b
     /AIMAG\( *(\)/s// IMAG\1/g
     /VR IMAG/s//VRAIMAG/g

     /4H..../{ 
      s/4H\(....\)/NC4TO8("\1")/g
      s/ *$//g
      /^.\{73,\}/s/^\(.\{72\}\)\(.*\)/\1\
     +\2/
               }
     /[Pp][Oo][Ss][Ii][Tt][Ii][Oo][Nn]=.[Aa][Pp][Pp][Ee][Nn][Dd]./{
      s/[Pp][Oo][Ss][Ii][Tt][Ii][Oo][Nn]/ACCESS/g
      s/ *$//g
      /^.\{73,\}/s/^\(.\{72\}\)\(.*\)/\1\
     +\2/
               }
     /[Cc][Vv][Mm][Gg][Tt]/{
      s/[Cc][Vv][Mm][Gg][Tt] *(/MERGE(/g
               }
     /[Ll][Nn][Bb][Ll][Nn][Kk]/{
      s/[Ll][Nn][Bb][Ll][Nn][Kk]/LEN_TRIM/g
      /^.\{73,\}/s/^\(.\{72\}\)\(.*\)/\1\
     +\2/
               }
    ' > $Tmpfil 
  diff $DiffSwtch $1 $Tmpfil > /dev/null && \rm -f $Tmpfil || \cp $Tmpfil $1
  fi
 else
  # ***** Default float mode
  if [ \( "$HOSTHW" = 'SX-6' -a "$F77CUT3" = 'f90' \) -o "$xf90" = 'yes' -o \
       \( "$OS" = 'AIX'      -a "$F77CUT3" = 'xlf' \)  -o "$F77CUT3" = 'ftn'  ] ; then
  # "f90" on SX6, or "xlf" on IBM ...
  $AWK '{
     if ( substr($0,1,1) ~ /[Cc]/ ) {
      print $0
      }
     else {
      print substr($0,1,72)
      }
     }' $1 | sed '/^[Cc]/b
     /^      DATA/b
     /^ [Ff][Uu][Nn][Cc][Tt][Ii][Oo][Nn] *[Ll][Nn][Bb][Ll][Nn][Kk]/b
     /^ [Ff][Uu][Nn][Cc][Tt][Ii][Oo][Nn] *[Cc][Vv][Mm][Gg][TtPp]/b
     /\/ *4H/b
     /[^ ,.=\/]4H/b
     /AIMAG\( *(\)/s// IMAG\1/g
     /VR IMAG/s//VRAIMAG/g

     /4H..../{ 
      s/4H\(....\)/NC4TO8("\1")/g
      s/ *$//g
      /^.\{73,\}/s/^\(.\{72\}\)\(.*\)/\1\
     +\2/
               }
     /[Aa][Cc][Cc][Ee][Ss][Ss]=.[Aa][Pp][Pp][Ee][Nn][Dd]./{
      s/[Aa][Cc][Cc][Ee][Ss][Ss]/POSITION/g
      s/ *$//g
      /^.\{73,\}/s/^\(.\{72\}\)\(.*\)/\1\
     +\2/
               }
     /[Cc][Vv][Mm][Gg][Tt]/{
      s/[Cc][Vv][Mm][Gg][Tt] *(/MERGE(/g
               }
     /[Ll][Nn][Bb][Ll][Nn][Kk]/{
      s/[Ll][Nn][Bb][Ll][Nn][Kk]/LEN_TRIM/g
      /^.\{73,\}/s/^\(.\{72\}\)\(.*\)/\1\
     +\2/
               }
    ' | sed -e 's/^[Cc]/\!/' -e 's/; *$//' > $Tmpfil 
  diff $DiffSwtch $1 $Tmpfil > /dev/null && \rm -f $Tmpfil || \cp $Tmpfil $1
  elif [ \( "$OS" = 'IRIX64' -a \( "$F77CUT3" = 'f90' -o "$F77CUT3" = 'f95' \) \) -o \
         \( "$OS" = 'Linux'  -a \( "$F77CUT3" = 'ifc' -o "$F77CUT3" = 'ifort' -o "$F77CUT3" = 'pgf90' \) \) ] ; then
  # "f90"/"f95" on Irix, "ifc"/"ifort" Intel, or "pgf90" PGI compiler on Linux ... 
  $AWK '{
     if ( substr($0,1,1) ~ /[Cc]/ ) {
      print $0
      }
     else {
      print substr($0,1,72)
      }
     }' $1 | sed '/^[Cc]/b
     /^      DATA/b
     /^ [Ff][Uu][Nn][Cc][Tt][Ii][Oo][Nn] *[Ll][Nn][Bb][Ll][Nn][Kk]/b
     /^ [Ff][Uu][Nn][Cc][Tt][Ii][Oo][Nn] *[Cc][Vv][Mm][Gg][TtPp]/b
     /\/ *4H/b
     /[^ ,.=\/]4H/b
     /AIMAG\( *(\)/s// IMAG\1/g
     /VR IMAG/s//VRAIMAG/g
     /^ *[Cc][Oo][Mm][Pp][Ll][Ee][Xx]\*16/!s/^\( *\)[Cc][Oo][Mm][Pp][Ll][Ee][Xx] */\1COMPLEX*16 /
     s/[Dd][Ff][Ll][Oo][Aa][Tt]\( *(\)/FLOAT\1/g
     /[Ff][Ll][Oo][Aa][Tt]/{
      s/[Ff][Ll][Oo][Aa][Tt] *(/DFLOAT(/g
      s/ *$//g
      /^.\{73,\}/s/^\(.\{72\}\)\(.*\)/\1\
     +\2/
               }

     /4H..../{ 
      s/4H\(....\)/NC4TO8("\1")/g
      s/ *$//g
      /^.\{73,\}/s/^\(.\{72\}\)\(.*\)/\1\
     +\2/
               }
     /[Aa][Cc][Cc][Ee][Ss][Ss]=.[Aa][Pp][Pp][Ee][Nn][Dd]./{
      s/[Aa][Cc][Cc][Ee][Ss][Ss]/POSITION/g
      s/ *$//g
      /^.\{73,\}/s/^\(.\{72\}\)\(.*\)/\1\
     +\2/
               }
     /[Cc][Vv][Mm][Gg][Tt]/{
      s/[Cc][Vv][Mm][Gg][Tt] *(/MERGE(/g
               }
     /[Ll][Nn][Bb][Ll][Nn][Kk]/{
      s/[Ll][Nn][Bb][Ll][Nn][Kk]/LEN_TRIM/g
      /^.\{73,\}/s/^\(.\{72\}\)\(.*\)/\1\
     +\2/
               }
    ' | sed -e 's/^[Cc]/\!/' -e 's/; *$//' > $Tmpfil 
  diff $DiffSwtch $1 $Tmpfil > /dev/null && \rm -f $Tmpfil || \cp $Tmpfil $1
  elif [ \( "$OS" = 'IRIX64' -a "$F77CUT3" = 'f77' \) ] ; then
  # "f77" on Irix ...
  $AWK '{
     if ( substr($0,1,1) ~ /[Cc]/ ) {
      print $0
      }
     else {
      print substr($0,1,72)
      }
     }' $1 | sed '/^[Cc]/b
     /^      DATA/b
     /^ [Ff][Uu][Nn][Cc][Tt][Ii][Oo][Nn] *[Ll][Nn][Bb][Ll][Nn][Kk]/b
     /^ [Ff][Uu][Nn][Cc][Tt][Ii][Oo][Nn] *[Cc][Vv][Mm][Gg][TtPp]/b
     /\/ *4H/b
     /[^ ,.=\/]4H/b
     /AIMAG\( *(\)/s// IMAG\1/g
     /VR IMAG/s//VRAIMAG/g
     /^ *[Cc][Oo][Mm][Pp][Ll][Ee][Xx]\*16/!s/^\( *\)[Cc][Oo][Mm][Pp][Ll][Ee][Xx] */\1COMPLEX*16 /
     s/[Dd][Ff][Ll][Oo][Aa][Tt]\( *(\)/FLOAT\1/g
     /[Ff][Ll][Oo][Aa][Tt]/{
      s/[Ff][Ll][Oo][Aa][Tt] *(/DFLOAT(/g
      s/ *$//g
      /^.\{73,\}/s/^\(.\{72\}\)\(.*\)/\1\
     +\2/
               }

     /4H..../{ 
      s/4H\(....\)/NC4TO8("\1")/g
      s/ *$//g
      /^.\{73,\}/s/^\(.\{72\}\)\(.*\)/\1\
     +\2/
               }
     /[Pp][Oo][Ss][Ii][Tt][Ii][Oo][Nn]=.[Aa][Pp][Pp][Ee][Nn][Dd]./{
      s/[Pp][Oo][Ss][Ii][Tt][Ii][Oo][Nn]/ACCESS/g
      s/ *$//g
      /^.\{73,\}/s/^\(.\{72\}\)\(.*\)/\1\
     +\2/
               }
     /[Mm][Ee][Rr][Gg][Ee]/{
      s/[Mm][Ee][Rr][Gg][Ee] *(/CVMGT(/g
               }
     /[Ll][Ee][Nn][_][Tt][Rr][Ii][Mm]/{
      s/[Ll][Ee][Nn][_][Tt][Rr][Ii][Mm]/LNBLNK/g
               }
    ' > $Tmpfil 
  diff $DiffSwtch $1 $Tmpfil > /dev/null && \rm -f $Tmpfil || \cp $Tmpfil $1
  elif [ "$OS" = 'Linux' -a \( "$F77CUT3" = 'f90' -o "$F77CUT3" = 'f95' \) ] ; then
  # Linux Absoft compiler
  $AWK '{
     if ( substr($0,1,1) ~ /[Cc]/ ) {
      print $0
      }
     else {
      print substr($0,1,72)
      }
     }' $1 | sed '/^[Cc]/b
     /^      DATA/b
     /^ [Ff][Uu][Nn][Cc][Tt][Ii][Oo][Nn] *[Ll][Nn][Bb][Ll][Nn][Kk]/b
     /^ [Ff][Uu][Nn][Cc][Tt][Ii][Oo][Nn] *[Cc][Vv][Mm][Gg][TtPp]/b
     /\/ *4H/b
     /[^ ,.=\/]4H/b
     /AIMAG\( *(\)/s// IMAG\1/g
     /VR IMAG/s//VRAIMAG/g
     /^ *[Cc][Oo][Mm][Pp][Ll][Ee][Xx]\*16/!s/^\( *\)[Cc][Oo][Mm][Pp][Ll][Ee][Xx] */\1COMPLEX*16 /
     s/[Dd][Ff][Ll][Oo][Aa][Tt]\( *(\)/FLOAT\1/g
     /[Ff][Ll][Oo][Aa][Tt]/{
      s/[Ff][Ll][Oo][Aa][Tt] *(/DFLOAT(/g
      s/ *$//g
      /^.\{73,\}/s/^\(.\{72\}\)\(.*\)/\1\
     +\2/
               }

     /4H..../{ 
      s/4H\(....\)/NC4TO8("\1")/g
      s/ *$//g
      /^.\{73,\}/s/^\(.\{72\}\)\(.*\)/\1\
     +\2/
               }
      /OPEN(UNIT,FILE=FILNAME,FORM=FORMAT,ACCESS='"'"'APPEND'"'"',IOSTAT=IOS)/{
       s/OPEN(UNIT,FILE=FILNAME,FORM=FORMAT,ACCESS='"'"'APPEND'"'"',IOSTAT=IOS)/OPEN(UNIT,FILE=FILNAME,CONVERT='"'"'BIG_ENDIAN'"'"',FORM=FORMAT,POSITION='"'"'APPEND'"'"',IOSTAT=IOS)/g
       s/ *$//g
       /^.\{73,\}/s/^\(.\{72\}\)\(.*\)/\1\
     +\2/
               }
      /OPEN(UNIT,FILE=FILNAME,FORM=FORMAT,POSITION='"'"'APPEND'"'"',IOSTAT=IOS)/{
       s/OPEN(UNIT,FILE=FILNAME,FORM=FORMAT,POSITION='"'"'APPEND'"'"',IOSTAT=IOS)/OPEN(UNIT,FILE=FILNAME,CONVERT='"'"'BIG_ENDIAN'"'"',FORM=FORMAT,POSITION='"'"'APPEND'"'"',IOSTAT=IOS)/g
       s/ *$//g
       /^.\{73,\}/s/^\(.\{72\}\)\(.*\)/\1\
     +\2/
               }
     /[Aa][Cc][Cc][Ee][Ss][Ss]=.[Aa][Pp][Pp][Ee][Nn][Dd]./{
      s/[Aa][Cc][Cc][Ee][Ss][Ss]/POSITION/g
      s/ *$//g
      /^.\{73,\}/s/^\(.\{72\}\)\(.*\)/\1\
     +\2/
               }
     /FORM='"'"'UNFORMATTED'"'"'/{
      s/\(FORM='"'"'UNFORMATTED'"'"'\)/CONVERT='"'"'BIG_ENDIAN'"'"',\1/g
      s/ *$//g
      /^.\{73,\}/s/^\(.\{72\}\)\(.*\)/\1\
     +\2/
               }
     /[Cc][Vv][Mm][Gg][Tt]/{
      s/[Cc][Vv][Mm][Gg][Tt] *(/MERGE(/g
               }
     /[Ll][Nn][Bb][Ll][Nn][Kk]/{
      s/[Ll][Nn][Bb][Ll][Nn][Kk]/LEN_TRIM/g
      /^.\{73,\}/s/^\(.\{72\}\)\(.*\)/\1\
     +\2/
               }
    ' > $Tmpfil 
  diff $DiffSwtch $1 $Tmpfil > /dev/null && \rm -f $Tmpfil || \cp $Tmpfil $1
  elif [ "$OS" = 'Linux' -a "$F77CUT3" = 'g77' ] ; then
  # "g77" on Linux case ...
  $AWK '{
     if ( substr($0,1,1) ~ /[Cc]/ ) {
      print $0
      }
     else {
      print substr($0,1,72)
      }
     }' $1 | sed '/^[Cc]/b
     /[Bb][Aa][Cc][Kk][Ss][Pp][Aa][Cc][Ee]/{
      s/[Bb][Aa][Cc][Kk][Ss][Pp][Aa][Cc][Ee]\(.*\)$/CALL G77BACK(\1)/
      s/ *$//g
      /^.\{73,\}/s/^\(.\{72\}\)\(.*\)/\1\
     +\2/
     }
     /'"'"'REAL'"'"'/b
     /'"'"'real'"'"'/b
     /^       *[Ii][Mm][Pp][Ll][Ii][Cc][Ii][Tt] *[Rr][Ee][Aa][Ll]\*/b
     /^       *[Ii][Mm][Pp][Ll][Ii][Cc][Ii][Tt]/s/[Rr][Ee][Aa][Ll] */REAL*8/g
     /^       *[Ii][Mm][Pp][Ll][Ii][Cc][Ii][Tt]/!s/[Rr][Ee][Aa][Ll] *(/DBLE(/g
     /^       *[Rr][Ee][Aa][Ll]\*/b
     /^       *[Rr][Ee][Aa][Ll]/{
      s/^       *[Rr][Ee][Aa][Ll]/      REAL*8/
      s/ *$//g
      /^.\{73,\}/s/^\(.\{72\}\)\(.*\)/\1\
     +\2/
     }

     /^      DATA/b
     /^ [Ff][Uu][Nn][Cc][Tt][Ii][Oo][Nn] *[Ll][Nn][Bb][Ll][Nn][Kk]/b
     /^ [Ff][Uu][Nn][Cc][Tt][Ii][Oo][Nn] *[Cc][Vv][Mm][Gg][TtPp]/b
     /\/ *4H/b
     /[^ ,.=\/]4H/b
     /AIMAG\( *(\)/s// IMAG\1/g
     /VR IMAG/s//VRAIMAG/g
     /^       *[Cc][Oo][Mm][Pp][Ll][Ee][Xx]\*16/!{
      s/^       *[Cc][Oo][Mm][Pp][Ll][Ee][Xx] */      COMPLEX*16 /
      s/ *$//g
      /^.\{73,\}/s/^\(.\{72\}\)\(.*\)/\1\
     +\2/
     }
      s/[Dd][Ff][Ll][Oo][Aa][Tt]\( *(\)/FLOAT\1/g
      s/[Ff][Ll][Oo][Aa][Tt] *(/DBLE(/g
      s/\([0-9]\+\.[0-9]*\)E\([+-][0-9]\+\)/\1D\2/g
      s/\([0-9]\+\.[0-9]*\)E\([0-9]\+\)/\1D\2/g


     /4H..../{ 
      s/4H\(....\)/NC4TO8("\1")/g
      s/ *$//g
      /^.\{73,\}/s/^\(.\{72\}\)\(.*\)/\1\
     +\2/
               }
     /[Pp][Oo][Ss][Ii][Tt][Ii][Oo][Nn]=.[Aa][Pp][Pp][Ee][Nn][Dd]./{
      s/[Pp][Oo][Ss][Ii][Tt][Ii][Oo][Nn]/ACCESS/g
      s/ *$//g
      /^.\{73,\}/s/^\(.\{72\}\)\(.*\)/\1\
     +\2/
               }
     /[Cc][Vv][Mm][Gg][Tt]/{
      s/[Cc][Vv][Mm][Gg][Tt] *(/MERGE(/g
               }
     /[Ll][Nn][Bb][Ll][Nn][Kk]/{
      s/[Ll][Nn][Bb][Ll][Nn][Kk]/LEN_TRIM/g
      /^.\{73,\}/s/^\(.\{72\}\)\(.*\)/\1\
     +\2/
               }
    ' > $Tmpfil 
  diff $DiffSwtch $1 $Tmpfil > /dev/null && \rm -f $Tmpfil || \cp $Tmpfil $1
  fi
 fi # float
 (\rm -f $Tmpfil || : )
fi # file specified
exit
