#!/bin/sh

#   Nov 14/03 - F. Majaess

#id relocat  - relocates file(s) off the archive server short term dataclass.

#   AUTHOR  - F. Majaess

#hd PURPOSE - "relocat" script is used to relocate file(s) from the sccount's
#hd           associated short term dataclass onto the default subdirectory
#hd           on the archive server.
#hd           Note: Ownership/permission of matching file(s) must have valid
#hd                 setting in order to permit successful dataset(s)
#hd                 relocation.
#hd                 Matching filenames must share the same valid prefix (as 
#hd                 defined in the standard filename convention) if the master 
#hd                 data directory (via "offcl=yes"; see "offcl" switch 
#hd                 description below) is where the relocated datasets are 
#hd                 to reside under.
#hd

#pr PARAMETERS:
#pr
#pr   POSITIONAL
#pr
#pr     f1 ... fn  = list of n file names to be relocatd
#pr                  (wild card characters format suitable for use with 
#pr                   "ls" command is acceptable).
#pr
#pr     fil_grpname= User has the option of specifying one of
#pr                  'ccrn_shr/ccrn_stf/ccrn_net/ccrn_rcm/ccrn_mam/ccrn_gst'
#pr                  group ownership on the relocated files.
#pr                  (=$DEFFGRP).
#pr
#pr     dir_grpname= User has the option of specifying one of
#pr                  'ccrn_shr/ccrn_stf/ccrn_net/ccrn_rcm/ccrn_mam/ccrn_gst'
#pr                  group ownership on created subdirectory(ies) 
#pr                  (if any).
#pr                  (=$DEFDGRP).
#pr                  
#pr           offcl= switch used to target the master data directory 
#pr                  for relocating the datasets onto on the file server.
#pr                  Default subdirectory will be targeted if this 
#pr                  switch is not set to "yes".
#pr                  This switch when set to "yes" will enforce 
#pr                  "nobackup=no".
#pr                  (=no/yes)
#pr       nobackup = switch used to archive data only on primary 
#pr                  tape (default is to archive data on primary
#pr                  and secondary tapes) 
#pr                  (set to "no" if offcl="yes")
#pr                  (=no/yes)
#pr         cfsuser= switch enable targeting "cfsuser" account's associated
#pr                  short term dataclass subdirectory on cfs instead of
#pr                  user's own subdirectory.
#pr                  (targeted files for relocation must be owned by the
#pr                   invoker username).
#pr   

#ex EXAMPLE:
#ex   
#ex     relocat filea 'fileb*'
#ex
#ex     The above relocates 'filea' and all files whose names start
#ex     with 'fileb' from the associated short term dataclass into
#ex     the default subdirectory on the archive server.
#ex
#ex     relocat cfsuser=acrnxyz filex
#ex
#ex     The above results in relocating 'filex' from the associated short
#ex     term dataclass subdirectory with 'acrnxyz' account into the invoker
#ex     account's associated default subdirectory on the archive server.
#ex     Note: The short term dataclass subdirectory must have the proper
#ex           permission and "filex" must be under the invoker account's 
#ex           ownership in order for the relocation to be successful.


#  * Reset field separators (otherwise those defined in the parent process
#  * will be used and these may cause problems if they include special 
#  * characters used in this script). 

IFS=' '
export IFS
# set -x

#  * Set variable 'AWK' to the version of "awk" to be used.

AWK=${AWK:='awk'}

#  * Obtain the list of file(s) to be relocated.

arg_list=$@
unset file_list
for arg in $arg_list
do 
  case $arg in
       -*) set $arg                    ;;
      *=*) eval "$arg"                 ;;
    offcl) offcl='yes'                 ;;
 nobackup) nobackup='yes'              ;;
        *) file_list="$file_list $arg"
  esac
done

if [ -z "$file_list" ] ; then
 echo "" ; echo "" ; echo "  relocat: no list of files to be relocated is found" ; echo ""
 exit 1
fi
Lgname=`logname`
usr=`echo $Lgname |  sed 's/^....//'`
grp=`echo $Lgname |  sed 's/...$//'`
if [ -n "$cfsuser" ] ; then
 cfsuser=`echo $cfsuser | tr '[A-Z]' '[a-z]'`
 usrt=`echo $cfsuser | sed 's/^....//'`
 grpt=`echo $cfsuser | sed 's/...$//'`
else
#usrt=`echo $Lgname |  sed 's/^....//'`
#grpt=`echo $Lgname |  sed 's/...$//'`
 usrt="$usr"
 grpt="$grp"
 cfsuser="$Lgname"
fi
master_account='acrnsrc'
arcvmid='cfs2'
# Set maximum total storage disk space per set (bytes)
# Mxszprst='25000000000'
# Mxszprst='15000000000'
# Mxszprst='50000000000'
  Mxszprst='35000000000'
# Set maximum number of files to process per set
Mxflsprst='200'
# dir_grpname=${dir_grpname:='ccrn_shr'}
DEFDGRP=${DEFDGRP:='ccrn_shr'}
dir_grpname=${dir_grpname:=$DEFDGRP}
# fil_grpname=${fil_grpname:='ccrn_shr'}
DEFFGRP=${DEFFGRP:='ccrn_shr'}
fil_grpname=${fil_grpname:=$DEFFGRP}
if [ -n "$fil_grpname" ] ; then
  Tfil_grpname=$fil_grpname
  case $Tfil_grpname in
  ccrn_shr|ccrn_stf|ccrn_net|ccrn_rcm|ccrn_mam|ccrn_gst) fil_grpname=$Tfil_grpname ;;
  *) fil_grpname='ccrn_shr' ;
     echo "" ; echo "" ; echo "  relocat: Sorry, $fil_grpname file group ownership will be used" ;
     echo "           instead of the specified $Tfil_grpname !" ; echo ""
  esac
  unset Tfil_grpname
fi
offcl=${offcl:='no'}
unset savdir
offcldir='/home/cfs_ccrd/ccrn/offcl_data'

# Define subdirectory path where the files are to be relocated from ...
# User's associated short term subdirectory ...

unset shrtmdir 
### if [ "$cfsuser" = 'acrpmdm' -o "$cfsuser" = 'acrmril' ] ; then
### if [ "$cfsuser" = 'acrpxxx' -o "$cfsuser" = 'acrmxxx' ] ; then
if [ "$cfsuser" = 'acrp001' -o "$cfsuser" = 'acrplbw' -o "$cfsuser" = 'acrpssu' -o "$cfsuser" = 'acrmrmc' -o "$cfsuser" = 'acrmrac' ] ; then
  shrtmdir="/home/cfs_ccrd/ccrd_short_term_archive/$grpt/$usrt"
else
 if [ "$grpt" = 'acrn' ] ; then
  case $usrt in
                            src ) shrtmdir=${shrtmdir:=$trgtdir} ;;
   nys|rbd|rfp|rmh|yam|yjk) shrtmdir="/home/cfs_ccrd/ccrd_long_term_archive/$usrt"  ;;
   005|006|002|007|003|nbp|mcr|mky|all|cbn|ocn|opt|sip|cc1|cc2|cc3|cc4|esm|gcm|ice|rdr|rdy|rfm|rgb) shrtmdir="/home/cfs_ccrd/ccrd_short_term_archive/$usrt"  ;;
   mr1|mr2|mr3|mr4|mr5|mr6|mr7|mr8|mr9|pd1|pd2|pd3|pd4|pd5|pd6|pd7|pd8|pd9) shrtmdir="/home/cfs_ccrd/ccrd_short_term_archive/$usrt"  ;;
   01[0-9]|020) shrtmdir="/home/cfs_ccrd/ccrd_short_term_archive/$usrt"  ;;
   rjk|ryj|rmq|wsl|rjl|rjs|smp|rby|ros|rdp|rrh|rsk|rks|rrm|rmn|rtm|008|rjc|rls|rms|rns|rrr|raa|rrs|rsa|ror|rjm|rpg|red) shrtmdir="/home/cfs_ccrd/ccrd_short_term_archive/$usrt"  ;;
   rkz|rwm|rmm|ram|rng|rkd|rkm|rgf|rjf|rmg|rcc|rva|rbb|rbt|ncs|jcl|jos|rja|004|rmb|rml|rnm|rsl|rwl) shrtmdir="/home/cfs_ccrd/ccrd_short_term_archive/$usrt"  ;;
   rrd|rhb|rec|rdv|jpb|vrm ) shrtmdir="/home/cfs_ccrd/ccrd_short_term_archive/$usrt"  ;;
   001|009|c02|y01|y03|y04|y06|y07|y09|yww|ydc|ysb|yvf|ycm ) shrtmdir="/home/cfs_ccrd/ccrd_short_term_archive/$usrt"  ;;
   #q0[1-7]|qrj|qaf|qdc|qdp|qhc|qmg|qsb ) shrtmdir="/home/cfs_ccrd/ccrd_short_term_archive/$usrt"  ;;
    qmg ) shrtmdir="/home/cfs_ccrd/ccrd_short_term_archive/$usrt"  ;;
                              * ) echo "" ; echo "" ; echo "  relocat: your account is not valid, usrt=$usrt " ; echo "" 
                                  exit 2 ;;
  esac
 fi
fi
###
if [ -z "$shrtmdir" ] ; then
  echo "" ; echo "" ; echo "  relocat: Sorry, target subdirectory to relocate dataset(s) from, must be defined!" ; echo ""
  exit 3
else
  trgtdir="$shrtmdir"
  unset shrtmdir
fi

# Define subdirectory path where the files are to be relocated onto...

if [ "$offcl" = 'yes' ] ; then
 savdir=$offcldir
 nobackup='no'
else
 nobackup=${nobackup:='no'}
 CcRdUsRa='ccrd_user_archive'
 CnEtUsRa='cnet'
# 003|004) savdir="/home/cfs_ccrd/ccrn/$usr"  ;;
 ### if [ "$Lgname" = 'acrpmdm' -o "$Lgname" = 'acrmril' ] ; then
 ### if [ "$Lgname" = 'acrpxxx' -o "$Lgname" = 'acrmxxx' ] ; then
 if [ "$Lgname" = 'acrp001' -o "$Lgname" = 'acrplbw' -o "$Lgname" = 'acrpssu' -o "$Lgname" = 'acrmrmc' -o "$Lgname" = 'acrmrac' ] ; then
  savdir="/home/cfs_ccrd/$CnEtUsRa/$grp/$usr"
 else
 if [ "$grp" = 'acrn' ] ; then
  if [ "$Lgname" = "$master_account" -a "$offcl" != 'yes' ] ; then
   usr="$usrt"
  fi
 case $usr in
#                         src ) savdir="/home/cfs_ccrd/ccrn/$usr"  ;;
#                  003|004|src) savdir="/home/cfs_ccrd/ccrn/offcl_data"  ;;
                    src) savdir="/home/cfs_ccrd/ccrn/offcl_data"  ;;
# nys|rbd|rfp|rmh|yam|yjk) savdir="/home/cfs_ccrd/ccrd_long_term_archive/$usr"  ;;
  005|006|002|007|003|nbp|mcr|mky|all|cbn|ocn|opt|sip|cc1|cc2|cc3|cc4|esm|gcm|ice|rdr|rdy|rfm|rgb) savdir="/home/cfs_ccrd/$CcRdUsRa/$usr"  ;;
  mr1|mr2|mr3|mr4|mr5|mr6|mr7|mr8|mr9|pd1|pd2|pd3|pd4|pd5|pd6|pd7|pd8|pd9) savdir="/home/cfs_ccrd/$CcRdUsRa/$usr"  ;;
  01[0-9]|020) savdir="/home/cfs_ccrd/$CcRdUsRa/$usr"  ;;
  rjk|ryj|rmq|wsl|rjl|rjs|smp|rby|ros|rdp|rrh|rsk|rks|rrm|rmn|rtm|008|rjc|rls|rms|rns|rrr|raa|rrs|rsa|ror|rjm|rpg|red) savdir="/home/cfs_ccrd/$CcRdUsRa/$usr"  ;;
  rkz|rwm|rmm|ram|rng|rkd|rkm|rgf|rjf|rmg|rcc|rva|rbb|rbt|ncs|jcl|jos|rja|004|rmb|rml|rnm|rsl|rwl) savdir="/home/cfs_ccrd/$CcRdUsRa/$usr"  ;;
  rrd|rhb|rec|jpb|rdv|vrm ) savdir="/home/cfs_ccrd/$CnEtUsRa/$usr"  ;;
  001|009|c02|y01|y03|y04|y06|y07|y09|yww|ydc|ysb|yvf|ycm ) savdir="/home/cfs_ccrd/$CnEtUsRa/$usr"  ;;
  # q0[1-7]|qrj|qaf|qdc|qdp|qhc|qmg|qsb ) savdir="/home/cfs_ccrd/$CnEtUsRa/$usr"  ;;
  qmg ) savdir="/home/cfs_ccrd/$CnEtUsRa/$usr"  ;;
                             * ) echo "" ; echo "" ; echo "  relocat: your account is not valid" ; echo "" 
                                 exit 4 ;;
 esac
 fi
 fi
 if [ -n "$savdir" ] ; then
  : 
 else
   echo "" ; echo "" ; echo "  relocat: ==> Sorry, no associated default subdirectory is setup to relocate data onto on the archive server?" ; echo ""
   exit 5
 fi
fi
masterdir=$offcldir

if [ "$nobackup" = 'yes' ] ; then

# set up option to archive data only on primary tape 
# and for immediate truncation.

 option='-c1 -fi'

else

# set up option to archive data primary and secondary tapes
# and for policy truncation.

# option='-c2 -fp'
 option='-c2 -fi'

fi

# Ensure archive server is accessible...

unset hstnm
hstnm=`ssh $arcvmid hostname 2>/dev/null | cut -c 1-4 `
hststate=`ssh $arcvmid fsstate -f | grep 'The Tertiary Manager system is active.'`
hststrgtdirck=`ssh $arcvmid ls -1d /home/cfs_ccrd/ccrn`
while [ "$hstnm" != "$arcvmid" -o "$hststate" != 'The Tertiary Manager system is active.' -o "$hststrgtdirck" != '/home/cfs_ccrd/ccrn' ]
 do
  Tmstmp=`date`
  echo "" ; echo "" ; echo "  relocat: archive server is not accessible ~ $Tmstmp ! " ; echo ""
  sleep 300
  unset hstnm hststate
  hstnm=`ssh $arcvmid hostname 2>/dev/null | cut -c 1-4 `
  hststate=`ssh $arcvmid fsstate -f | grep 'The Tertiary Manager system is active.'`
  hststrgtdirck=`ssh $arcvmid ls -1d /home/cfs_ccrd/ccrn`
 done

# Abort if subdirectory to relocate datasets from is no existing ...

sline1=`ssh $arcvmid fsls -nd $trgtdir 2>/dev/null | tail -1 | grep '^d' | grep $trgtdir `
if [ -z "$sline1" ] ; then
 echo "" ; echo "" ; echo "  relocat: Error; $trgtdir subdirectory, to relocate datasets from, does not exit on the archive server!" ; echo ""
 exit 6 
else
 echo "" ; echo "" ; echo "  relocat: Target subdirectory to relocate datasets from, is: $trgtdir " ; echo ""
fi

# Create and move into a temporary subdirectory in which some
# needed files will be created ....

Stamp="${HOSTID}_"`date +%Y%j%H%M%S`
mkdir $HOME/tmp/tmp_relocat_$$_$Stamp
cd $HOME/tmp/tmp_relocat_$$_$Stamp
# cd ..
# rm -r tmp_relocat_$$_$Stamp

# Obtain the list of files to process in "Fullflist" local file ...

(\rm -f Fullflist || : )
for fname in $file_list
do
# echo "Scanning for $fname"
 (\rm -f Indvdlflist Errflist Invldflist || : )
 (set +xv ;  eval "ssh $arcvmid 'cd $trgtdir ; ls -ld $fname '" > Indvdlflist  2>>Errflist )

 # Disallow processing links or subdirectories...

 if [ -s Indvdlflist ] ; then
  cat Indvdlflist | sed -n -e '/^d/p' -e '/^l/p' > Invldflist
 fi
 if [ -s Errflist -o ! -s Indvdlflist -o -s Invldflist ] ; then
  echo "" ; echo " relocat: ********************** ENCOUNTERED ERRORS *********************" ; echo ""
  if [ ! -s Indvdlflist ] ; then
   echo "Empty list of matching files for [$fname] scan in $trgtdir !"
  fi
  if [ -s Invldflist ] ; then
   echo "Detected illegal matching links and/or subdirectories:" ; echo ""
   cat Invldflist
   echo "" ; echo "while processing for [$fname] scan in $trgtdir !"
  fi
  if [ -s Errflist ] ; then
   echo "The following error message(s) were issued ...:" ; echo ""
   echo ">>>>>>>>>>>" ; echo ""
   cat Errflist
   echo "" ; echo "<<<<<<<<<<<" ; echo ""
  fi
  echo "" ; echo "relocat: Will proceed by skip processing --> [$fname] <-- argument !" ; echo ""
  echo "***************************************************************************" ; echo ""
 else
#  echo "" ; echo "List of matching files from [$fname] scan:" ; echo ""
#  cat Indvdlflist
   cat Indvdlflist >> Fullflist
#  echo "" ; echo "*****" ; echo ""
 fi
 done

(\rm -f Indvdlflist Errflist Invldflist || : )

if [ -s Fullflist ] ; then
 (\rm -f TFullflist || : )
#cat Fullflist | sed -e 's/  */ /g' | sort -k9 | uniq > TFullflist
#cat Fullflist | $AWK '{printf "%10s%5s%8s%10s%12d%4s%3s%6s %-s\n",$1,$2,$3,$4,$5,$6,$7,$8,$9}' | sort -k9 | uniq > TFullflist
 cat Fullflist | $AWK '{printf "%10s%5s%8s%10s%12s%4s%3s%6s %-s\n",$1,$2,$3,$4,$5,$6,$7,$8,$9}' | sort -k9 | uniq > TFullflist
 mv TFullflist Fullflist
 echo "" ; echo " relocat: Full list of matching files to process:" ; echo ""
 cat Fullflist
else
 echo "" ; echo "" ; echo "  relocat: Error; Unable to get matching files list from the archive server!" ; echo ""
 cd ..
 rm -r tmp_relocat_$$_$Stamp
 exit 8
fi
#!!!!!!!!!

# Check and abort if any of the files to be processed is owned
# by other than the invoker active username...

(\rm -f InvalidList || : )

if [ "$Lgname" != "$master_account" ] ; then
  (set +xv ;  cat Fullflist | eval "$AWK ' { if ( \$3 !~ /$Lgname/ ) { print \$0 ; } ; } ' " > InvalidList )
  if [ -s InvalidList ] ; then
   echo "" ; echo "" ; echo "  relocat: Error; Sorry can not proceed since the following files are detected not to"
   echo "                  be under the ownership of the invoker active username ($Lgname):" ; echo ""
   cat InvalidList
   echo "" ; echo " If need be, please seek assistance from CCCma archive sever"
   echo " resource manager to rectify the encountered problem(s)" ; echo ""
   cd ..
   rm -r tmp_relocat_$$_$Stamp
   exit 9
  fi
fi
# 
# Check and abort if the master subdirectory is where the files are 
# to be relocated onto,(ie. "$offcl" = 'yes' or "$savdir" = "$offcldir" ),
# and not all the files share the same standard prefix...

if [ "$offcl" = 'yes' -o "$savdir" = "$offcldir" ] ; then
 (\rm -f InvalidList || : )

 (set +xv ;  cat Fullflist | $AWK '{ print $9 ; } ' | $AWK -F'_' ' BEGIN { linenum = 0 ; prfx = 0 } ; { linenum = ++linenum ; if ( linenum == 1 ) { typeclass=$1 ; runame=$2 ; } else if ( $1 != typeclass || $2 != runame ) { if ( prfx == 0 ) { {print "  Expected prefix based on the first filename: " typeclass "_" runame ; prfx = 1 ;} {print "\n  The following is a list of detected invalid filenames:\n" ; } ; }; print "    " $0 ; } ; } ' > InvalidList )

 if [ -s InvalidList ] ; then
  echo "" ; echo " relocat: *********** ERROR *********** " ; echo ""
  echo "" ; echo " Since the target subdirectory to relocate the files onto is:" ; echo "    $savdir " ; echo ""
  echo " all files should share the same prefix..." ; echo "" ; echo " *******************" ; echo ""
  cat InvalidList
  echo "" ; echo " *******************" ; echo ""
  echo "" ; echo " relocat: Process can not proceed and it will terminate..." ; echo ""
  cd ..
  rm -r tmp_relocat_$$_$Stamp
  exit 10
 fi

 # Abort if the filename prefix is not suitable for targeting the master subdirectory...

 # tocrack=`head -1 Fullflist |  $AWK '{ print $9 ; } ' | $AWK -F'_' '{print $1 " " $2 ; }'`
 tocrack=`head -1 Fullflist |  $AWK '{ print $9 ; } ' | $AWK -F'_' '{ f1=$1 ; f2=$2 ; if ( f2 != "ipcc-ddc" ) { i=index(f2,"-") ; if ( i > 1 ) { f2=substr(f2,1,i-1) ; } ; } ; print f1 " " f2 ; }'`
 if [ -n "$tocrack" ] ; then
  tocrack=`echo "$tocrack 99 99"`
  type=`echo $tocrack | $AWK '{ chr = substr($1,1,1) } ; END { print chr ; }' `
  class=`echo $tocrack | $AWK '{ chr = substr($1,2,1) } ; END { print chr ; }' `
# runame=`echo $tocrack | $AWK '{ if ( length($2) > 5 ) { print "99" } else { print $2 ; } ; }'`
  runame=`echo $tocrack | $AWK '{ if ( length($2) > 8 ) { print "99" } else { print $2 ; } ; }'`
  if [ "$runame" != '99' ] ; then
   NoSpclCrctr=`echo $tocrack | $AWK '{ print $2 ; }' | sed -e 's/^[a-z][a-z][a-z0-9]*/true/'`
   if [ "$NoSpclCrctr" != 'true' ] ; then
    runame='99'
   fi
   unset NoSpclCrctr
  fi
  case $type in
   m|p|d|t|s) : ;;
   *) unset type
  esac
  case $class in
   a|o|c|m|r|f|d) : ;;
   *) unset class
  esac
  if [ -n "$type" -a -n "$class" -a "$runame" != '99' ] ; then
   savdir="$savdir/$class/$runame/$type"
  else
   echo "" ; echo "" ; echo "  relocat: Error; Sorry can not proceed due to failed mapping into a valid master subdirectory, based on the specified file prefix!" ; echo ""
   cd ..
   rm -r tmp_relocat_$$_$Stamp
   exit 11
  fi
 else
  echo "" ; echo "" ; echo "  relocat: Error; Sorry can not proceed due to unsuitable file name prefix to map onto master subdirectory!" ; echo ""
  cd ..
  rm -r tmp_relocat_$$_$Stamp
  exit 12
 fi
fi
# 

# set -a

# Check and abort if there is a matching filename already present for any
# of the files to be relocated in the target destination subdirectory ...

chkprsnc () {
 # set -x
 unset destfln prsntfl
 if [ -n "$1" -a -n "$savdir" -a -n "$arcvmid" ] ; then
  destfln="$savdir/$1"
  prsntfl=`ssh $arcvmid fsls -nd $destfln 2>/dev/null `
  if [ -n "$prsntfl" ] ; then
   touch haltit
   if [ ! -s haltit ] ; then
    ( echo "" ; echo "" ; echo "  relocat: Error; file(s) already present:" ; echo "")  >> haltit
   fi
   echo "$prsntfl"  >> haltit
  fi
 else
  touch haltit
  (echo "" ; echo "  relocat: Invalid setting for --> chkprsnc $@ <-- call!") >> haltit
 fi
}

(\rm -f chkprsnc_flist || : )
cat Fullflist | $AWK '{print $9 ; }' |  sed -e 's/^/chkprsnc /' > chkprsnc_flist

if [ -s "chkprsnc_flist" ] ; then

  # Ensure archive server is accessible...
  
  unset hstnm
  hstnm=`ssh $arcvmid hostname 2>/dev/null | cut -c 1-4 `
  hststate=`ssh $arcvmid fsstate -f | grep 'The Tertiary Manager system is active.'`
  hststrgtdirck=`ssh $arcvmid ls -1d /home/cfs_ccrd/ccrn`
  while [ "$hstnm" != "$arcvmid" -o "$hststate" != 'The Tertiary Manager system is active.' -o "$hststrgtdirck" != '/home/cfs_ccrd/ccrn' ]
   do
    Tmstmp=`date`
    echo "" ; echo "" ; echo "  relocat: archive server is not accessible ~ $Tmstmp ! " ; echo ""
    sleep 300
    unset hstnm hststate
    hstnm=`ssh $arcvmid hostname 2>/dev/null | cut -c 1-4 `
    hststate=`ssh $arcvmid fsstate -f | grep 'The Tertiary Manager system is active.'`
    hststrgtdirck=`ssh $arcvmid ls -1d /home/cfs_ccrd/ccrn`
   done


## # Abort if subdirectory, to relocate datasets to, is no existing ...
## 
## sline1=`rsh $arcvmid fsls -nd $savdir 2>/dev/null | tail -1 | grep '^d' | grep $savdir `
## if [ -z "$sline1" ] ; then
##  echo "" ; echo "" ; echo "  relocat: Error; $savdir subdirectory, to relocate datasets to, does not exit on the archive server!" ; echo ""
##  exit 7 
## fi

  usrdir=`ssh $arcvmid "cd $savdir ; pwd "`
  
  # Ensure the target destination subdirectory is present ....

  if [ "$savdir" = "$usrdir" ] ; then
   echo "" ; echo "" ; echo "  relocat: Target subdirectory to relocate datasets to, is: $savdir " ; echo ""
   . chkprsnc_flist
  else
   touch haltit
   (echo "" ; echo "" ; echo "  relocat: Sorry, not valid target destination subdirectory --> $savdir <-- !" ; echo "") >> haltit
  fi

fi

if [ -s haltit ] ; then
 echo "" ; echo "" ; echo "  relocat: Error; Sorry can not proceed ..." ; echo ""
 cat haltit 
 echo "" ; echo " If need be, please seek assistance from CCCma archive sever"
 echo " resource manager to rectify the encountered problem(s)" ; echo ""
 cd ..
 rm -r tmp_relocat_$$_$Stamp
 exit 13
fi

# Define "vrfyme" function to check later on for successful file(s)
# relocation..

vrfyme () {
 unset Dfln Xpctsz  destfln Actlsz 
 if [ -n "$1" -a -n "$2" -a -n "$savdir" -a -n "$arcvmid" ] ; then
  Dfln="$1"
  Xpctsz="$2"
  destfln="$savdir/$Dfln"
  Actlsz=`ssh $arcvmid fsls -nd $destfln 2>/dev/null | $AWK '{ print \$5 ; }' `
  if [ -z "$Actlsz" ] ; then
    sleep 5
    Actlsz=`ssh $arcvmid fsls -nd $destfln 2>/dev/null | $AWK '{ print \$5 ; }' `
  fi
  if [ "$Xpctsz" != "$Actlsz" ] ; then
   touch haltit
   (echo "" ; echo "" ; echo "  relocat: Unsuccessful relocation of $destfln :") >> haltit
   (echo "           file size mismatch; Xpctsz=$Xpctsz, Actlsz=$Actlsz !" ; echo "") >> haltit
  else
   ssh $arcvmid "fschfiat -t i $destfln"
   if [ -n "$master_account" -a "$offcl" = 'yes' ] ; then
    ssh $arcvmid chmod ug-w+r,o-w $destfln
    if [ -n "$fil_grpname" ] ; then
     ssh $arcvmid chgrp $fil_grpname $destfln
    fi
    ssh $arcvmid chown $master_account $destfln
   fi
   ssh $arcvmid fsls -nd $destfln
  fi
 else
  touch haltit
  (echo "" ; echo "" ; echo "  relocat: Invalid setting for --> vrfyme $@ <-- call!") >> haltit
 fi 
}

# Loop over the full set of matching files and process them
# respecting the "Mxszprst" and "Mxflsprst" set limits....

ntimes=0
while [ -s Fullflist -a ! -s haltit ]
 do
   ntimes=`expr $ntimes + 1`
#  ls -ld Fullflist
#  echo "" ; echo " ntimes=$ntimes; Fullflist:" ; echo ""
#  cat Fullflist
   # Generate from "Fullflist" file:
   #   "to_process" - the long listing of files to be processed in this round,
   #   "for_later"  - the long listing of remaining files to process in future 
   #                  round(s) (this file becomes "Fullflist" for the next
   #                            round),
   #   "files_list_to_process" - the list of files (one entry per line) to
   #                             be processed in this round,
   #   "files_list_to_verify"  - contain calls to "vrfyme" function in order to 
   #                             verify the proper relocation of the processed
   #                             files in this round. 
   # 
   (\rm -f to_process for_later files_list_to_process files_list_to_verify || : )
   touch to_process for_later files_list_to_process files_list_to_verify
#  set -x
   cat Fullflist | $AWK -v Mxszprst="$Mxszprst" -v Mxflsprst="$Mxflsprst" 'BEGIN { nfls = 0 ; nsz = 0 ; } ; { nfls = nfls + 1 ; nsz = nsz + $5 ; if ( nfls <= 1 || nsz <= Mxszprst && nfls <= Mxflsprst ) { print $0 > "to_process" ; print $9 > "files_list_to_process" ;  print "vrfyme " $9 " " $5 > "files_list_to_verify" ; } else { print $0 > "for_later" ; } ; } ' 
#  set +x

#ls -ld to_process for_later files_list_to_process files_list_to_verify
 if [ -s to_process ] ; then
# echo "" ; echo " ntimes=$ntimes; to_process now:" ; echo ""
# cat to_process
# echo "" ; echo " ntimes=$ntimes; files_list_to_process now:" ; echo ""
# cat files_list_to_process
# echo "" ; echo " ntimes=$ntimes; files_list_to_verify now:" ; echo ""
# cat files_list_to_verify

  # Verify successful relocation of each of these files ...

  echo "" ; echo "" ; echo "  relocat: List of files to be relocated from $trgtdir subdirectory on the archive server: " ; echo ""

  cat to_process

  # Ensure archive server is accessible...
  
  unset hstnm
  hstnm=`ssh $arcvmid hostname 2>/dev/null | cut -c 1-4 `
  hststate=`ssh $arcvmid fsstate -f | grep 'The Tertiary Manager system is active.'`
  hststrgtdirck=`ssh $arcvmid ls -1d /home/cfs_ccrd/ccrn`
  while [ "$hstnm" != "$arcvmid" -o "$hststate" != 'The Tertiary Manager system is active.' -o "$hststrgtdirck" != '/home/cfs_ccrd/ccrn' ]
   do
    Tmstmp=`date`
    echo "" ; echo "" ; echo "  relocat: archive server is not accessible ~ $Tmstmp ! " ; echo ""
    sleep 300
    unset hstnm hststate
    hstnm=`ssh $arcvmid hostname 2>/dev/null | cut -c 1-4 `
    hststate=`ssh $arcvmid fsstate -f | grep 'The Tertiary Manager system is active.'`
    hststrgtdirck=`ssh $arcvmid ls -1d /home/cfs_ccrd/ccrn`
   done
  
###   byFreeSpace=${byFreeSpace='on'}
###   if [ "$byFreeSpace" = 'on' ] ; then
###    # Make sure of the space on the disk
###    FreeSpace=`ssh $arcvmid df -Pm $savdir | tail -1 | awk '{ print $4 ; }'`
###    FreeSpace=${FreeSpace=0} 
###    while test $FreeSpace -lt 95000
###    do
###     echo "" ; echo "" ; echo "  relocat : waiting, free disk space is $FreeSpace MB (< 95GB) on cfs:$savdir ~ `date` " ; echo "" 
###     sleep 300 
###     unset FreeSpace 
###     FreeSpace=`ssh $arcvmid df -Pm $savdir | tail -1 | awk '{ print $4 ; }'`
###     FreeSpace=${FreeSpace=0} 
###     done 
###    #
###   else
###    # Make sure of the space on the disk
###  
###    # DskSpace=`rsh $arcvmid df $savdir | tail -1 | awk 'BEGIN {} { print $6 ; } END {} ' `
###    # DskSpace=`ssh $arcvmid df $savdir | tail -1 | sed -e 's/%/ /g' | awk 'BEGIN {} { print $5 ; } END {} ' `
###    DskSpace=`ssh $arcvmid df -P $savdir | tail -1 | sed -e 's/%/ /g' | awk 'BEGIN {} { print $5 ; } END {} ' `
###    DskSpace=${DskSpace=100} 
###    # while test $DskSpace -gt 82
###    while test $DskSpace -gt 92
###    do
###     echo "" ; echo "" ; echo "  relocat : waiting, disk space usage is $DskSpace % on cfs:$savdir ~ `date` " ; echo "" 
###     sleep 300 
###     unset DskSpace 
###     # DskSpace=`ssh $arcvmid df $savdir | tail -1 | sed -e 's/%/ /g' | awk 'BEGIN {} { print $5 ; } END {} ' `
###     DskSpace=`ssh $arcvmid df -P $savdir | tail -1 | sed -e 's/%/ /g' | awk 'BEGIN {} { print $5 ; } END {} ' `
###     DskSpace=${DskSpace=100} 
###     done 
###   fi
  # Relocate the list of files supplied in "files_list_to_process" from "trgtdir"
  # onto "savdir" on the archive server....

  echo "" ; echo "" ; echo "  relocat: As of `date`, relocation request is just about to be launched" ; echo ""
# echo "Core call is disabled for now..."
  cat files_list_to_process | ssh $arcvmid "cd $trgtdir ; rip - -n 1 -exec mv {} $savdir "
  echo "" ; echo "" ; echo "  relocat: As of `date`, relocation request is finished" ; echo ""
  sleep 5
  

  # Verify successful relocation of each of these files ...

  echo "" ; echo "" ; echo "  relocat: List of relocated files onto $savdir subdirectory on the archive server: " ; echo ""

  . files_list_to_verify

 fi


 # Prepare for processing the next batch (if any) ...

#if [ -s for_later ] ; then
# echo "" ; echo " ntimes=$ntimes; for_later:" ; echo ""
# cat for_later
#fi

 mv for_later Fullflist

 done

if [ -s haltit ] ; then

 echo "" ; echo "  relocat: Errors encountered ..."
 cat haltit 
 cd ..
 rm -r tmp_relocat_$$_$Stamp
 exit 14

else
 
# Normal termination ...

 cd ..
 rm -r tmp_relocat_$$_$Stamp

 exit
fi
