#! /bin/sh
 
#    MAR 10/92 - F.Majaess
 
#id  scndry_dflt_prmtrs - Used to check/set secondary defaults.
 
#    AUTHOR  - F.Majaess
 
#hd  PURPOSE - "scndry_dflt_prmtrs" code is used to check/set 
#hd            parameters to their secondary default values...
 
#pr  PARAMETERS:
#pr
#pr    PRIMARY/SECONDARY
#pr
#pr      assem  = switch accomodating user request to process assembler
#pr               code instead of Fortran (=no/yes).
#pr      adt    = switch controls producing an audit of back-end disk
#pr               space usage (=no/yes).
#pr      byaccss= switch controls processing of back-end files which have
#pr               not been accessed since a specified date (=no/yes).
#pr      byown  = switch controls processing only user's own files (=no/yes).
#pr      byusers= switch controls processing of back-end files by 
#pr               section/users (=no/yes).
#pr      change = switch controlling listing/changing password occurences.
#pr               (=no/yes)
#pr      cft    = switch accomodating user request to use CFT instead of
#pr               CFT77 to compile Fortran source code (=no/yes).
#pr      rmv    = switch controlling deletion of input file (=no/yes).
#pr      script = switch to generate a copy of the pre-processed job
#pr               script (i.e the next job in the string) without sending
#pr               the job to NQS (diagnostic jobs only) (=/'script').
#pr      extlib = external library to be referenced for the loader
#pr               (=NOLIB/AAAALIB).
#pr      full   = switch to accomodate user request to process whole set/
#pr               options (=no/yes).
#pr      keep   = switch controls keeping of input file (=no/yes).
#pr      lc     = update control character (='*'/'%')
#pr      lpath  = path of subdirectory containig scripts  
#pr               (=$CCRNSTD/$SUBPROC).
#pr      nobin  = switch controlling type of data transfer (=no/yes).
#pr      noconv = switch controlling conversion from/to CCRN standard binary
#pr               file to/from character format (=no/yes).
#pr      nolist = switch controlling source listing (=no/yes).
#pr      nobj   = switch controlling creation of an object library on the
#pr               back-end (=no/yes).
#pr      nocheck= switch used to double/bypass checking parameters
#pr               entered (=no/yes).
#pr      nosvaf = switch controlling saving accessed file on the front-end
#pr               (=no/yes).
#pr      nosave = switch controlling saving generated file on the back-end
#pr               (=no/yes).
#pr      others = switch controls deletion of back-end temporary diagnostic
#pr               run files (=no/yes).
#pr      ppath  = path of subdirectory containing output  (=$OUTPUTQ/$PRNTQ).
#pr      rs     = switch controls deletion of back-end model restart files
#pr               (=no/yes).
#pr      sjtfe  = switch controls job submission to the front-end instead 
#pr               of the back-end (=no/yes).
#pr      subaf  = switch accomodating user request to submit an accessed
#pr               file from another machine (=no/yes).
#pr      sx3tmfx= switch controlling file transfer direction from mfx/sx3 
#pr               to  sx3/mfx (=no/yes).
#pr      secdata= switch controlling default "despath" to primary (=no;default)
#pr               or secondary (=yes) data subdirectory on destination machine.
#pr      sx3    = switch controls changing destination to SX3 (=no/yes).
#pr      updat  = switch controlling creation of front-end update library 
#pr               (=no/yes).
#pr      updates= switch indicating updates to deck are to be used
#pr               (=no/yes).
#pr      updt1  = switch indicating updates to first  deck are to be used
#pr               (=no/yes).
#pr      updt2  = switch indicating updates to second deck are to be used
#pr               (=no/yes).
#pr      vdim   = switch indicating variable dimensioned Fortran source code
#pr               (=no/yes).
#pr      wait   = switch signaling the process to wait (if necessary) for
#pr               transfered input data (=no/yes).
#pr
 
#   * make sure arguments list variable "prmtrl" is initialized...
 
prmtrl=${prmtrl=}
 
#   * The following code deals with checking/setting parameters to
#   * secondary defaults provided "prmtrl" is not empty.
#   * Note: keyword is treated as a value to a positional parameter if 
#   *       the secondary default parameter whose name is "keyword" 
#   *       is already set.
 
if [ -n "$prmtrl" ] ; then
  
  tprmtr=
 
#  * for each argument undelt with yet do...
  
  for prmtr in $prmtrl
  do
      
    case $prmtr in
 
#   * check for keyword(s) then 
#   *     if the corresponding parameter is already set then
#   *        store "keyword" in "tprmtr" so that it is treated 
#   *        later on as a value to a positional parameter
#   *     otherwise set parameter whose name is "keyword" to 
#   *     its secondary default.
 
  assem ) if [ -n "$assem" ] ; then
            tprmtr="$tprmtr $prmtr"
          else
            assem='yes' 
          fi ;;
 
  adt   ) if [ -n "$adt" ] ; then
            tprmtr="$tprmtr $prmtr"
          else
            adt='yes' 
          fi ;;
 
byaccss ) if [ -n "$byaccss" ] ; then
            tprmtr="$tprmtr $prmtr"
          else
            byaccss='yes' 
          fi ;;
 
  byown ) if [ -n "$byown" ] ; then
            tprmtr="$tprmtr $prmtr"
          else
            byown='yes' 
          fi ;;
 
byusers ) if [ -n "$byusers" ] ; then
            tprmtr="$tprmtr $prmtr"
          else
            byusers='yes' 
          fi ;;
 
 change ) if [ -n "$change" ] ; then
            tprmtr="$tprmtr $prmtr"
          else
            change='yes' 
          fi ;;
 
    cft ) if [ -n "$cft" ] ; then
            tprmtr="$tprmtr $prmtr"
          else
            cft='yes' 
          fi ;;
 
    rmv ) if [ -n "$rmv" ] ; then
            tprmtr="$tprmtr $prmtr"
          else
            rmv='yes' 
          fi ;;
 
 script ) if [ -n "$script" ] ; then
            tprmtr="$tprmtr $prmtr"
          else
            script='script' 
          fi ;;
 
 extlib ) if [ -n "$extlib" ] ; then
            tprmtr="$tprmtr $prmtr"
          else
            extlib='AAAALIB' 
          fi ;;
 
   full ) if [ -n "$full" ] ; then
            tprmtr="$tprmtr $prmtr"
          else
            full='yes' 
          fi ;;
 
   keep ) if [ -n "$keep" ] ; then
            tprmtr="$tprmtr $prmtr"
          else
            keep='yes' 
          fi ;;
 
     lc ) if [ -n "$lc" ] ; then
            tprmtr="$tprmtr $prmtr"
          else
            lc='%' 
          fi ;;
 
  lpath ) if [ -n "$lpath" ] ; then
            tprmtr="$tprmtr $prmtr"
          else
            lpath='$SUBPROC' 
          fi ;;
 
  ppath ) if [ -n "$ppath" ] ; then
            tprmtr="$tprmtr $prmtr"
          else
            ppath='$PRNTQ' 
          fi ;;
 
  nobin ) if [ -n "$nobin" ] ; then
            tprmtr="$tprmtr $prmtr"
          else
            nobin='yes' 
          fi ;;
 
nocheck ) if [ -n "$nocheck" ] ; then
            tprmtr="$tprmtr $prmtr"
          else
            nocheck='yes' 
          fi ;;
 
 noconv ) if [ -n "$noconv" ] ; then
            tprmtr="$tprmtr $prmtr"
          else
            noconv='yes' 
          fi ;;
 
  nobj  ) if [ -n "$nobj" ] ; then
            tprmtr="$tprmtr $prmtr"
          else
            nobj='yes' 
          fi ;;
 
 nolist ) if [ -n "$nolist" ] ; then
            tprmtr="$tprmtr $prmtr"
          else
            nolist='yes' 
          fi ;;
 
 nosave ) if [ -n "$nosave" ] ; then
            tprmtr="$tprmtr $prmtr"
          else
            nosave='yes' 
          fi ;;
 
 nosvaf ) if [ -n "$nosvaf" ] ; then
            tprmtr="$tprmtr $prmtr"
          else
            nosvaf='yes' 
          fi ;;
 
 others ) if [ -n "$others" ] ; then
            tprmtr="$tprmtr $prmtr"
          else
            others='yes' 
          fi ;;
 
     rs ) if [ -n "$rs" ] ; then
            tprmtr="$tprmtr $prmtr"
          else
            rs='yes' 
          fi ;;
 
  sjtfe ) if [ -n "$sjtfe" ] ; then
            tprmtr="$tprmtr $prmtr"
          else
            sjtfe='yes' 
          fi ;;
 
  subaf ) if [ -n "$subaf" ] ; then
            tprmtr="$tprmtr $prmtr"
          else
            subaf='yes' 
          fi ;;
 
sx3tmfx ) if [ -n "$sx3tmfx" ] ; then
            tprmtr="$tprmtr $prmtr"
          else
            sx3tmfx='yes' 
          fi ;;
 
  updat ) if [ -n "$updat" ] ; then
            tprmtr="$tprmtr $prmtr"
          else
            updat='yes' 
          fi ;;
 
updates ) if [ -n "$updates" ] ; then
            tprmtr="$tprmtr $prmtr"
          else
            updates='yes' 
          fi ;;
 
  updt1 ) if [ -n "$updt1" ] ; then
            tprmtr="$tprmtr $prmtr"
          else
            updt1='yes' 
          fi ;;
 
  updt2 ) if [ -n "$updt2" ] ; then
            tprmtr="$tprmtr $prmtr"
          else
            updt2='yes' 
          fi ;;
 
   vdim ) if [ -n "$vdim" ] ; then
            tprmtr="$tprmtr $prmtr"
          else
            vdim='yes' 
          fi ;;
 
    sx3 ) if [ -n "$sx3" ] ; then
            tprmtr="$tprmtr $prmtr"
          else
            sx3='yes' 
          fi ;;
 
secdata ) if [ -n "$secdata" ] ; then
            tprmtr="$tprmtr $prmtr"
          else
            secdata='yes' 
          fi ;;
 
  wait ) if [ -n "$wait" ] ; then
            tprmtr="$tprmtr $prmtr"
          else
            wait='yes' 
          fi ;;
 
#   * anything else case accumulate in "tprmtr" temporary var...
 
    *  )  tprmtr="$tprmtr $prmtr";;
 
    esac
  done
  prmtrl=$tprmtr
  tprmtr=''
fi
