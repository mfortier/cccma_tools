#! /bin/sh

#    Oct 18/02 - F.Majaess

#id  chkdelt - Checks and possibly massages "delt" parmsub parameter setting.

#    AUTHOR  - F.Majaess

#hd  PURPOSE - "chkdelt" script is used to check and possibly massage
#hd            "delt"  parmsub parameter setting.
#hd            An abort will result if no-empty critical section setting
#hd            is found (by way of no-empty "critical0" file) and "delt"
#hd            string is not found to be 9 characters long.
#hd            In the case of no critical section setting, "delt" is 
#hd            expected to be consisting of either 6 or 9 characters 
#hd            string length, where in the former case it's massaged to
#hd            9 characters string long.
#hd            
#hd            Note: The definition of "delt", if specified, is assumed 
#hd                  to be supplied in "parmsub.in" file from which it
#hd                  gets extracted, evaluated and checked. If "delt" is
#hd                  found to be defined as a 6 characters long string, 
#hd                  then it gets redefined and propagated for the 
#hd                  subsequent jobs in the bottom of "parmsub.in" 
#hd                  local file as a 9 characters long string by adding 
#hd                  3 blank characters on the left.
#hd                  No action will take place if no active definition for
#hd                  "delt" is present in "parmsub.in" file.

#ex  EXAMPLE:
#ex
#ex   chkdelt  
#ex

(\rm -f chkdelt_delt_captur || : )

# Process information supplied in "parmsub.in" file:

if [ -s "parmsub.in" ] ; then

 # Extract valid lines containing "delt=" into "chkdelt_delt_captur" file.

 cat parmsub.in | sed -n -e '/^ *$/d' -e '/^ *#/d' -e '/delt=/p' > chkdelt_delt_captur

 if [ -s "chkdelt_delt_captur" ] ; then

  # Evaluate the extracted lines into "chkdelt_delt_captur" file.

  . chkdelt_delt_captur

  (\rm -f chkdelt_delt_captur || : )

  if [ -n "$delt" ] ; then

    # Provided "delt" is set...
    # obtain the length of the string defined in it.

    AWK=${AWK:-awk} 
    Tdelt_string_length=`echo "$delt" | sed -e 's/ /_/g'| $AWK '{ print length($1) ; }'`
#   echo "Tdelt_string_length($Tdelt_string_length)"

    # Based on "delt" string length ...

    if [ "$Tdelt_string_length" != '9' ] ; then

     # If it's not 9 characters long...

     if [ -s "critical0" ] ; then

      # Abort if the critical section is not empty (by way of no-empty "critical0" local file) 
    

      echo "\nchkdelt: ****> Since the critical section is not empty; 9 characters <**** "
      echo "         ****> string length is expected for delt(=$delt)!          <**** \n"
      echo "\nchkdelt: ****> Since the critical section is not empty; 9 characters <**** " >> haltit
      echo "         ****> string length is expected for delt(=$delt)!          <**** \n" >> haltit
      exit 1

     else

      # Abort if "delt" string length is not consisting of 9 or 6 characters long.

      if [ "$Tdelt_string_length" != '6' ] ; then

       echo "\nchkdelt: ****> Inappropriate delt(=$delt) string setting of $Tdelt_string_length characters long! <**** \n"
       echo "\nchkdelt: ****> Inappropriate delt(=$delt) string setting of $Tdelt_string_length characters long! <**** \n" >> haltit
       exit 1

      else
       
       # Implement 9 characters long string setting for "delt" as the effective one
       # to use by propagating it into the following job's "parmsub" section via 
       # "parmsub.in" local file.

       echo "#-------- Altered parameter settings:" >> parmsub.in
       echo "delt='   ""$delt""';" >> parmsub.in
       
      fi
     fi

    fi

  fi

 fi

 (\rm -f chkdelt_delt_captur || : )

fi
