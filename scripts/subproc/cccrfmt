#! /bin/sh

#    Aug 18/10 - F.Majaess

#id  cccrfmt - Checks for CCCma record format.

#    AUTHOR  - F.Majaess

#hd  PURPOSE - "cccrfmt" script checks the bit pattern at the beginning 
#hd             of the specified file. It returns "yes" if it is  
#hd             conforming to CCCma 8-word label pattern, otherwise 
#hd             "no" is returned.
#hd             Note: The check is not full proof since it's based on
#hd                   the sequential fortran binary file 4-bytes 
#hd                   header/trailer values. 

#pr  PARAMETERS:
#pr
#pr    POSITIONAL
#pr
#pr      arg1   = [path/]filename to be checked.
#pr

#ex  EXAMPLE:
#ex
#ex          cccrfmt ma_xxx_999_m12_gs
#ex
#ex The above will check the bit-pattern at the top of "ma_xxx_999_m12_gs" 
#ex and returns "yes" if it seems to be consistent with CCCma 8-word label 
#ex record format.
#ex

# set -x

if [ -s "$1" ] ; then
 Filn="$1"
 od -o -N 72 -w16 $Filn | sed -n -e '1p' -e '5p' | $AWK -F' ' 'BEGIN { linenum = 0 ; CCCMA = "no" } ; { linenum = ++linenum ; if ( linenum == 1 ) { head1=$2 ; head2=$3 ; } else if ( $4 == head1 && head1 == "000000" && $5 == head2 && ( head2 == "040000" || head2 == "000100")  ) { CCCMA = "yes" ; } ; } ; END { print CCCMA ; } '
else
 echo " " ; echo "cccrfmt: >>>> invalid/empty -> $1 <- file!" ; echo " "
 exit 5 
fi
