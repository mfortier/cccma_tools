#!/bin/sh

#   Mar 24/92 - E. Chan

#id read_script - reads an in-line job from standard input and saves the 
#id               command and input sections in separate temporary files

#   AUTHOR  - E. Chan

#hd PURPOSE - "read_script" reads an in-line job, splits it into two parts
#hd           (a Unix command section and an input card image section) and
#hd           saves them in temporary files using names supplied on the
#hd           command line. If names are not supplied, by default, the 
#hd           command section in placed in "exec_script" and the input
#hd           section is saved as "Input_Cards".  The in-line job must be
#hd           placed immediately following the line on which this script is
#hd           invoked. Since the in-line job is actually a 'here' document,
#hd           the function must be invoked by specifying:
#hd
#hd                             read_script <<string
#hd
#hd           where 'string' is used as the terminating line of the in-line
#hd           job. The command and input sections must also be separated 
#hd           with a '*EOR'. The user's job may then be executed simply by
#hd           sourcing the commands contained in the "exec_script" file.

#pr PARAMETERS:
#pr
#pr   PRIMARY
#pr
#pr     cfn    = name of file to store Unix command section of job
#pr              (=exec_script)  
#pr     ifn    = name of file to store input card image section of job
#pr              (=Input_Cards)

#ex EXAMPLE:
#ex   
#ex     read_script cfn=command_file <<*EOF
#ex     ...commands...
#ex     *EOR
#ex     ...input cards...
#ex     *EOF
#ex
#ex     The above example reads an in-line job from standard input terminated
#ex     by the string '*EOF'. The portion containing Unix commands are placed  
#ex     in a file called "command_file" and the input section is stored in  
#ex     "Input_Cards".

#  * Reset field separators (otherwise those defined in the parent process
#  * will be used and these may cause problems if they include special 
#  * characters used in this script). 

IFS=' '
export IFS

#  * Obtain the file names.

for arg in $@
do            
  eval $arg
done

#  * Set the defaults.

cfn=${cfn:=exec_script}
ifn=${ifn:=Input_Cards}

#  * Invoke "sed" to split the in-line job into command and input sections
#  * and save them in the appropriate files. 

sed "

1,/*EOR/{
/*EOR/!w $cfn
d
}

"      > $ifn

exit



