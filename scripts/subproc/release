#!/bin/sh

#   Dec 29/93 - F. Majaess (Modify for releasing files from other than
#                           "$RUNPATH")
#   Sep 11/91 - E. Chan

#id release - release removes a file or a symbolic link

#   AUTHOR  - E. Chan

#hd PURPOSE - "release" removes a file or a soft-link that points to
#hd           a file on the official CCRN data directory. Any associated  
#hd           temporary files generated by "copyr", "copyf", or "copyd" are
#hd           also removed. One or more files/soft-links may be released.   
#hd
#hd           This script is designed to emulate the "release" command
#hd           of the Cray Operating System.

#pr PARAMETERS:
#pr
#pr   POSITIONAL
#pr
#pr     fn1 fn2 ... fnm = list of m files to be released

#ex EXAMPLE:
#ex   
#ex  release file1 file2 file3
#ex
#ex     The above example removes 'file1', 'file2', and 'file3' and any
#ex     associated temporary files generated with a "copyr"/"copyf"/"copyd"
#ex     operation.

#  * Reset field separators (otherwise those defined in the parent process
#  * will be used and these may cause problems if they include special 
#  * characters used in this script).

IFS=' '
export IFS

#  * Get the files.
 
file_list=$@

#  * Remove each specified file including the associated temporary file
#  * generated through a "copyr", "copyf", or "copyd" operation (if it 
#  * exists).

for file in $file_list
do 
  Lfn=".""$file""_Link"
  if [ -d $file ] ; then
    chmod -R u+w $file 
  fi
  rm -rf $file ${file}_Copied_Lines $Lfn 
done

exit

