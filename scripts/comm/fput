#! /bin/sh
 
#    SEP 07/93 - F.Majaess (enforce using Victoria node batch line)
#    MAR 02/92 - F.Majaess
 
#id  fput    - Used to transfer file from current machine to ccrn server.
 
#    AUTHOR  - F.Majaess
 
#hd  PURPOSE - "fput" script is used to transfer "fln" file from current 
#hd            machine to ccrn sun main server. The user have the choice 
#hd            of deleting "fln" on the current machine (front-end) after
#hd            being transfered.
 
#pr  PARAMETERS:
#pr
#pr    POSITIONAL     
#pr
#pr      fln    = front end filename to be transfered.
#pr               (the same name is used to store the file on ccrn server).
#pr
#pr    PRIMARY/SECONDARY
#pr
#pr      rmv    = switch controlling deletion of "fln" file on the front-end
#pr               (=no/yes).
#pr      ppath  = path of subdirectory containing possible error output from 
#pr               running fput (=$OUTPUTQ/$PRNTQ).
#pr
#pr    PRIMARY
#pr
#pr      fepath = path to user's favourite directory on front-end 
#pr               account (=$FEPATH).
#pr      sunpath= path to user's favourite directory on ccrn sun's 
#pr               account (=$SUNPATH).
#pr      sv     = ccrn sun server net address (=$RMTSV)
#pr
 
#ex  EXAMPLE: 
#ex
#ex    fput fln=myjob fepath=$HOME/work sunpath='\\\$HOME/back-end-jobs' rmv
 
#   * code used to set switches and deal with parameter=value arguments 
#   * as well as setting parameters to their primary/secondary defaults.
#   * The list of other arguments (if any) is returned in "prmtrl" var...
 
. $SUBPROC/check_set_swtches_prmtrs
 
#   * make sure arguments list variable "prmtrl" is initialized...
 
prmtrl=${prmtrl=}
  
#   * The following code deals with checking/setting positional 
#   * parameters ...
#   * Note: script will abort if too many arguments are specified.
 
if [ -n "$prmtrl" ] ; then
  i=1
  for prmtr in $prmtrl
  do
    case $i in
      1) fln=$prmtr;;
      *) eval "echo '$0 : too many parameters !' "; exit;;
    esac
    i=`expr $i + 1`
  done
fi
 
#   * Prompt for a positional parameter value(s) if none was/were
#   * specified...
 
while [ -z "$fln" ] ; do
  echo "please enter the name of the file to be transfered > \\c"
  read fln
done
 
if [ -f $fepath/$fln ] ; then
  :
else
  eval "echo '$0: ' "
  eval "echo '$fepath/$fln is not a valid filename !' "
  exit
fi
 
if [ "$rmv" = "yes" ] ; then
  eval "echo '$0: ' "
  eval "echo '$fepath/$fln ' "
  eval "echo 'file will be removed from current machine once the transfer' "
  eval "echo 'is complete.' "
  eval "echo 'if this is what you want, enter <CR> ' "
  eval "echo 'to abort, type ^C or ...  ' "
  read prmtr
fi
 
#   ****   Task of the script...   ****
 
#   * setup needed parameters...
# 
 
if [ "$sv" = "$VICSV01" ] ; then
  sv='ftp1.cccma.ec.gc.ca'
fi
Tpid=$$
 
nohupout="/dev/null "
# nohupout="$OUTPUTQ/$$.out"
nohuperr="$ppath/fput""_""$fln""_""$Tpid""_err"
 
# Tupdir1="rcp -p  $fepath/$fln $sv:'$sunpath/$fln' && "
Tupdir1="put $fepath/$fln $sunpath/$fln  "
 
if [ "$rmv" = "yes" ] ; then
# Tupdir2="\rm $fepath/$fln && "
  Tupdir2="!\rm $fepath/$fln "
else
  Tupdir2="ascii" 
fi
 
# set -x
# set -v
( cat << FPUTwoof ) | ( eval "nohup sh - 1>>$nohupout 2>>$nohuperr & " )
## ( /usr/ucb/ftp -v $sv << woof )
## binary
## $Tupdir1
## $Tupdir2
## !\rm $nohuperr
## woof
scp -o NoneSwitch=yes $fepath/$fln "$sv:./$sunpath/$fln" &&
if [ "$rmv" = "yes" ] ; then
 \rm $fepath/$fln
fi || echo 'Problem!'
\rm $nohuperr
FPUTwoof
exit
