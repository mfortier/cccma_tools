#! /bin/sh
 
#    Dec 03/08 - F.Majaess (Revised for sa/saiph, ib/dorval-ib, af/alef)
#    Oct 16/06 - F.Majaess (Revised for ma/maia, ns/naos)
#    Jun 19/06 - F.Majaess (Add CFS auditing for short term files to expire)
#    Mar 01/06 - F.Majaess (Revised for rg/rigel)
#    Jun 22/04 - F.Majaess (Added "expand" to "auditor ... cfs" calls)
#    Jun 20/03 - F.Majaess (Revised for az/azur)
#    Feb 25/02 - F.Majaess (Revised for ya/yata)
#    Jan 05/00 - F.Majaess (Added kz/kaze)
#    Jan 21/99 - F.Majaess (Added yo/yonaka)
#    Jul 14/97 - F.Majaess (Added sx/hiru)
#    May 05/97 - F.Majaess (adjust CFS auditing when pollux is targeted)
#    Apr 17/97 - F.Majaess (add CFS auditing when pollux is targeted)
#    Mar 18/96 - F.Majaess (revise for SX-4)
#    Jun 27/95 - F.Majaess (Added orion)
#    Nov 08/94 - F.Majaess (Modify for sx3r and new destination identifiers)
#    Dec 03/92 - F.Majaess
 
#id spcemgr - Used to clean user's old "tmp..." & produce an audit.
 
#   AUTHOR  - F.Majaess

#hd PURPOSE - "spcemgr" script is used to clean up "usr" "target" 
#hd           file(s)/subdirectories which are more than "ndays" 
#hd           old in /data/... directory(ies) on destination machine
#hd           as well as producing an audit listing of "usr" files.
 
#pr PARAMETERS:
#pr
#pr    PRIMARY
#pr
#pr      Note: One of 'ha/sp/po'.
#pr
#pr          ha = to target job to IBM (hadar).
#pr
#pr          sp = to target job to IBM (spica).
#pr
#pr          po = to target job to Linux (pollux).
#pr
#pr          ca = to target job to Linux (castor).
#pr
#pr          mz = to target job to Linux (mez).
#pr
#pr      target = file/subdiretory name identifier (='tmp*')
#pr      tarpath= subdirectory in which "target" data is residing 
#pr               and is accessible from current machine (=/data/ccrn*).
#pr      ndays  = number of days on which elimination of "target" is 
#pr               based (=3)
#pr      usr    = username code of "target" owner (=$USER)
#pr      time   = back-end CPU time in seconds (=600)
#pr      mfl    = back-end MFL                 (=50MB)

 
#ex  EXAMPLE: 
#ex  
#ex          spcemgr ndays=2 target='tmp*' ha
#ex    
#ex    The above example submit a job to hadar to remove all 'tmp*'
#ex    files/subdirectories in "tarpath" which are at least 2 days old.
 
 
# if [ -f $HOME/info/audit.r ] ; then
#    (\rm -f $HOME/info/audit.r > /dev/null 2>/dev/null || : ) > /dev/null 2>/dev/null
# fi
# if [ -f $HOME/info/audit_convex_cfs ] ; then
#    (\rm -f $HOME/info/audit_convex_cfs > /dev/null 2>/dev/null || : ) > /dev/null 2>/dev/null
# fi

#   * code used to set switches and deal with parameter=value arguments 
#   * as well as setting parameters to their primary/secondary defaults.
#   * The list of other arguments (if any) is returned in "prmtrl" var...
 
#  *  check if switches/parameters specified on script call
 
if [ $# -gt 0 ] ; then
  
#  * put argument list in "prmtrl" ...
   
  prmtrl=$@
  prmtrl=`echo $prmtrl | tr '[A-Z]' '[a-z]'`
  
#  * for each argument specified do...
    
  for prmtr in $prmtrl
  do
    case $prmtr in
 
#   * check for standard shell switches and set them accordingly
 
     --|-|[-+][aefhkntuvx]) set `echo $prmtr ` ;;
 
#   * case where a parameter is to be assigned a value
 
    *=*   ) eval $prmtr ;;

#   * destination.

     ha) mdest=${mdest:='hadar'}       ;;
     sp) mdest=${mdest:='spica'}       ;;
     po) mdest=${mdest:='pollux'}     ;;
     ca) mdest=${mdest:='castor'}     ;;
     mz) mdest=${mdest:='mez'}        ;;
 
#   * anything else case skip...
 
    *  )  tprmtr="$tprmtr $prmtr";;
 
    esac
  done
  prmtrl=$tprmtr
  tprmtr=
 
fi

#   * make sure arguments list variable "prmtrl" is initialized...
 
prmtrl=${prmtrl=}
  
#   * Skip extra parameters...
 
if [ -n "$prmtrl" ] ; then
 eval "echo '$0 : Skipped $prmtrl parameters !' "
fi

#  * Prompt for a destination if none was specified.

while [ -z "$mdest" ]
do
 if [ "$SITE_ID" = 'Dorval' ] ; then
  echo "please enter a destination; ha/sp/po/ca/mz: > \\c"
  read tmdest
  case $tmdest in
      ha) mdest='hadar'                 ;;
      sp) mdest='spica'                 ;;
      po) mdest='pollux'                ;;
      ca) mdest='castor'                ;;
      mz) mdest='mez'                ;;
       *) echo "illegal destination ! " ;;
  esac
 else
  echo "please enter a destination: > \\c"
  read mdest
 fi
done
 
#   * code used to set hard coded primary defaults...
 
target=${target='tmp*'}
tarpath=${tarpath='/data/ccrn'}
eval "ndays=${ndays=3}"
eval "usr=${usr=$USER}"
eval "time=${time='600'}"
eval "mfl=${mfl='250MB'}"
nonqs=${nonqs:=$NONQS}
nonqs=${nonqs:='no'}
mfl=`echo $mfl | tr '[a-z]' '[A-Z]'`
mnfrmid=`echo $mdest | sed -e 's/...//'`
if [ "$mdest" = 'hadar' ] ; then
  mnfrmid='ha'
  BEDATAPATHZ='/fs/dev/crb/had_data/data'
elif [ "$mdest" = 'spica' ] ; then
  mnfrmid='sp'
  BEDATAPATHZ='/fs/dev/crb/spd_data/data'
elif [ "$mdest" = 'pollux' -o "$mdest" = 'jou' ] ; then
# mnfrmid='plx'
  mnfrmid='po'
 #BEDATAPATHZ='/data/cava_ccrn3/lnx_data/data'
  BEDATAPATHZ='/fs/cava/dev/crb/sas/ccrnd/lnx_data/data'
elif [ "$mdest" = 'castor' ] ; then
  mnfrmid='ca'
  BEDATAPATHZ='/fs/cava/dev/crb/sas/ccrnd/lnx_data/data'
elif [ "$mdest" = 'mez' ] ; then
  mnfrmid='mz'
  BEDATAPATHZ='/fs/cava/dev/crb/sas/ccrnd/lnx_data/data'
fi

if [ "$tarpath" = '/data/ccrn' ] ; then

  if [ "$mdest" = 'hadar' ] ; then
#   Dlst="$DATAPATHZ"
    Dlst="/fs/dev/crb/had02/src_data/utmp"
    Dlst="$Dlst /fs/dev/crb/had01/data/utmp /fs/dev/crb/had02/data/utmp /fs/dev/crb/had03/data/utmp"
#   Dlst="$Dlst $DATAPATHZ"
#   if [ "$DATAPATHZ" != "$BERUNPATH" ] ; then
#     Dlst="$Dlst $BERUNPATH"
#   fi
    Dfullst=""
  elif [ "$mdest" = 'spica' ] ; then
#   Dlst="$DATAPATHZ"
    Dlst="/fs/dev/crb/spd02/src_data/utmp"
    Dlst="$Dlst /fs/dev/crb/spd01/data/utmp /fs/dev/crb/spd02/data/utmp /fs/dev/crb/spd03/data/utmp"
#   Dlst="$Dlst $DATAPATHZ"
#   if [ "$DATAPATHZ" != "$BERUNPATH" ] ; then
#     Dlst="$Dlst $BERUNPATH"
#   fi
    Dfullst=""
  elif [ "$mdest" = 'pollux' -o "$mdest" = 'jou' -o "$mdest" = 'castor' -o "$mdest" = 'mez' ] ; then
    Dlst="/fs/cava/dev/crb/sata/ccrna02/src_data/utmp"
    Dlst="$Dlst /fs/cava/dev/crb/sata/ccrna01/data/utmp /fs/cava/dev/crb/sata/ccrna02/data/utmp /fs/cava/dev/crb/sata/ccrna03/data/utmp"
    Dlst="$Dlst /fs/cava/dev/crb/sata/ccrna01/data /fs/cava/dev/crb/sata/ccrna02/data /fs/cava/dev/crb/sata/ccrna03/data"
    Dlst="$Dlst /fs/cava/dev/crb/sata/ccrna04/data/utmp /fs/cava/dev/crb/sata/ccrna05/data/utmp"
    Dlst="$Dlst /fs/cava/dev/crb/sata/ccrna04/data /fs/cava/dev/crb/sata/ccrna05/data"
    Dlst="$Dlst /fs/cava/dev/crb/sas/ccrns01/data/utmp /fs/cava/dev/crb/sas/ccrns02/data/utmp /fs/cava/dev/crb/sas/ccrns03/data/utmp /fs/cava/dev/crb/sas/ccrns04/data/utmp"
    Dlst="$Dlst /fs/cava/dev/crb/sas/ccrns01/data /fs/cava/dev/crb/sas/ccrns02/data /fs/cava/dev/crb/sas/ccrns03/data /fs/cava/dev/crb/sas/ccrns04/data"
    Dlst="$Dlst /fs/cava/dev/crb/sas/ccrns05/data/utmp /fs/cava/dev/crb/sas/ccrns06/data/utmp /fs/cava/dev/crb/sas/ccrns07/data/utmp /fs/cava/dev/crb/sas/ccrns08/data/utmp"
    Dlst="$Dlst /fs/cava/dev/crb/sas/ccrns05/data /fs/cava/dev/crb/sas/ccrns06/data /fs/cava/dev/crb/sas/ccrns07/data /fs/cava/dev/crb/sas/ccrns08/data"
    Dlst="$Dlst /fs/cava/dev/crb/sas/ccrns09/data/utmp /fs/cava/dev/crb/sas/ccrns09/data"
    Dlst="$Dlst /fs/cava/dev/crb/sas/ccrns10/data/utmp /fs/cava/dev/crb/sas/ccrns10/data"
    Dlst="$Dlst /fs/cava/dev/crb/sas/ccrns11/data/utmp /fs/cava/dev/crb/sas/ccrns11/data"
    Dlst="$Dlst /fs/cava/dev/crb/sas/ccrns12/data/utmp /fs/cava/dev/crb/sas/ccrns12/data"
    Dlst="$Dlst /fs/cava/dev/crb/sas/ccrns13/data/utmp /fs/cava/dev/crb/sas/ccrns13/data"
    Dlst="$Dlst /fs/cava/dev/crb/sas/ccrns14/data/utmp /fs/cava/dev/crb/sas/ccrns14/data"
    Dlst="$Dlst /fs/cetus/fs2/crb/linux/cetus_ccrn01/data/utmp /fs/cetus/fs2/crb/linux/cetus_ccrn01/data"
   #Dlst="$Dlst /cnfs/dev/crb/linux/cnfs_ccrn02/data/utmp /cnfs/dev/crb/linux/cnfs_ccrn02/data"
   #Dlst="$Dlst $DATAPATHZ"
    Dfullst="$HOME"
  fi

else
  Dlst=$tarpath
  if [ "$mdest" = 'hadar' ] ; then
    Dfullst=""
  elif [ "$mdest" = 'spica' ] ; then
    Dfullst=""
  elif [ "$mdest" = 'pollux' -o "$mdest" = 'jou' ] ; then
    Dfullst=""
  elif [ "$mdest" = 'castor' -o "$mdest" = 'mez' ] ; then
    Dfullst=""
  else
    Dfullst="$HOME"
  fi
fi

Outfl="$HOME/info/audit.$mnfrmid"
INFODIR="$HOME/info"

#  Create $INFODIR subdirectory if necessary...
#  Correct permissions for $INFODIR ...
 
if [ ! -d "$INFODIR" ] ; then
  \mkdir -m 755 $INFODIR
# echo " $INFODIR subdirectory created "
# \chmod 750 $INFODIR 
fi
chmod g+x $INFODIR

# set -x
# set -v

# setup for proper batch queue

## if [ "$mdest" = 'sx' ] ; then
##  queue=${DEFBEQ:='hiru'}
## # mdest='hiru'
## else
##  queue=$mdest
## fi

queue=$mdest
 
# cat << woofsub  
# umask 027
# (chmod 750 $Outfl || : )

# (cat << woofsub |  qsub -q $queue -lT $time -lM $mfl -r spcemgr -eo -o $Outfl ) 

if [ "$nonqs" = 'yes' ] ; then
 HOSTIDf=`echo $HOSTID | cut -c 1-3`
#if [ "$queue" = "$HOSTID" -o \( "$queue" = 'pollux' -a "$HOSTIDf" = 'ib3' \) ] ; then
 if [ "$queue" = "$HOSTID" -o \( "$queue" = 'pollux' -a "$HOSTIDf" = 'ib3' \) -o \( "$queue" = 'castor' -a "$HOSTIDf" = 'ib4' \) -o \( "$queue" = 'mez' -a "$HOSTIDf" = 'ib8' \) ] ; then
  if [ -s "$Outfl" ] ; then
   (\rm -f $Outfl || : )
  fi
  line="nohup sh -s - 1>>$Outfl 2>>$Outfl & "
 else
  echo "SPCEMGR: Sorry, nohup is limited to the same platform"
  echo "         queue=$queue; HOSTID=$HOSTID !"
  exit 1
 fi
else
  if [ -s "$Outfl" ] ; then
   (\rm -f $Outfl || : )
  fi
 line="qsub -q $queue -lT $time -lM $mfl -r spcemgr -eo -o $Outfl"
fi
cd $HOME/tmp
(cat <<woofsub) | eval "$line"
#! /bin/sh
# set -x
# set -v
# date
Dlst="$Dlst"
Dfullst="$Dfullst"
# Ensure CCRNTMP is processed as well.
varinlist='false'
for prmtr in \$Dlst 
  do
  if [ \$prmtr = \$CCRNTMP ] ; then
   varinlist='true' 
  fi
  done 
if [ \$varinlist = 'false' ] ; then
  Dlst="\$Dlst \$CCRNTMP"
fi
# Ensure RUNPATH is processed as well.
varinlist='false'
for prmtr in \$Dlst 
  do
  if [ \$prmtr = \$RUNPATH ] ; then
   varinlist='true' 
  fi
  done 
# if [ \$varinlist = 'false' ] ; then
# if [ \$varinlist = 'false' -a "$mdest" = 'pollux' ] ; then
if [ \$varinlist = 'false' -a \( "$mdest" = 'pollux' -o "$mdest" = 'castor' -o "$mdest" = 'mez' \) ] ; then
  Dlst="\$Dlst \$RUNPATH"
fi

# set +x
for prmtr in \$Dlst 
  do
  if [ -d \$prmtr ] ; then
   (clntmp tarpath=\$prmtr target='$target' ndays=$ndays usr=$usr  > /dev/null 2> /dev/null || : ) \\
   > /dev/null 2>/dev/null
  fi
  done 
if [ -n "\$Dfullst" ] ; then 
for prmtr in \$Dfullst 
  do
# auditor user=$usr own \\
# auditor user=$usr own full \\
# if [ -d \$prmtr ] ; then
  if [ -d \$prmtr -a \`logname\` != 'acrnsrc' ] ; then
   auditor -a user=$usr full links \\
   dir=\$prmtr 
  fi
  done
fi
# if [ \( "$mdest" = 'alef' -o "$mdest" = 'erg'  -o "$mdest" = 'pollux' \) -a \`logname\` != 'acrnsrc' ] ; then
# if [ \( "$mdest" = 'pollux' -o "$mdest" = 'alef' -o "$mdest" = 'erg' \) -a \`logname\` != 'acrnsrc' ] ; then
# if [ \( "$mdest" = 'pollux' -o "$mdest" = 'alef' -o "$mdest" = 'erg' -o "$mdest" = 'jou' \) -a \`logname\` != 'acrnsrc' ] ; then
# if [ \( "$mdest" = 'pollux' -o "$mdest" = 'jou' \) -a \`logname\` != 'acrnsrc' ] ; then
if [ \( "$mdest" = 'pollux' -o "$mdest" = 'jou' -o "$mdest" = 'castor' -o "$mdest" = 'mez' \) -a \`logname\` != 'acrnsrc' ] ; then
 if [ -f \$CCRNINFO/adtcfs_dir/\`logname\` ] ; then 
   (\rm -f $HOME/info/taudit.cfs $HOME/info/taudit2xpr.cfs > /dev/null 2>/dev/null || : ) > /dev/null 2>/dev/null
   if [ \`ssh cfs2 hostname | cut -c 1-4\` = 'cfs2' -a \`ssh cfs2 ls -1d /home/cfs_ccrd/ccrn\` = '/home/cfs_ccrd/ccrn' ] ; then
#    (auditor own cfs)  > $HOME/info/taudit.cfs 2>/dev/null
     touch $HOME/info/taudit.cfs
     touch $HOME/info/taudit2xpr.cfs
     (auditor full shorterm xdays=75 cfs)  >> $HOME/info/taudit2xpr.cfs 2>/dev/null
     sleep 2
     (auditor full expand cfs)  >> $HOME/info/taudit.cfs 2>/dev/null
     sleep 2
(echo "" ; echo " ==============================================================================" ; echo "") >> $HOME/info/taudit.cfs
     sleep 2
     (auditor full shorterm timesort expand cfs)  >> $HOME/info/taudit.cfs 2>/dev/null
     sleep 2
     if [ -s $HOME/info/taudit.cfs ] ; then
      chgrp ccrn_shr $HOME/info/taudit.cfs
      chmod 640 $HOME/info/taudit.cfs
      \mv $HOME/info/taudit.cfs $HOME/info/audit.cfs
      chmod 640 $HOME/info/audit.cfs
#     if [ ! -f $HOME/info/audit_convex_cfs ] ; then
#       \cp -p $HOME/info/audit.cfs $HOME/info/audit_convex_cfs
#     fi
     fi
     if [ -s $HOME/info/taudit2xpr.cfs ] ; then
      chgrp ccrn_shr $HOME/info/taudit2xpr.cfs
      chmod 640 $HOME/info/taudit2xpr.cfs
      Ttl2xpr=\`cat $HOME/info/taudit2xpr.cfs | tail -11 | sed -n -e 's/^.*Grand Total:  0 bytes (0 files).*$/Empty/p'\`
      \mv $HOME/info/taudit2xpr.cfs $HOME/info/audit2xpr.cfs
      chmod 640 $HOME/info/audit2xpr.cfs
      if [ "\$Ttl2xpr" != 'Empty' ] ; then
       NOTIFY_CFS2XPR=\${NOTIFY_CFS2XPR:-'7'}
       if [ -n "\$NOTIFY_CFS2XPR" -a "\$NOTIFY_CFS2XPR" != 'OFF' ] ; then
        if [ -f "$HOME/info/.cfs2xpr_notified_last" ] ; then
         if [ "\$NOTIFY_CFS2XPR" = \`date '+%a'\` ] ; then
          Mdays=1
         else
          Mdays=\`echo \$NOTIFY_CFS2XPR | sed -n -e '/^[0-9]*$/p'\`
         fi
         Mdays=\${Mdays:-'7'}
         LastUpdt=\`find $HOME/info -name '.cfs2xpr_notified_last' -mtime +\${Mdays} -print\`
        fi
        if [ ! -f "$HOME/info/.cfs2xpr_notified_last" -o -n "\$LastUpdt" ] ; then
         cat $HOME/info/audit2xpr.cfs | Mail -s List_of_CFS_files_to_expire \`logname\` 
         touch $HOME/info/.cfs2xpr_notified_last
         if [ ! -s "$HOME/info/.cfs2xpr_notified_last" ] ; then
          echo ".cfs2xpr_notified_last file: " >> $HOME/info/.cfs2xpr_notified_last
          echo "Used to keep track of the date/time stamp of the last e-mailed" >> $HOME/info/.cfs2xpr_notified_last
          echo "copy of --> audit2xpr.cfs <-- file" >> $HOME/info/.cfs2xpr_notified_last
         fi
        fi
       fi
      else
       (\rm -f $HOME/info/.cfs2xpr_notified_last || : )
      fi
     else
      (\rm -f $HOME/info/.cfs2xpr_notified_last || : )
     fi
   fi
   chmod g+x $HOME
   chmod g+x $HOME/info
   if [ -s $HOME/info/audit.cfs ] ; then
    chmod g+r $HOME/info/audit.cfs
   fi
   if [ -s $HOME/info/audit2xpr.cfs ] ; then
    chmod g+r $HOME/info/audit2xpr.cfs
   fi
 fi
fi
# set +x
# set -x
# set -v
#    (mgrtfls -x   -v   || : ) >> $BEHOME/mgrtfls_out    2>> $BEHOME/mgrtfls_out   
( rmllfls || : )
# date
exit
woofsub
echo "" ; echo "  spcemgr: Job is submitted to run on $mdest "
# ( crontab $HOME/.crontab_ccrn > /dev/null 2>/dev/null ) > /dev/null 2> /dev/null
exit
#  (mgrtfls       || : ) >> $BEHOME/mgrtfls_out    2>> $BEHOME/mgrtfls_out   

