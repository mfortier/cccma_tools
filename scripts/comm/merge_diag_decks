#!/bin/sh

USAGE='
 USAGE
        merge_diag_decks -o merged.dk deck1.dk deck2.dk ...

 DESCRIPTION
        This shell script merges individual diagnostic decks deck1, 
        deck2, ... into a single diagnostic deck.

 FLAGS
        -o merged.dk
                The name of the output merged diagnostic deck. Must be specified.

        -f      If specified, Overwrite the existing merged diagnostic deck.
                Otherwise, the script will abort if the output deck exists.

        -l diag_libs
                The colon separated list of directories to search for input decks.
                The default is "$CCRNSRC/source/diag4".

        deck1.dk deck2.dk ...
                Input diagnostic deck names to be merged. At least one deck must 
                be specified.
 
        -h      Print usage help.

# Authors:
#
# S.Kharin and F.Majaess, Oct 2017
'
if [ $# -lt 1 ] ; then
    echo "$USAGE"
    exit
fi

#
# Evaluate the command line options
#
input_decks=''
output_deck=''
force='off'
diag_libs="$CCRNSRC/source/diag4"
while [ $# -ge 1 ] ; do
    case "$1" in
        -o)     shift
                output_deck=$1
                ;;
        -f)     force='on'
                ;;
        -l)     shift
                diag_libs=$1
                ;;
        -h)     echo "$USAGE"
                exit
                ;;
        *)      input_decks="${input_decks} $1"
                ;;
    esac
    shift
done

echo input_decks="${input_decks}"
if [ -z "${input_decks}" ] ; then
    echo "ERROR: input decks are not specified."
    echo "$USAGE"
    exit
fi
echo output_deck="${output_deck}"
if [ -z "${output_deck}" ] ; then
    echo "ERROR: output_deck deck is not specified."
    echo "$USAGE"
    exit
fi
echo diag_libs="$diag_libs"
echo force="$force"

# check if the output deck already exists
if [ -s ${output_deck} ] ; then
    if [ "$force" = "on" ] ; then
	echo "remove the existing output deck ${output_deck}"
	rm -f ${output_deck}
    else
	echo "ERROR: the output deck ${output_deck} already exist."
	echo "Please remove it first, or use -f flag"
	exit 1
    fi
fi

# Execute_Script opening part
outname=`basename ${output_deck} .dk`

echo "#deck ${outname}
jobname=${outname} ; time=\$stime ; memory=\$memory1
. comjcl.cdk

cat > Execute_Script <<'end_of_script'
startdate=\`date +%s\`
pwd=\`pwd\`" > ${output_deck}

# merge all Execute_Script sections
echo "
Merging all Execute_Script sections...
"

for deck in ${input_decks} ; do
  echo Processing $deck ...
  # scan all directories where decks can be found
  dirs=${diag_libs}
  while [ -n "$dirs" ] ; do
    dir=`echo "$dirs" | cut -f1 -d':'`
    deck_found="off"
    if [ -f $dir/$deck ] ; then
      deck_found="on"
      echo Found $dir/$deck
      echo "
#========= $deck start ===================================
rm -rf \$pwd/tmp.$deck ; mkdir -p \$pwd/tmp.$deck ; cd \$pwd/tmp.$deck ; cp \$pwd/Input_Cards ." >> ${output_deck}
      cat $dir/$deck | sed \
       -e '1,/cat.*Execute_Script.*end_of_script/d' \
       -e '/end_of_script/,$d' \
       -e '/tmpsave.cdk/d' \
       >> ${output_deck}
#       -e 's/^[ 	]*ccc /    /' \
      echo "
cp Input_Cards \$pwd/. ; cd \$pwd; rm -rf \$pwd/tmp.$deck
#========= $deck end =====================================" >> ${output_deck}

      # break if the deck is processed
      break
    fi
    # search in the next directory
    dirs=`echo "$dirs" | cut -s -f2- -d':'`
  done # dirs
  if [ "$deck_found" = "off" ] ; then
    echo "ERROR: $deck is not found!"
    exit 1
  fi
done # input_decks

# Execute_Script closing part

echo "
stopdate=\`date +%s\`
echo runtime \$flabel \$startdate \$stopdate \`expr \$stopdate - \$startdate\`
end_of_script
" >> ${output_deck}


# Input_Cards opening part

echo "
cat > Input_Cards <<end_of_data
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
         DAYS      = \$days
         RUN       = \$run
         FLABEL    = \$flabel
         MODEL1    = \$model1
         T1        = \$t1
         T2        = \$t2
         T3        =      \$t3
         DELT      = \$delt
0 MERGEDIAG ---------------------------------------------------------- MERGEDIAG" >> ${output_deck}

# merge all Input_Cards sections
echo "
Merging all Input_Cards sections...
"

for deck in ${input_decks} ; do
  echo Processing $deck ...
  dirs=${diag_libs}
  while [ -n "$dirs" ] ; do
    dir=`echo "$dirs" | cut -f1 -d':'`
    if [ -f $dir/$deck ] ; then
      echo Found $dir/$deck
      cat $dir/$deck | sed \
       -e '1,/cat.*Input_Cards.*end_of_data/d' \
       -e '/end_of_data/,$d' \
       -e '0,/+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :/d' \
       -e '/\(+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :\|if.*;.*then\)/,$!d' \
       | sed \
       -e '${/+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :/d}' \
       >> ${output_deck}
      break
    fi
    dirs=`echo "$dirs" | cut -s -f2- -d':'`
  done # dirs
done # input_decks

# Input_Cards closing part

echo "end_of_data

. endjcl.cdk" >> ${output_deck}
echo "Done!"
