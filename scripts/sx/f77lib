#!/bin/sh
   
#   Dec 03/2008 - F.Majaess (Revised for sa/saiph, ib/dorval-ib, af/alef)
#   Oct 16/2006 - F.Majaess (Revised for ma/maia, ns/naos)
#   Mar 01/2006 - F.Majaess (Revised for rg/rigel)
#   Jun 20/2003 - F.Majaess (Revised for az/azur)
#   Feb 25/2002 - F.Majaess (Revised for ya/yata)
#   Feb 23/2000 - F.Majaess (Added "sublib" & revised for kz/kaze and "sxf90")
#   Jan 21/1999 - F.Majaess (Added yo/yonaka)
#   Jul 14/1997 - F.Majaess (Added sx/hiru)
#   Mar 18/1996 - F.Majaess (revise for SX-4)
#   Nov 08/1994 - F.Majaess (Modify for sx3r and new destination identifiers)
#   May 10/1994 - E. Chan (Include output from ".s" file if it exists)
#   Oct 27/1992 - E. Chan
   
#id f77lib  - generate a compiled listing for a list of programs or 
#id           subroutines currently residing in the Fortran source code
#id           libraries.
     
#   AUTHOR  - E. Chan
     
#hd PURPOSE - "f77lib" submits a job to one of the back-ends to run the 
#hd           Fortran compiler f77sx (through the environment variable 
#hd           '$F77') on a list of programs or subroutines supplied on the 
#hd           command line. The names of the programs and subroutines
#hd           must be identical to those residing in ~acrnsrc/source$OSbin/lspgm
#hd           and ~acrnsrc/source$OSbin/lssub/[/...] subdirectories on the 
#hd           target platform destination, where "OSbin" refers to the 
#hd           associated platform's identifier extension.
#hd           On output, a compiled listing is returned. 
#hd    
#hd           Wildcard characters may be used in the list of names but
#hd           must be quoted to protect them from expansion by the shell
#hd           during input to the script. 
#hd    
#hd           Any parameter specified on the command line with a '-'
#hd           immediately in front of it is passed on to the Fortran 
#hd           compiler as an option. These are in addition to those already
#hd           defined as part of the environment variable '$F77' used to 
#hd           invoke the compiler. Note that if an option is specified
#hd           more than once, the last value specified for that option
#hd           is used. This is useful in overriding options already turned 
#hd           on in '$F77'.  Options that require blanks between keywords
#hd           must be specified with underscores in place of the blanks.
#hd    
#hd           Because NQS allows a maximum of 10 megabytes to be 
#hd           transferred across from the back-end to the front-end, large 
#hd           printouts are automatically routed back to the front-end via 
#hd           ftp. In this case, two files will appear in the user's .queue 
#hd           directory with the same pid appended to end of the filenames. 
#hd           The listing will be in the file with a period before the pid. 
#hd   
#hd           The "cmprs" option forces "gzip" compression of the listing before
#hd           transferring it back to the front-end via ftp. The filename of 
#hd           the compressed listing will have a '.gz' appended to the end
#hd           of it to indicate that it is in a compressed form. The 
#hd           file may be decompressed by issuing the following command:
#hd   
#hd                      gzip -d filename
#hd    
#hd           where 'filename' is the name of the file (and has a '.gz'
#hd           extension). The decompressed listing will be placed in a
#hd           file with the same name as the original but without the
#hd           '.gz' extension. The compressed file will be automatically
#hd           deleted. 
#hd    
#hd           ***********************************************************
#hd           *** The compiled listing should be printed out with the ***
#hd           *** "li" script on the front-end which correctly handles***
#hd           *** carriage controls contained in the printout. The    ***
#hd           *** script "lo" is NOT recommended.                     ***
#hd           ***********************************************************
       
#pr PARAMETERS:
#pr   
#pr   POSITIONAL
#pr  
#pr   file_lst = list of program or subroutine names residing in the source
#pr              directories on the back-end.
#pr  
#pr   PRIMARY
#pr       
#pr      Note: One of 'ha','sp','po','ca' or 'mz' is to be specified.
#pr
#pr         ha = to target job to IBM (hadar).
#pr
#pr         sp = to target job to IBM (spica).
#pr
#pr         po = to target job to Linux (pollux).
#pr
#pr         ca = to target job to Linux (castor).
#pr
#pr         mz = to target job to Linux (mez).
#pr
#pr       time = time limit in seconds (=600)
#pr   
#pr        mem = memory limit in megabytes (=>900 for rigel/maia/noas, 
#pr                                          =50 otherwise)
#pr   
#pr        lib = library path under $CCRNSRC  
#pr              (=lssub by default; =lpsgm if "lspgm" is invoked)
#pr
#pr     sublib = (optional); subdirectory residing ( not necessarily immediately)
#pr              under $CCRNSRC/source$OSbin/$lib to start search for "fil" from 
#pr              instead of the default $CCRNSRC/source$OSbin/$lib.
#pr              (i.e. if lib=lssub and sublib=gcm6u; target search subdirectory
#pr                    will be $CCRNSRC/source$OSbin/lssub/.../gcm6u )
#pr
#pr   SECONDARY
#pr   
#pr      lspgm = switch to indicate list of names specified are programs
#pr              and not subroutines (the default);
#pr              (if specified leads to lib=lspgm).
#pr    
#pr      cmprs = switch to turn on compression of compiled listing 
#pr    
#pr          xf90   = DISABLED.
#pr                   switch to force the use of the cross compiler on the
#pr                   on the front-end which also entails running the job 
#pr                   on the front-end.
#pr                   (='no'/'yes').
      
#ex EXAMPLES:
#ex   
#ex     f77lib lib=lssub/model/agcm/gcm6u mhanl6.f mhexp6.f ha
#ex
#ex     The above example generates a compiled listing for the subroutines
#ex     contained in the "mhanl6.f" and "mhexp6.f" in the "gcm6u" model
#ex     source subroutine directory $CCRNSRC/source$OSbin/lssub/model/agcm/gcm6u
#ex     on Hadar.
#ex   
#ex     f77lib lspgm -L_fmtlist_page pgm1.f 't*.f' sx
#ex  
#ex     The above example generates a compiled listing for the programs 
#ex     contained in the files pgm1.f and in those beginning with 't' and 
#ex     ending with '.f' in the source program directory on SX(s). Note 
#ex     that the parameter 'lspgm' must be specified. The compiler options
#ex     '-L fmtlist page' is passed on to the f77sx command line.
#ex
#ex     f77lib lib=lssub gcm6u mhanl6.f mhexp6.f xf90 kz
#ex
#ex     The above example generates a compiled listing, via the cross
#ex     compiler on the front-end, for the subroutines contained in the 
#ex     "mhanl6.f" and "mhexp6.f" files in the "gcm6u" model source 
#ex     subroutine directory $CCRNSRC/source$OSbin/lssub/model/agcm/gcm6u
#ex     on SX(s).
      
#  * Reset field separators (otherwise those defined in the parent process
#  * will be used and these may cause problems if they include special 
#  * characters used in this script). 

IFS=' '
export IFS

#  * Obtain the submission file name and any specified option.

for arg in $@
do
  case $arg in 
       -*) arg=`echo $arg | tr '_' ' '` ;
           options="$options $arg"      ;;
      *=*) eval $arg                    ;;
#    xf90) xf90='yes'                   ;;
       ha) mdest=${mdest:='hadar'}      ;;
       sp) mdest=${mdest:='spica'}      ;;
       po) mdest=${mdest:='pollux'}       ;;
       ca) mdest=${mdest:='castor'}       ;;
       mz) mdest=${mdest:='mez'}       ;;
    cmprs) compress='yes'               ;;
    lspgm|lssub) lib=${lib:=$arg}       ;;
      *.f) file_lst="$file_lst $arg"    ;; 
        *) tmp1=${lib:+$arg}            ; 
           sublib=${sublib:=$tmp1}      ; 
           lib=${lib:=$arg}
  esac
done

#  * Prompt for a destination if none was specified.

while [ -z "$mdest" ]
do
  echo "please enter a destination; ha/sp/po/ca/mz: > \\c"
  read tmdest
  case $tmdest in
      ha) mdest='hadar'                 ;;
      sp) mdest='spica'                 ;;
      po) mdest='pollux'                  ;;
      ca) mdest='castor'                ;;
      mz) mdest='mez'                   ;;
       *) echo "illegal destination ! " ;;
  esac
done

#  * Set the defaults.

lib=${lib:=lssub}

time=${time:=600}
if [ "$mdest" = 'hadar' -o "$mdest" = 'spica' ] ; then
 mem=${mem:=900}
 if [ "$mem" -lt 900 ] ; then
  echo "Note: mem=$mem is changed to mem=900 "
  mem=900
 fi
else
 mem=${mem:=250}
fi

# if [ "$xf90" = 'yes' ] ; then
#   if [ \( "$mdest" != 'kaze' -a "$mdest" != 'yonaka' -a "$mdest" != 'yata' \) -o "$HOSTID" != 'pollux' ] ; then
#    echo "" ; echo "  f77lib: Sorry, the script should be executing on Pollux"
#    echo "                 and mdest=[kaze,yata,yonaka] when xf90=yes!"
#    exit 1
#   else
#    if [ "$mdest" != 'yonaka' -a "$mdest" != 'yata' ] ; then
#      mdest='kaze'
#    fi
#   fi
# fi
# # if [ \( "$mdest" = 'kaze' -o "$mdest" = 'yata' -o "$mdest" = 'yonaka' \) -a "$SITE_ID" = 'Dorval' ] ; then
# if [ \( "$mdest" = 'kaze' -o "$mdest" = 'yonaka' \) -a "$SITE_ID" = 'Dorval' ] ; then
#   xf90='yes'
#   # Set variables needed to adjust for SXCROSS_kit & "sxf90" versions...
# 
#   SXCROSS_KIT=${SXCROSS_KIT:='10.1'}
#   sxcross_kit=${sxcross_kit:=$SXCROSS_KIT}
#   SXF90_VER=${SXF90_VER:='2.05'}
#   sxf90_ver=${sxf90_ver:=$SXF90_VER}
#    export sxcross_kit sxf90_ver
# fi

# setup for proper batch queue

# if [ "$mdest" = 'sx' ] ; then
#  queue=${DEFBEQ:='hiru'}
#  mdest='hiru'
# elif [ "$xf90" = 'yes' ] ; then
#  queue=${queue:='pollux'}
# else
#  queue=$mdest
# fi

queue=$mdest

if [ "$mdest" = 'hadar' ] ; then
  mext='ha'
elif [ "$mdest" = 'spica' ] ; then
  mext='sp'
elif [ "$mdest" = 'pollux' ] ; then
  mext='po'
elif [ "$mdest" = 'castor' ] ; then
  mext='ca'
elif [ "$mdest" = 'mez' ] ; then
  mext='mz'
else
  mext='sss'
fi

#  * Move into a temporary directory to execute the script.

Stamp="${HOSTID}_"`date +%Y%j%H%M%S`
mkdir -m 755 $HOME/tmp/tmp_f77lib_$$_$Stamp
cd $HOME/tmp/tmp_f77lib_$$_$Stamp

#
#  * Build script to do the compiling on the destination machine.
#

cat <<endcat > job
#!/bin/sh
file_lst="$file_lst"
# Set target directory for the source code...
if [ -n "$sublib" ] ; then
  libpath=\`find \$CCRNSRC/source\$OSbin/${lib}/. -name $sublib -print -depth 2>/dev/null \`
  if [ -z "\$libpath" -o \\( -n "\$libpath" -a ! -d "\$libpath" \\) ] ; then
    echo "f77lib: unable to find --> \$sublib <-- subdirectory under \$CCRNSRC/source\$OSbin/$lib !"
    exit 1
  fi
else
  libpath="\$CCRNSRC/source\$OSbin/$lib"
fi

if [ -d "\$libpath" ] ; then

  Stamp="\${HOSTID}_"\`date +%Y%j%H%M%S\`
  mkdir -m 755 \$CCRNTMP/tmp_f77lib_\$\$_\$Stamp
  cd \$libpath 
  #
  #  * Put all the code into one file.
  #
# cat $file_lst >> \$CCRNTMP/tmp_f77lib_\$\$_\$Stamp/source.f
  touch \$CCRNTMP/tmp_f77lib_\$\$_\$Stamp/source.f
  for arg in \$file_lst
   do
    pthfl=\`find ./. -name \$arg -print | tail -1\`
    if [ -n "\$pthfl" ] ; then 
      if [ -s "\$pthfl" ] ; then
       cat \$pthfl >> \$CCRNTMP/tmp_f77lib_\$\$_\$Stamp/source.f
      else
       echo "f77lib: skipped \$pthfl"
      fi
   else
    echo "f77lib: \$arg not found; skipped!"
   fi
   done
  cd \$CCRNTMP/tmp_f77lib_\$\$_\$Stamp
  #
  #  * Run the compiler.
  #
  set -x
  ##  if [ "$xf90" = 'yes' ] ; then
  ##   set +x
  ##   eval \`swset -style sh -exitonerror -set P -first -p pub/SXCROSSkit-${sxcross_kit} \`
  ##   eval \`swset -style sh -exitonerror -set P -first -p pub/sxf90-${sxf90_ver} \`
  ##   set -x
  ##   \$SXF90 $options -c source.f 
  ##  else
  ##   \$F77 $options -c source.f 
  ##  fi

  \$F77 $options -c source.f 
  set +x
  if [ -s *.s ] ; then
    cat *.s >> source.lst
  fi
  #
  #  * Obtain the size of the listing.
  #
  line=\`ls -l source.lst\`
  size=\`echo \$line | cut -d' ' -f5\`
  #
  #  * Determine whether to route the output via NQS or ftp.
  #
  if [ "\$size" -lt 8000000 -a "$compress" != 'yes' ] ; then
    echo  
    cat source.lst
  else
    if [ "$compress" = 'yes' ] ; then
      # compress source.lst
      # ext=.Z
      gzip source.lst
      ext=.gz
    fi
##  if [ "$xf90" = 'yes' -o "\$HOSTID" = 'pollux' ] ; then
##   mv source.L\${ext} \$HOME/.queue/f77lib.$mext.$$\${ext}
##  else
###      ftp pollux <<..endftp
###      binary
###      cd .queue
###      put source.lst\${ext} f77lib.$mext.$$\${ext}
### ..endftp
     cp source.lst\${ext} $HOME/.queue/f77lib.$mext.$$\${ext}
##  fi
    echo "######################################################"
    echo " check f77lib.$mext.$$\${ext} for compiled listing      "
    echo "######################################################"
    if [ "$compress" = 'yes' ] ; then
      echo
      echo "######################################################"
      echo "   f77lib.$mext.$$\${ext} must be uncompressed          "
      echo "######################################################"
    fi
  fi
  #
  #  * Remove temporary directory and exit the script.
  #
  cd ..
  \rm -r tmp_f77lib_\$\$_\$Stamp
  ( rmllfls || : )
else
  echo "f77lib: \$libpath is not a valid subdirectory !"
  ( rmllfls || : )
  exit 2
fi
 
endcat
  
#  * Submit the job.

qsub -q $queue job -lT $time -lM ${mem}mb -r f77lib -eo \
     -o $OUTPUTQ/f77lib.${mext}_$$ 

#  * Remove temporary directory and exit script.

cd ..
\rm -r tmp_f77lib_$$_$Stamp

exit
