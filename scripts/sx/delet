#! /bin/sh

#    Dec 03/08 - F.Majaess (Revised for sa/saiph, ib/dorval-ib, af/alef)
#    Oct 16/06 - F.Majaess (Revised for ma/maia, ns/naos)
#    Mar 01/06 - F.Majaess (Revised for rg/rigel)
#    Feb 15/05 - F.Majaess (Revised for AIX/Linux setup in Victoria)
#    Jun 20/03 - F.Majaess (Revised for az/azur)
#    Feb 25/02 - F.Majaess (Revised for ya/yata)
#    Sep 14/00 - F.Majaess (Revised for tu/tsunami)
#    Jan 05/00 - F.Majaess (Added kz/kaze)
#    Jan 21/99 - F.Majaess (Added yo/yonaka)
#    Jul 14/97 - F.Majaess (Added sx/hiru)
#    Mar 14/96 - F.Majaess (Added sx4)
#    Jan 08/96 - MB,FM (Revise to use access/delete)
#    Jun 02/95 - F.Majaess (Added orion, gandalf & aragorn)
#    Nov 08/94 - F.Majaess (Modify for new destination identifiers)
#    Nov 13/92 - E. Chan   
 
#id  delet   - Removes a file from one of the official CCRD/N file systems
#id            and its associated symbolic link from the official CCRD/N
#id            data directory.
 
#    AUTHOR - E. Chan  
 
#hd  PURPOSE - "delet" submits a job to one of the local machine at the 
#hd            local site to delete the specified file on one of the 
#hd            official CCRD/N file systems and its corresponding link on 
#hd            the official CCRD/N data directory. If the file actually 
#hd            resides on the data directory, then there isn't any link 
#hd            to be removed.
#hd 
#hd            By default, the latest edition is removed. An optional parameter
#hd            "ed" may be used to delete other editions.
#hd  
#hd            No printout is returned unless the "outfil" switch is used.
 
#pr  PARAMETERS:
#pr  
#pr    POSITIONAL     
#pr  
#pr      file   = name of permanent file 
#pr
#pr    PRIMARY
#pr   
#pr      Note: One of 'ha/sp/po/ca/mz/hr/br/p1/p2'  in Dorval.
#pr            In Victoria, default destination: "$HOSTID"  for Linux.
#pr
#pr          hr = to target job to hare.
#pr
#pr          br = to target job to brooks.
#pr
#pr          p1 = to target job to ppp1.
#pr
#pr          p2 = to target job to ppp2.
#pr
#pr          ha = to target job to IBM (hadar).
#pr
#pr          sp = to target job to IBM (spica).
#pr
#pr          po = to target job to Linux (pollux).
#pr   
#pr          ca = to target job to Linux (castor).
#pr
#pr          mz = to target job to Linux (mez).
#pr
#pr  
#pr      time   = time limit for job in seconds (=200)
#pr  
#pr       mem   = memory limit for job in megabytes (=25)
#pr  
#pr        ed   = edition number of file (defaults to the latest edition)
#pr  
#pr    SECONDARY
#pr  
#pr      outfil = switch to force the return of printout
 
#ex  EXAMPLE: 
#ex  
#ex  delet filex ha ed=3
#ex 
#ex  The above example deletes a file named "filex.003" from one of the
#ex  official CCRD/N file systems on Haar. The corresponding link on the 
#ex  official data directory is also removed if it exists. 

#  * Obtain any specified option. 

for arg in $@
do
  case $arg in
     -x|-v) set $arg                         ;;
       *=*) eval $arg                        ;;
      ed=*) ed=`expr $arg : 'ed=\([0-9]*\)'` ;;
   hare|hr) mdest=${mdest:='hare'}           ;;
 brooks|br) mdest=${mdest:='brooks'}         ;;
   ppp1|p1) mdest=${mdest:='ppp1'}           ;;
   ppp2|p2) mdest=${mdest:='ppp2'}           ;;
  hadar|ha) mdest=${mdest:='hadar'}          ;;
  spica|sp) mdest=${mdest:='spica'}          ;;
 po|pollux) mdest=${mdest:='pollux'}         ;;
 ca|castor) mdest=${mdest:='castor'}         ;;
   mz|mez) mdest=${mdest:='mez'}             ;;
    mamccc) mdest=${mdest:='mamccc'}         ;;
      odin) mdest=${mdest:='odin'}           ;;
   climate) mdest=${mdest:='climate'}        ;;
    outfil) outfil=yes                       ;;
         *) file=${file:=$arg}
  esac
done

#  * Set the defaults.

time=${time:=200}
mem=${mem:=25}
nonqs=${nonqs:=$NONQS}
nonqs=${nonqs:='no'}
 
#  * Prompt for filename if none was specified.
 
while [ -z "$file" ] 
do
  echo "please enter file name: > \\c"
  read file
done

#  * Prompt for a destination if none was specified.

while [ -z "$mdest" ]
do
 if [ "$SITE_ID" = 'Victoria' ] ; then
    mdest="$HOSTID"
 elif [ "$SITE_ID" = 'York' ] ; then
   mdest="$HOSTID"
 elif [ "$SITE_ID" = 'Utor1' -o "$SITE_ID" = 'UOFT' ] ; then
   mdest="$HOSTID"
 elif [ "$SITE_ID" = 'Dorval' -o "$SITE_ID" = 'DrvlSC' ] ; then
  echo "please enter a destination; ha/sp/po/ca/mz/mz/hr/br/p1/p2: > \\c"
  read tmdest
  case $tmdest in

   hare|hr) mdest='hare'                ;;
 brooks|br) mdest='brooks'              ;;
   ppp1|p1) mdest='ppp1'                ;;
   ppp2|p2) mdest='ppp2'                ;;
  hadar|ha) mdest='hadar'               ;;
  spica|sp) mdest='spica'               ;;
 po|pollux) mdest='pollux'              ;;
 ca|castor) mdest='castor'              ;;
    mz|mez) mdest='mez'                 ;;
       *) echo "illegal destination ! " ;;
  esac
 elif [ "$SITE_ID" = 'Downsview' ] ; then
    mdest="$HOSTID"
 else
  echo "please enter a destination: > \\c"
  read mdest
 fi
done
 
#  * Remove leading zeros in the edition number and ensure that  
#  * three digits are used if it is specified.
 
if [ -n "$ed" ] ; then
  ed=`expr $ed + 0`
  if [ "$ed" -lt 100 ] ; then
    ed=0$ed
  fi
  if [ "$ed" -lt 10 ] ; then
    ed=0$ed
  fi
fi

#  * Construct and submit the job.

if [ "$nonqs" = 'yes' ] ; then
  if [ -z "$outfil" ] ; then
    outdest="/dev/null"
  else
    outdest=$OUTPUTQ/delet_${file}.$$
  fi
  if [ "$OS" = 'Linux' ] ; then
   Batch_job="/tmp/tmp_delet_job$$"
   RM_Batch_job="rm -f $Batch_job"
   line="cat > $Batch_job ; chmod u+x $Batch_job ; nohup $Batch_job 1>>$outdest 2>>$outdest & " 
  else
   RM_Batch_job=':'
   line="nohup sh -s - 1>>$outdest 2>>$outdest & "
  fi
else
  if [ -z "$outfil" ] ; then
    if [ "$SITE_ID" = 'Dorval' -o "$SITE_ID" = 'DrvlSC' ] ; then
      outdest="$mdest:/dev/null"
    else
      outdest="/dev/null"
    fi
  elif [ "$mdest" = 'hare' ] ; then
    outdest=$OUTPUTQ/delet_${file}.hr.$$
  elif [ "$mdest" = 'brooks' ] ; then
    outdest=$OUTPUTQ/delet_${file}.br.$$
  elif [ "$mdest" = 'ppp1' ] ; then
    outdest=$OUTPUTQ/delet_${file}.p1.$$
  elif [ "$mdest" = 'ppp2' ] ; then
    outdest=$OUTPUTQ/delet_${file}.p2.$$
  elif [ "$mdest" = 'hadar' ] ; then
    outdest=$OUTPUTQ/delet_${file}.ha.$$
  elif [ "$mdest" = 'spica' ] ; then
    outdest=$OUTPUTQ/delet_${file}.sp.$$
  elif [ "$mdest" = 'pollux' ] ; then
    outdest=$OUTPUTQ/delet_${file}.po.$$
  elif [ "$mdest" = 'castor' ] ; then
    outdest=$OUTPUTQ/delet_${file}.ca.$$
  elif [ "$mdest" = 'mez' ] ; then
    outdest=$OUTPUTQ/delet_${file}.mz.$$
  else
    outdest=$OUTPUTQ/delet_${file}.$$
  fi
  line="$CCCQSUB -q $mdest -lT $time -lM ${mem}mb -r delet -eo -o $outdest "
fi
cd $HOME/tmp
# $CCCQSUB -q $mdest -lT $time -lM ${mem}mb -r delet.$file \
# $CCCQSUB -q $mdest -lT $time -lM ${mem}mb -r delet \
#                -eo -o $outdest <<endjob
(cat << endjob) | eval "$line"
#!/bin/sh
#
if [ -n "$outfil" ] ; then
  set -x
fi  
#
Stamp="\${HOSTID}_"\`date +%Y%j%H%M%S\`
mkdir -m 755 \$CCRNTMP/tmp_delet_\$\$_\$Stamp
cd \$CCRNTMP/tmp_delet_\$\$_\$Stamp
#
#  * Access the file...
#  * The latest edition is used if the edition number
#  * is not specified.
#
if [ -z "$ed" ] ; then
  access delet_file ${file} nocp na 2>/dev/null
else
  access delet_file ${file} ed=$ed nocp na 2>/dev/null
fi
#
#  * Delete the file.
#
delete delet_file na
release delet_file
#
cd \$CCRNTMP 
\rm -r -f \$CCRNTMP/tmp_delet_\$\$_\$Stamp
( rmllfls || : )
$RM_Batch_job
exit
endjob

exit
